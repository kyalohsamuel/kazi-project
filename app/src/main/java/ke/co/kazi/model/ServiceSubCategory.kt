package ke.co.kazi.model

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * {
"_id": "5b9252bc7a5ed62e5f347f3b",
"status": "ACTIVE",
"created_by": "SYSTEM",
"category_code": "LEGAL",
"subcategory_code": "ADVOCATE",
"subcategory_name": "ADVOCATE",
"description": " ADVOCATE SUB CATEGORY",
"image_url": "/images/service/default.png",
"created_date": "2018-09-07T10:28:12.898Z",
"ServicesubCategory_id": 1,
"__v": 0
}
 */

//@Entity(tableName = "service_sub_categories")
data class ServiceSubCategory(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var serviceId: Int?,
                              var description: String,
                              @SerializedName("created_date") @ColumnInfo(name = "created_date") var createdDate: String,
                              @SerializedName("created_by") @ColumnInfo(name = "created_by") var createdBy: String,
                              var status: String,
                              @SerializedName("category_code") @ColumnInfo(name = "category_code") var categoryCode: String,
                              @SerializedName("subcategory_code") @ColumnInfo(name = "subcategory_code") var subCategoryCode: String,
                              @SerializedName("subcategory_name") @ColumnInfo(name = "subcategory_name") var subCategoryName: String,
                              @SerializedName("image_url") @ColumnInfo(name = "image_url") var imageUrl: String,
                              @SerializedName("ServiceCategory_id") @ColumnInfo(name = "service_category_id") var serviceCategoryId: Int?,
                              @SerializedName("num_of_services") var serviceCount: Int = 0) : Serializable {
    constructor() : this(null, "", "", "", "", "", "", "", "", null)

    fun remainingServiceCount() = serviceCount
}