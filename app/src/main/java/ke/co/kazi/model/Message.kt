package ke.co.kazi.model

import com.google.gson.annotations.SerializedName

data class Message(@SerializedName("chat_id") var id: String,
                   var to:String,
                   @SerializedName("from_user") var from: String,
                   var message:String,
                   @SerializedName("user_type") var type: MessageType,
                   @SerializedName("request_id") var requestId: String,
                   var time:String)

enum class MessageType {
    CLIENT, SP
}