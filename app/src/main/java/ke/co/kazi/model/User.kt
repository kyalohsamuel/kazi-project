package ke.co.kazi.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

/*
 *
 *    {
        "current_loc": {
            "coordinates": []
        },
        "user_type": "CLIENT",
        "registered_by": "SELF",
        "status": "ACTIVE",
        "login_attempts": 0,
        "first_time_login": false,
        "user_category": "STANDARD",
        "_id": "5b8609d05d3335424fc45f12",
        "username": "0716010617",
        "password": "AAAAEAABhqDHc4idg25Quq3soM5krCuRdFxmavYg9zQ+a0iiFUWxUtgInFjUuw90k9gfQ6U3jRc=",
        "profile_image_path": "/images/profile/avatar.png",
        "names": "Daniel Gathigai",
        "email": "gatihgai@gmail.com",
        "contact": "0716010617",
        "country_code": "254",
        "tnc": true,
        "registration_date": "2018-08-29T02:49:52.143Z",
        "user_id": 1,
        "__v": 0,
        "last_login": "2018-08-29T19:08:10.109Z"
    }
 */
@Entity(tableName = "users")
data class User(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var userId: Int = 0,
                @SerializedName("user_id") var serverId: Int? = null,
                @SerializedName("names") var fullName: String = "",
                var status: String = "",
                @SerializedName("login_attempts") @ColumnInfo(name = "login_attempts") var loginAttempts: String = "",
                @SerializedName("first_time_login") @ColumnInfo(name = "first_time_login") var firstTimeLogin: Boolean = false,
                @SerializedName("user_category") @ColumnInfo(name = "user_category") var userCategory: String = "",
                @ColumnInfo(name = "username") var username: String = "",
                @SerializedName("profile_image_path") @ColumnInfo(name = "profile_image_path") var profileImagePath: String = "",
                @ColumnInfo(name = "email") var email: String = "",
                var contact: String = "",
                @SerializedName("country_code") @ColumnInfo(name = "country_code") var countryCode: String = "",
                @SerializedName("tnc") var termsAndConditionsAccepted: Boolean = false,
                @SerializedName("registration_date") @ColumnInfo(name = "registration_date") var registrationDate: String = "",
                @SerializedName("last_login") @ColumnInfo(name = "last_login") var lastLogin: String = "",
                @SerializedName("user_type") @ColumnInfo(name = "user_type") var userType: String = "",
                @SerializedName("registered_by") @ColumnInfo(name = "registered_by") var registeredBy: String = ""
)