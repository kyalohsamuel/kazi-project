package ke.co.kazi.model

import android.net.Uri

data class Certification(
        var name: String,
        var institution: String,
        var qualification: String,
        var imageUri: Uri?,
        var syncing: Boolean = true)