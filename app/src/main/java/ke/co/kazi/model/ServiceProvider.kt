package ke.co.kazi.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ServiceProvider(
        @SerializedName("user_type") var userType: String = "",
        @SerializedName("registered_by") var registeredBy: String = "",
        @SerializedName("status") var status: String = "",
        @SerializedName("login_attempts") var loginAttempts: Int = 0,
        @SerializedName("first_time_login") var firstTimeLogin: Boolean = false,
        @SerializedName("_id") var id: String = "",
        @SerializedName("username") var username: String = "",
        @SerializedName("profile_image_path") var profileImagePath: String = "",
        @SerializedName("names") var names: String = "",
        @SerializedName("email") var email: String = "",
        @SerializedName("contact") var contact: String = "",
        @SerializedName("country_code") var countryCode: String = "",
        @SerializedName("tnc") var tnc: Boolean = false,
        @SerializedName("user_id") var userId: Int = 0
) : Serializable