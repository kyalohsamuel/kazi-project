package ke.co.kazi.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ServiceRequest(
        var status: String,
        @SerializedName("request_date") var requestDate: String,
        @SerializedName("service_request_id") var requestId: Int,
        var service: Service,
        @SerializedName("client_location") var location: Location = Location(),
        var client: Client,
        @SerializedName("service_provider") var serviceProvider: ServiceProvider

) : Serializable

data class Client(var names: String = "", var email: String = "", var contact: String = "") : Serializable