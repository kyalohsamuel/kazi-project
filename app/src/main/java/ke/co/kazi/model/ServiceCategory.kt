package ke.co.kazi.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 *  {
"_id": "5b853b415d3335424fc45f10",
"status": "ACTIVE",
"created_by": "SYSTEM",
"category_code": "LEGAL",
"category_name": "LEGAL",
"description": " LEGAL Category",
"image_url": "/images/service/default.png",
"created_date": "2018-08-28T12:08:33.406Z",
"ServiceCategory_id": 3,
"__v": 0
}
 */
@Entity(tableName = "service_categories")
data class ServiceCategory(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var serviceId: Int?,
                           var description: String,
                           @SerializedName("created_date") @ColumnInfo(name = "created_date") var createdDate: String,
                           @SerializedName("created_by") @ColumnInfo(name = "created_by") var createdBy: String,
                           var status: String,
                           @SerializedName("category_code") @ColumnInfo(name = "category_code") var categoryCode: String,
                           @SerializedName("category_name") @ColumnInfo(name = "category_name") var categoryName: String,
                           @SerializedName("image_url") @ColumnInfo(name = "image_url") var imageUrl: String,
                           @SerializedName("ServiceCategory_id") @ColumnInfo(name = "service_category_id") var serviceCategoryId: Int?,
                           @SerializedName("num_of_services") var subCategoryCount: Int = 0) : Serializable {
    constructor() : this(null, "", "", "", "", "", "", "", null)
}