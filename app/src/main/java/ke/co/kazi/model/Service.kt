package ke.co.kazi.model

import androidx.room.*
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*


/**
 *  {
"_id": "5b8aa7bdb279ffb2d016a92c",
"website": "www.dani.co.ke",
"service_social": {
"facebook": "facebook",
"twitter": "#daniandsons"
},
"service_location": {
"coordinates": [
-1.2916937,
36.7981071
],
"type": "Point"
},
"num_view": 0,
"ranking": 0,
"rating": 0,
"likes": 0,
"status": "ACTIVE",
"service_sub_category_code": "ADVOCATE",
"business_service_name": "DANIEL AND SONS ADVOCATES",
"spid": 2,
"service_description": "Our legal service",
"created_by": "2",
"service_contacts": " 0725596227",
{
"service_operations": [
{
"_id": "5b8aa7bdb279ffb2d016a92e",
"Day": "MONDAY",
"from": "8:00 AM",
"to": "7:00 PM"
},
"_id": "5b8aa7bdb279ffb2d016a92d",
"Day": "TEUSDAY",
"from": "8:00 AM",
"to": "7:00 PM"
}
],
"service_address": " P.O.BOX 56422 NAIROBI ",
"created_date": "2018-09-01T14:52:45.673Z",
"service_id": 1,
}
 */

@Entity(tableName = "services")
data class Service(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var serviceId: Int? = null,
                   @SerializedName("business_service_name") var name: String = "",
                   @SerializedName("service_id") var serverId: Int? = null,
                   @SerializedName("service_description") var description: String = "",
                   @SerializedName("created_date") var createdDate: Date = Date(),
                   @Embedded(prefix = "location_") @SerializedName("service_location") var location: Location = Location(),
                   var website: String = "",
                   @Embedded(prefix = "social_") @SerializedName("service_social") var social: Social = Social(),
                   @SerializedName("num_view") var numberOfViews: Int = 0,
                   var ranking: Int = 0,
                   var rating: Float = 3f,
                   var likes: Int = 0,
                   var status: String = "",
                   @Ignore var lat: String = "",
                   @Ignore var lng: String = "",
                   @SerializedName("category_code") var categoryCode: String = "",
                   @SerializedName("service_sub_category_code") var subCategoryCode: String = "",
                   @SerializedName("service_contacts") var contact: String = "",
                   @SerializedName("service_email") var email: String = "",
                   @SerializedName("service_address") var address: String = "",
                   @SerializedName("location_name") var locationName: String = "Nairobi, Kenya",
                   @SerializedName("service_operations") var operations: Array<OperationTime> = arrayOf(),
                   var images: Array<ServiceImage> = arrayOf()
) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Service

        if (serviceId != other.serviceId) return false
        if (name != other.name) return false
        if (serverId != other.serverId) return false
        if (description != other.description) return false
        if (createdDate != other.createdDate) return false
        if (location != other.location) return false
        if (website != other.website) return false
        if (social != other.social) return false
        if (numberOfViews != other.numberOfViews) return false
        if (ranking != other.ranking) return false
        if (likes != other.likes) return false
        if (status != other.status) return false
        if (subCategoryCode != other.subCategoryCode) return false
        if (contact != other.contact) return false
        if (address != other.address) return false
        if (!Arrays.equals(operations, other.operations)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = serviceId ?: 0
        result = 31 * result + (serverId ?: 0)
        result = 31 * result + name.hashCode()
        result = 31 * result + description.hashCode()
        result = 31 * result + createdDate.hashCode()
        result = 31 * result + location.hashCode()
        result = 31 * result + website.hashCode()
        result = 31 * result + social.hashCode()
        result = 31 * result + numberOfViews
        result = 31 * result + ranking
        result = 31 * result + likes
        result = 31 * result + status.hashCode()
        result = 31 * result + subCategoryCode.hashCode()
        result = 31 * result + contact.hashCode()
        result = 31 * result + address.hashCode()
        result = 31 * result + Arrays.hashCode(operations)
        return result
    }
}


data class Location(var coordinates: Array<Double> = arrayOf(0.0, 0.0), var distance: Double = 1.2) : Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Location

        if (!Arrays.equals(coordinates, other.coordinates)) return false

        return true
    }

    override fun hashCode(): Int {
        return Arrays.hashCode(coordinates)
    }
}

data class Social(var twitter: String = "", var facebook: String = "", var website: String = "") : Serializable

data class OperationTime(@SerializedName("Day") var day: String? = null, var from: String? = null, var to: String? = null) : Serializable

enum class ServiceStatus {
    ACTIVE, INACTIVE
}

data class DataSocial(var twitter: String = "", var facebook: String = "", var website: String = "")

data class DataService(@SerializedName("business_service_name") var name: String = "",
                       @SerializedName("service_id") var serverId: Int? = null,
                       @SerializedName("service_description") var description: String = "",
                       @SerializedName("created_date") var createdDate: Date = Date(),
                       @Embedded(prefix = "location_") @SerializedName("service_location") var location: Location = Location(),
                       var website: String = "",
                       @Embedded(prefix = "social_") @SerializedName("service_social") var social: DataSocial? = null,
                       @SerializedName("num_view") var numberOfViews: Int = 0,
                       var ranking: Int = 0,
                       var rating: Float = 3f,
                       var likes: Int = 0,
                       var status: String = "",
                       var lat: String = "",
                       var lng: String = "",
                       @SerializedName("service_sub_category_code") var subCategoryCode: String = "",
                       @SerializedName("service_contacts") var contact: String = "",
                       @SerializedName("service_email") var email: String = "",
                       @SerializedName("service_address") var address: String = "",
                       @SerializedName("location_name") var locationName: String = "Nairobi, Kenya",
                       @SerializedName("service_operations") var operations: Array<OperationTime> = arrayOf(),
                       @SerializedName("proficiency") var proficiencies: ArrayList<String> = arrayListOf())

data class ServiceImage(var path: String = ""): Serializable