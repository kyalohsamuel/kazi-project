package ke.co.kazi.model

import androidx.room.*
import com.google.gson.annotations.SerializedName

/**
 * {
"countries": [
{
"_id": "5b87aff50baf482dd3539373",
"created_by": "SYSTEM",
"country_name": "Kenya",
"country_code": "KEN",
"country_prefix": "+254",
"country_image_path": "/image/country/kenya.png",
"created_date": "2018-08-30T08:51:01.658Z",
"country_id": 1,
"__v": 0
}
],
"tnc": {
"_id": "5b87afdf4d45b97da372931c",
"name": "tnc",
"__v": 0,
"tnc": "This are our terms and conditions"
}
}
 */
@Entity(tableName = "settings")
data class Setting(
        @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) var settingId: Long? = null,
        @Embedded var tnc: TermsAndConditions? = null,
        @Ignore var countries: List<Country> = listOf()
)

@Entity(tableName = "countries")
data class Country(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var countryId: Long? = null,
        @SerializedName("country_name") var name: String = "",
        @SerializedName("country_code") var code: String = "",
        @SerializedName("country_prefix") var prefix: String = "",
        @SerializedName("country_image_path") var imagePath: String = "",
        @SerializedName("country_id") var countryServerid: Long? = null
) {
    override fun toString(): String = name
}

data class TermsAndConditions(
        @SerializedName("tnc") var value: String = ""
)