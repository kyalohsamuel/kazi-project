package ke.co.kazi.helper

import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.fieldError(error: String){
    this.error = error
    this.isErrorEnabled = true
}