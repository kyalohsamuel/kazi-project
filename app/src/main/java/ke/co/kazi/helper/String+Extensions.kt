package ke.co.kazi.helper

import android.util.Patterns

fun String.isEmail(): Boolean =
        this.isNotEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.isNotEmail(): Boolean =
        !this.isEmail()


fun String.requiredField() = "$this is a required field"