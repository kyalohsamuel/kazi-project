package ke.co.kazi.helper

import android.text.Editable
import android.text.TextWatcher
import com.google.android.material.textfield.TextInputLayout

class ClearErrorTextWatcher(private val textInputLayout: TextInputLayout): TextWatcher {
    override fun afterTextChanged(p0: Editable?) {
        textInputLayout.error = null
        textInputLayout.isErrorEnabled = false
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}
