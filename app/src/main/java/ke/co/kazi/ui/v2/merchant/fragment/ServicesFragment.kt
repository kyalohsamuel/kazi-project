package ke.co.kazi.ui.v2.merchant.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import ke.co.kazi.R
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.model.Service
import ke.co.kazi.ui.v2.adapter.ServicesAdapter
import ke.co.kazi.ui.v2.merchant.MerchantActivity
import ke.co.kazi.ui.v2.util.MarginItemDecoration
import ke.co.kazi.vo.Resource
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_merchant.*
import kotlinx.android.synthetic.main.fragment_merchant_services.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.dip


class ServicesFragment : ServiceFragment(), AnkoLogger {

    private val services = arrayListOf<Service>()
    private val servicesAdapter = ServicesAdapter(services)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_merchant_services, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    override fun onResume() {
        super.onResume()
        loadServices()
    }

    private fun setupViews() {
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.addItemDecoration(MarginItemDecoration(context?.dip(16)!!))
        recyclerView.adapter = servicesAdapter

        swipeRefreshLayout.setColorSchemeResources(R.color.radicalRed, R.color.softBlue)
        swipeRefreshLayout.setOnRefreshListener { loadServices() }
    }

    private fun loadServices() {
        viewModel.loadServices().observe(this, Observer { resource ->
            swipeRefreshLayout.isRefreshing = false
            when (resource.status) {
                Status.LOADING -> swipeRefreshLayout.isRefreshing = true
                Status.SUCCESS -> updateServices(resource)
                Status.FAILED -> handleNetworkError(resource, (activity as MerchantActivity).coordinatorLayout)
            }
        })
    }

    private fun updateServices(resource: Resource<List<Service>>) {
        services.clear()
        resource.data?.let { list ->
            for (service in list) {
                services.add(service)
            }
        }
        servicesAdapter.notifyDataSetChanged()
    }
}
