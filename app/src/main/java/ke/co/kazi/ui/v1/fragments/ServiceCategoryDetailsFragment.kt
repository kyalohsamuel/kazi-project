package ke.co.kazi.ui.v1.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import dagger.android.support.AndroidSupportInjection
import ke.co.kazi.GlideApp

import ke.co.kazi.R
import ke.co.kazi.databinding.FragmentServiceCategoryDetailsBinding
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.SUB_CATEGORY_CODE
import ke.co.kazi.ui.v1.adapter.ServiceSubCategoryRecyclerViewAdapter
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.ServiceCategoryViewModel
import kotlinx.android.synthetic.main.fragment_service_category_details.*
import org.jetbrains.anko.bundleOf
import timber.log.Timber
import javax.inject.Inject

const val CATEGORY_EXTRA_DATA = "CATEGORY_EXTRA_DATA"

class ServiceCategoryDetailsFragment : androidx.fragment.app.Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var categoryViewModel: ServiceCategoryViewModel
    private lateinit var binding: FragmentServiceCategoryDetailsBinding

    var category: ServiceCategory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            category = it.getSerializable(CATEGORY_EXTRA_DATA) as? ServiceCategory
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentServiceCategoryDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryViewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceCategoryViewModel::class.java]

        binding.service = category

        val serviceSubCategoryRecyclerViewAdapter = ServiceSubCategoryRecyclerViewAdapter(object : ServiceSubCategoryRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: ServiceSubCategory, view: View) {

                Timber.e("Click sub category: $item")

                val bundle = bundleOf(SUB_CATEGORY_CODE to item, CATEGORY_EXTRA_DATA to category)
                Navigation.findNavController(view).navigate(R.id.action_serviceCategoryDetailsFragment_to_newServiceFragment, bundle)
//                if (forward == true) {
//                    startActivity<ServicesListActivity>(Pair(SUB_CATEGORY_CODE, item))
//                } else {
//                    val intent = Intent()
//                    intent.putExtra(SUB_CATEGORY_CODE, item)
//                    setResult(Activity.RESULT_OK, intent)
//                    finish()
//                }
            }
        })

        with(list){
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = mLayoutManager
            adapter = serviceSubCategoryRecyclerViewAdapter
        }

        categoryViewModel.loadServiceSubCategories(category?.categoryCode
                ?: "").reObserve(this, Observer {
            it?.data.apply {
                serviceSubCategoryRecyclerViewAdapter.submitList(this)
            }
        })

        GlideApp.with(this)
                .load("http://45.79.18.37:8080"+category?.imageUrl)
                .fitCenter()
                .into(binding.imageCat)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}
