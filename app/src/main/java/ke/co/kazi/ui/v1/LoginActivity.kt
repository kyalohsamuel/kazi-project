package ke.co.kazi.ui.v1

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.models.data.LoginData
import ke.co.kazi.databinding.ActivityLoginBinding
import ke.co.kazi.notification.KaziSocket
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.LoginViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import timber.log.Timber
import javax.inject.Inject


const val INTENT_USER_PHONE_NUMBER = "phone_number"
const val INTENT_PROFILE_IMAGE_URL = "INTENT_PROFILE_IMAGE_URL"
const val INTENT_USER_COUNTRY_CODE = "INTENT_USER_COUNTRY_CODE"

class LoginActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private lateinit var viewModel: LoginViewModel

    lateinit var binding: ActivityLoginBinding

    private val loginData = LoginData()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[LoginViewModel::class.java]

        loginData.username = intent?.getStringExtra(INTENT_USER_PHONE_NUMBER) ?: ""
        loginData.profileImageUrl = intent?.getStringExtra(INTENT_PROFILE_IMAGE_URL) ?: ""

        binding.data = loginData

//        binding.signUp.setOnClickListener {
//            startActivity(intentFor<RegisterActivity>().putExtra(INTENT_USER_PHONE_NUMBER, loginData.username))
//        }
        subscribeUi()

        binding.btnForgotPassword2.setOnClickListener {
            startActivity(intentFor<GetOtpActivity>()
                    .putExtra(INTENT_USER_PHONE_NUMBER, loginData.username)
                    .putExtra(INTENT_IS_RESET_PASSWORD, true)
            )
        }
    }

    fun onBackPressed(view: View) {
        finish()
    }

    fun onSignIn(view: View) {
        if (validate()) viewModel.loginAction(loginData)
    }

    private fun subscribeUi() {
        viewModel.userDetails.reObserve(this, Observer { resource ->
            resource?.let {
                binding.resource = it

                Timber.e("Response: ${resource.data} + ' ' + ${resource.status} + ' ' + ${resource.message}")
            }

            resource?.data?.let { loginResponse ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        loginResponse.data.apply {
                            viewModel.saveUser(this, loginResponse.token).observe(this@LoginActivity, Observer {
                                binding.resource = it
                                it?.data?.let { userId ->
                                    Timber.e("User saved: $userId")
                                    KaziSocket.init(sharedPreferences)
                                    this@LoginActivity.startActivity<MainDrawerActivity>()
                                    finishAffinity()
                                }
                            })
                        }
                    }
                    Status.FAILED -> Toast.makeText(this, "Invalid Credentials. Please try again!", Toast.LENGTH_LONG).show()
                    Status.LOADING -> {
                    }
                }
            }
        })
    }

    private fun validate(): Boolean {
        return when {
            TextUtils.isEmpty(loginData.password) -> {
                textPassword.error = "Enter password"
                false
            }
            else -> true
        }
    }

}