package ke.co.kazi.ui.v1

import android.Manifest
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.pm.PackageManager
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.models.data.MobileCheckRequest
import ke.co.kazi.databinding.ActivityCheckMobileBinding
import ke.co.kazi.model.Country
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.LoginViewModel
import ke.co.kazi.vo.LoginStatus
import kotlinx.android.synthetic.main.activity_check_mobile.*
import org.jetbrains.anko.*
import timber.log.Timber
import javax.inject.Inject

class CheckMobileActivity : AppCompatActivity(), AnkoLogger {

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private val CAPTURE_PIN = 1

    private lateinit var viewModel: LoginViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityCheckMobileBinding

    private val mobileCheckRequest = MobileCheckRequest()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val decorView: View = window.decorView
        decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN

        binding = DataBindingUtil.setContentView(this, R.layout.activity_check_mobile)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[LoginViewModel::class.java]
        binding.mobileCheckRequest = mobileCheckRequest

//        requestReadPhoneStatePermissions()

//        binding.imgLogo.setImageDrawable(getDrawableRes(R.drawable.logo, this).changeColor(R.color.colorPrimary))

        viewModel.loadCountries().reObserve(this, Observer {
            it?.let {
                binding.resource = it
            }
            it?.data?.let {
                it.forEach { t: Country? -> Timber.e("Country: ${t?.code}") }
                binding.spinner.adapter = ArrayAdapter<Country>(this, R.layout.spinner_item, it)
            }
        })

        subscribeUI()

//        binding.signUp.setOnClickListener {
//            startActivity(intentFor<RegisterActivity>())
//        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            READ_PHONE_PERM -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    requestReadPhoneStatePermissions()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

    private fun subscribeUI() {
        viewModel.mobileCheckResponse.reObserve(this, Observer { responseResource ->
            responseResource?.let {
                error("User details loaded: ${it.data}")
                binding.resource = it
                when (responseResource.data?.code) {
                    LoginStatus.NEW_USER.value -> {
                        startActivity(intentFor<GetOtpActivity>()
                                .putExtra(INTENT_USER_PHONE_NUMBER, mobileCheckRequest.mobileNumber)
                                .putExtra(INTENT_USER_COUNTRY_CODE, mobileCheckRequest.countryCode))
                    }
                    LoginStatus.USER_FOUND.value -> {
                        sharedPreferences.userProfileImage = responseResource.data?.profileImageUrl
                                ?: ""
                        startActivity(intentFor<LoginActivity>()
                                .putExtra(INTENT_USER_PHONE_NUMBER, mobileCheckRequest.mobileNumber)
                                .putExtra(INTENT_PROFILE_IMAGE_URL, responseResource.data?.profileImageUrl
                                        ?: "")
                                .putExtra(INTENT_USER_COUNTRY_CODE, mobileCheckRequest.countryCode))
                    }

                    else -> {
                        error("Code not specified")
                    }
                }
            }
        })

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>, v: View?, pos: Int, id: Long) {
                if (pos == Spinner.INVALID_POSITION) {
                    return
                }

                Timber.e("Country Selected: ${(parent.getItemAtPosition(pos) as Country).code}")
                mobileCheckRequest.countryCode = (parent.getItemAtPosition(pos) as Country).code

                Timber.e("Request: $mobileCheckRequest")
            }
        }

    }

    fun onButtonContinueClick(view: View) {
        binding.textPhoneNumber.error = null
        when {
            validate() -> viewModel.checkMobileNumberStatus(mobileCheckRequest)
        }
    }

    private fun validate(): Boolean {
        return when {
            TextUtils.isEmpty(mobileCheckRequest.mobileNumber) -> {
                binding.textPhoneNumber.error = "Valid phone number required."
                false
            }
            mobileCheckRequest.countryCode.isEmpty() -> {
                longToast("Select a country.")
                false
            }
            else -> true
        }
    }

    private fun requestReadPhoneStatePermissions() {
        Timber.e("Checking permissions")



        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_PHONE_STATE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_PHONE_STATE),
                        READ_PHONE_PERM)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
//            FirebaseMessaging.getInstance().isAutoInitEnabled = true
        }

    }
}

private const val READ_PHONE_PERM = 9089