package ke.co.kazi.ui.v1.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.PlaceDetectionClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import dagger.android.support.AndroidSupportInjection

import ke.co.kazi.R
import ke.co.kazi.databinding.FragmentServiceMapBinding
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.adapter.NewServicesRecyclerViewAdapter
import ke.co.kazi.ui.v1.services.Constants
import ke.co.kazi.util.SERVICE_EXTRA
import ke.co.kazi.viewmodel.ServiceViewModel
import kotlinx.android.synthetic.main.fragment_service_map.*
import org.jetbrains.anko.bundleOf
import timber.log.Timber
import javax.inject.Inject

private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
private const val DEFAULT_ZOOM = 15F

class ServiceMapFragment : androidx.fragment.app.Fragment(), OnMapReadyCallback {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentServiceMapBinding
    private lateinit var viewModel: ServiceViewModel

    var serviceSubCategory: ServiceSubCategory? = null

    private lateinit var mMap: GoogleMap
    private lateinit var mGeoDataClient: GeoDataClient
    private lateinit var mPlaceDetectionClient: PlaceDetectionClient
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var mCameraPosition: CameraPosition
    private lateinit var resultReceiver: ResultReceiver

    private var mLastKnownLocation: Location? = null
    private var mLocationPermissionGranted: Boolean = false
    private val mDefaultLocation = LatLng(1.2921, 36.8219)
    private var mServiceLocation: Location? = null

    private var selectedPlaceMarker: Marker? = null

    override fun onMapReady(googleMap: GoogleMap) {
        Timber.e("Map ready")
        mMap = googleMap

        updateLocationUI()
        getDeviceLocation()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            serviceSubCategory = it.getSerializable(SUB_CATEGORY_CODE) as? ServiceSubCategory
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentServiceMapBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]

        mGeoDataClient = Places.getGeoDataClient(activity!!)
        mPlaceDetectionClient = Places.getPlaceDetectionClient(activity!!)
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!)
        resultReceiver = AddressResultReceiver(Handler())

        initMap()

        val bundle = bundleOf(SUB_CATEGORY_CODE to serviceSubCategory)
        btnSwitchToList.setOnClickListener (Navigation.createNavigateOnClickListener(R.id.action_serviceMapFragment_to_newServiceFragment, bundle))

        val servicesRecyclerViewAdapter = NewServicesRecyclerViewAdapter(1, object : NewServicesRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: Service) {
//                startActivity<ServiceDetailsActivity>(Pair(SERVICE_EXTRA, item))

                Timber.e("Click service: $item")

                val serviceBundle = bundleOf(SERVICE_EXTRA to item)
                Navigation.findNavController(view).navigate(R.id.action_serviceMapFragment_to_serviceDetailFragment, serviceBundle)
            }

        })

        with(list){
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context, androidx.recyclerview.widget.RecyclerView.HORIZONTAL, false)
            layoutManager = mLayoutManager
            adapter = servicesRecyclerViewAdapter
        }

        serviceSubCategory?.let { serviceSubCategory ->
            viewModel.loadServicesBySubCategory(serviceSubCategory.categoryCode).observe(this, Observer { resource ->
                resource?.data?.let {
                    servicesRecyclerViewAdapter.submitList(it)
                }
            })
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }

    private fun initMap() {
        val mapFragment = childFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)
    }

    private fun updateLocationUI() {
        try {
            if (mLocationPermissionGranted) {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                mLastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Timber.e("Exception: ${e.message}")
        }

    }

    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(context!!,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(activity!!,
                    arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                val locationResult: Task<Location> = mFusedLocationProviderClient.lastLocation

                locationResult.addOnCompleteListener(activity!!) { task ->
                    if (task.isSuccessful) {
                        Timber.e("${task.result}")
                        mLastKnownLocation = task.result

                        mLastKnownLocation?.let { location ->

                            mServiceLocation = location
//                            FetchAddressIntentService.start(context!!, resultReceiver, mServiceLocation!!)
                            Timber.e("Location not null")
                            val lastKnownLatLong = LatLng(location.latitude, location.longitude)

                            selectedPlaceMarker = mMap.addMarker(MarkerOptions().position(lastKnownLatLong).title("Current Location"))

                            // Set the map's camera position to the current location of the device.
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(location.latitude, location.longitude), DEFAULT_ZOOM))
                        }

                    } else {
                        Timber.e("Current location is null. Using defaults.")
                        Timber.e("Exception: ${task.exception}")
                        mMap.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM))
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Timber.e("Exception: ${e.message}")
        }
    }

    inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {

            // Display the address string
            // or an error message sent from the intent service.
            val addressOutput = resultData?.getString(Constants.RESULT_DATA_KEY) ?: ""
//            displayAddressOutput()

            // Show a toast message if an address was found.
            if (resultCode == Constants.SUCCESS_RESULT) {
                Timber.e(getString(R.string.address_found))
                Timber.e("Found address: $addressOutput")

//                textLocation.text = Editable.Factory().newEditable(addressOutput)
            } else {
                Timber.e(getString(R.string.no_address_found))
                Timber.e("Address not found: $addressOutput")
            }

        }
    }
}
