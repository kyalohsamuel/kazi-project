package ke.co.kazi.ui.v2.onboarding

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import ke.co.kazi.ui.v2.dashboard.DashboardActivity
import ke.co.kazi.ui.v2.merchant.MerchantActivity
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.SettingViewModel
import ke.co.kazi.viewmodel.UserProfileViewModel
import ke.co.kazi.vo.Status
import org.jetbrains.anko.*
import timber.log.Timber
import javax.inject.Inject


class SplashActivity: AppCompatActivity(){

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private lateinit var viewModel: SettingViewModel
    private lateinit var userViewModel: UserProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setupProgressBar()
        loadSettings()
    }

    private fun loadSettings() {
        viewModel = ViewModelProviders.of(this, viewModelFactory)[SettingViewModel::class.java]
        userViewModel = ViewModelProviders.of(this, viewModelFactory)[UserProfileViewModel::class.java]

        viewModel.loadSettings().reObserve(this, Observer { response ->
            response?.let {
                when (it.status) {
                    Status.SUCCESS -> navigate()
                    Status.LOADING -> {}
                    Status.FAILED -> longToast("Error opening application. Check your network connection.")
                }
            }
        })
    }

    private fun setupProgressBar() {
        val layout = RelativeLayout(this)

        val progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleLarge)
        progressBar.isIndeterminate = true
        progressBar.visibility = View.VISIBLE

        val params = RelativeLayout.LayoutParams(100, 100)
        params.addRule(RelativeLayout.CENTER_HORIZONTAL)
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        params.bottomMargin = dip(80)
        layout.addView(progressBar, params)

        setContentView(layout)
    }

    private fun navigate() {
        userViewModel.fetchUser().observe(this, Observer { resource ->
            when(resource.status){
                Status.LOADING -> {}
                Status.SUCCESS -> {
                    Timber.e("User: ${resource.data}")
                    when{
                        resource.data == null -> startActivity(intentFor<OnBoardingActivity>()
                                .clearTop().clearTask().newTask())
                        else -> navigate(resource.data?.userType!!)
                    }
                }
                Status.FAILED -> longToast("Error opening application. Check your network connection.")
            }
        })

        //Logged in Client
//        startActivity(Intent(this, DashboardActivity::class.java)
//                .clearTop().clearTask().newTask())
    }

    private fun navigate(userType: String) {
        when(userType){
            "CLIENT" -> intentFor<DashboardActivity>().clearTask().clearTop().newTask()
            "SP" -> intentFor<MerchantActivity>().clearTask().clearTop().newTask()
            else -> {
                longToast("Unknown User Type. Contact System Admin")
                null
            }
        }?.let{
            startActivity(it)
        }
    }
}