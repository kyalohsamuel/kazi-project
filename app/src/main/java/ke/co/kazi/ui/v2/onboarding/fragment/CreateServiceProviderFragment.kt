package ke.co.kazi.ui.v2.onboarding.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.support.AndroidSupportInjection
import ke.co.kazi.viewmodel.ServiceViewModel
import javax.inject.Inject

abstract class CreateServiceProviderFragment: Fragment(){

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory
    protected lateinit var viewModel: ServiceViewModel
    protected var loading = false
        set(value) {
            isLoading(value)
            field = value
        }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ServiceViewModel::class.java)
    }

    abstract fun isLoading(loading: Boolean)
}