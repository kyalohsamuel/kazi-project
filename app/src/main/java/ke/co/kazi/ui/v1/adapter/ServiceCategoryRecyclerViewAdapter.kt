package ke.co.kazi.ui.v1.adapter


import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ke.co.kazi.GlideApp
import ke.co.kazi.binding.BindingConverter
import ke.co.kazi.databinding.ListItemServiceCategoryBinding
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.ui.v1.adapter.callback.MyServiceDiffCallback
import timber.log.Timber

/**
 * [RecyclerView.Adapter] that can display a [ServiceCategory]
 */
class ServiceCategoryRecyclerViewAdapter(val context: Context, val onItemSelectedListener: OnItemSelectedListener) : ListAdapter<ServiceCategory, ServiceCategoryRecyclerViewAdapter.ViewHolder>(MyServiceDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListItemServiceCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(val binding: ListItemServiceCategoryBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ServiceCategory) {
            Timber.e("Service category: $item")
            binding.root.setOnClickListener { onItemSelectedListener.onItemSelected(item, binding.root) }
            binding.service = item
            binding.converter = BindingConverter()

            GlideApp.with(context)
                    .load("http://45.79.18.37:8080"+item.imageUrl)
                    .fitCenter()
                    .into(binding.imageCat)

            binding.executePendingBindings()
        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(item: ServiceCategory, view: View)
    }
}
