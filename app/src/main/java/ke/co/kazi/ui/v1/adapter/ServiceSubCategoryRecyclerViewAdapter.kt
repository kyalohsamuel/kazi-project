package ke.co.kazi.ui.v1.adapter


import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ke.co.kazi.GlideApp
import ke.co.kazi.databinding.ListItemServiceSubCategoriesBinding
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.adapter.callback.ServiceSubCategoriesDiffCallback

/**
 * [RecyclerView.Adapter] that can display a [ServiceCategory]
 */
class ServiceSubCategoryRecyclerViewAdapter(val onItemSelectedListener: OnItemSelectedListener) : ListAdapter<ServiceSubCategory, ServiceSubCategoryRecyclerViewAdapter.ViewHolder>(ServiceSubCategoriesDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListItemServiceSubCategoriesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(parent.context, binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(val context: Context, val binding: ListItemServiceSubCategoriesBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ServiceSubCategory) {
            binding.root.setOnClickListener { onItemSelectedListener.onItemSelected(item, binding.root) }
            binding.category = item

            GlideApp.with(context)
                    .load("http://45.79.18.37:8080"+item.imageUrl)
                    .fitCenter()
                    .into(binding.imageCat)

            binding.executePendingBindings()
        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(item: ServiceSubCategory, view: View)
    }
}
