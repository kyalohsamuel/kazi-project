package ke.co.kazi.ui.v1

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.models.data.MobileCheckRequest
import ke.co.kazi.api.models.data.ValidateOtpData
import ke.co.kazi.databinding.ActivityGetOtpBinding
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.LoginViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_get_otp.*
import org.jetbrains.anko.intentFor
import timber.log.Timber
import javax.inject.Inject

const val INTENT_IS_RESET_PASSWORD = "INTENT_IS_RESET_PASSWORD"

class GetOtpActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityGetOtpBinding

    private val mobileCheckRequest = MobileCheckRequest()
    private var isResetPassword = false

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_get_otp)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[LoginViewModel::class.java]

        isResetPassword = intent?.getBooleanExtra(INTENT_IS_RESET_PASSWORD, false) ?: false
        mobileCheckRequest.mobileNumber = intent?.getStringExtra(INTENT_USER_PHONE_NUMBER) ?: ""
        mobileCheckRequest.countryCode = intent?.getStringExtra(INTENT_USER_COUNTRY_CODE) ?: ""
        binding.data = mobileCheckRequest


        viewModel.requestOtp.reObserve(this, Observer {
            binding.resource = it
            Timber.e("Otp requested")
        })

        if (!TextUtils.isEmpty(mobileCheckRequest.mobileNumber)) {
            viewModel.requestOtp(mobileNumber = mobileCheckRequest.mobileNumber)
        } else {
            Timber.e("Invalid mobile phone")
        }

        binding.buttonSubmit.setOnClickListener { _ ->
            text_otp.error = null
            if (TextUtils.isEmpty(text_otp.text.toString())) {
                text_otp.error = "Enter otp."
                return@setOnClickListener
            }

            viewModel.validateOtp(ValidateOtpData(text_otp.text.toString(), mobileCheckRequest.mobileNumber))
                    .observe(this@GetOtpActivity, Observer { resource ->
                        binding.resource = resource
                        Timber.e("Valid Otp")
                        when (resource?.status) {
                            Status.SUCCESS -> {
                                if (isResetPassword) {
                                    startActivity(intentFor<ForgotPasswordActivity>().putExtra(INTENT_USER_PHONE_NUMBER, mobileCheckRequest.mobileNumber))
                                } else {
                                    startActivity(intentFor<RegisterActivity>()
                                            .putExtra(INTENT_USER_PHONE_NUMBER, mobileCheckRequest.mobileNumber)
                                            .putExtra(INTENT_USER_COUNTRY_CODE, mobileCheckRequest.countryCode))
                                }
                                finish()
                            }
                            Status.FAILED -> Toast.makeText(this, "Invalid Credentials. Please try again!", Toast.LENGTH_LONG).show()
                            Status.LOADING -> {
                            }
                        }
                    })
        }
    }

}
