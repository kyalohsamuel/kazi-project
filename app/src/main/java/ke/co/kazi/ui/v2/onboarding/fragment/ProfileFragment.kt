package ke.co.kazi.ui.v2.onboarding.fragment


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.api.models.data.LoginData
import ke.co.kazi.api.models.data.ProviderType
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.model.User
import ke.co.kazi.ui.v2.dashboard.DashboardActivity
import ke.co.kazi.ui.v2.onboarding.CreateServiceProviderActivity
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.FileUploadUtil
import ke.co.kazi.viewmodel.ServiceViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import timber.log.Timber
import java.io.File
import javax.inject.Inject


class ProfileFragment : OnBoardingFragment() {

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences
    lateinit var serviceViewModel: ServiceViewModel

    private lateinit var user: User
    private lateinit var token: String

    private val args: ProfileFragmentArgs by navArgs()

    private var imageUri: Uri? = null
        set(value) {
            setCircleImageView(value)
            field = value
        }
    private var loading: Boolean = false
        set(value){
            isLoading(value)
            field = value
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                when (resultCode) {
                    RESULT_OK -> imageUri = result.uri
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> Timber.e(result.error)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(ke.co.kazi.R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        login()
        serviceViewModel = ViewModelProviders.of(this, viewModelFactory).get(ServiceViewModel::class.java)

        circleImageView.setOnClickListener {
            CropImage.activity()
                    .setAspectRatio(1,1)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(context!!, this) }
        setupServiceProviderListeners()
        proceedButton.setOnClickListener { uploadImage() }
    }

    private fun login() {
        viewModel.login(LoginData(args.pin, args.phoneNumber)).observe(this, Observer { response ->
            loading = false
            when(response.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> {
                    response.data?.data?.let { user ->
                        viewModel.saveUser(user, response?.data?.token!!).observe(this, Observer { resource ->
                            loading = false
                            when(resource.status){
                                Status.LOADING -> loading = true
                                Status.SUCCESS -> {}
                                Status.FAILED -> handleNetworkError(resource)
                            }
                        })
                    }
                }
                Status.FAILED -> login()
            }
        })
    }

    private fun uploadImage() {
        if(!validate()) return
        uploadImage(imageUri!!)
    }

    private fun uploadImage(uri: Uri) {
        viewModel.uploadProfilePhoto(FileUploadUtil.prepareFilePart("image", File(uri.path), context!!))
                .observe(this, Observer { response ->
                    loading = false
                    when(response.status){
                        Status.LOADING -> loading = true
                        Status.SUCCESS -> createServiceProvider()
                        Status.FAILED -> handleNetworkError(response)
                    }
                })
    }

    private fun createServiceProvider() {
        when(serviceProviderRadioGroup.checkedRadioButtonId == yesRadioButton.id){
            true -> {
                val type = when(providerTypeRadioGroup.checkedRadioButtonId){
                    companyRadioButton.id -> ProviderType.TYPE_COMPANY
                    individualRadioButton.id -> ProviderType.TYPE_INDIVIDUAL
                    else -> throw IllegalStateException()
                }

                serviceViewModel.createServiceProvider(type).observe(this, Observer { response ->
                    loading = false
                    when(response.status){
                        Status.LOADING -> loading = true
                        Status.SUCCESS -> navigate(type)
                        Status.FAILED -> handleNetworkError(response)
                    }
                })
            }
            false ->
                startActivity(context?.intentFor<DashboardActivity>()?.clearTask()?.clearTop()?.newTask())
        }
    }

    private fun navigate(type: String) {
        val flow = when(type){
            ProviderType.TYPE_COMPANY -> CreateServiceProviderActivity.FLOW_INDIVIDUAL
            ProviderType.TYPE_INDIVIDUAL -> CreateServiceProviderActivity.FLOW_COMPANY
            else -> throw IllegalStateException()
        }
        CreateServiceProviderActivity.startCreateServiceProviderActivity(context, flow)
    }

    private fun setupServiceProviderListeners() {
        serviceProviderRadioGroup.setOnCheckedChangeListener { _, checkedId ->
            activateProfileType(checkedId == yesRadioButton.id)
        }
    }

    private fun activateProfileType(activate: Boolean) {
        companyRadioButton.isEnabled = activate
        individualRadioButton.isEnabled = activate
    }

    private fun setCircleImageView(uri: Uri?) {
        GlideApp.with(context!!)
                .load(uri)
                .error(R.drawable.img_profile_placeholder_100dp)
                .into(circleImageView)

    }

    private fun validate(): Boolean {
        var valid = true
        when(serviceProviderRadioGroup.checkedRadioButtonId == yesRadioButton.id){
            true -> {
//                when(!companyRadioButton.isChecked || !individualRadioButton.isChecked){
//                    true -> {
//                        Toast.makeText(context, getString(R.string.error_select_provider),Toast.LENGTH_SHORT).show()
//                        valid = false
//                    }
//                }
            }
        }

        if(!valid) return valid

        when (imageUri == null) {
            true -> {
                Toast.makeText(context, getString(R.string.error_pick_image), Toast.LENGTH_SHORT).show()
                valid = false
            }
        }

        return valid
    }

    private fun isLoading(loading: Boolean) {
        proceedButton.text = when(loading){ true -> getString(R.string.action_saving) false -> getText(R.string.action_proceed) }
        progressBar.visibility = when(loading){ true -> View.VISIBLE false -> View.GONE }
        pointedArrowImageView.visibility = when(loading){ true -> View.GONE false -> View.VISIBLE }
    }
}
