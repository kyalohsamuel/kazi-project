package ke.co.kazi.ui.v1.adapter.callback

import androidx.recyclerview.widget.DiffUtil
import ke.co.kazi.model.Message
import timber.log.Timber

class MessageDiffCallback : DiffUtil.ItemCallback<Message>() {
    override fun areItemsTheSame(oldItem: Message, newItem: Message): Boolean {
        return oldItem?.id == newItem?.id
    }

    override fun areContentsTheSame(oldItem: Message, newItem: Message): Boolean {
        return oldItem == newItem
    }
}