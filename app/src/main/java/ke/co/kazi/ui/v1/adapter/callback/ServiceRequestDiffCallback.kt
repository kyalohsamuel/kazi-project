package ke.co.kazi.ui.v1.adapter.callback

import androidx.recyclerview.widget.DiffUtil
import ke.co.kazi.model.ServiceRequest

class ServiceRequestDiffCallback : DiffUtil.ItemCallback<ServiceRequest>() {
    override fun areItemsTheSame(oldItem: ServiceRequest, newItem: ServiceRequest): Boolean {
        return oldItem?.requestId == newItem?.requestId
    }

    override fun areContentsTheSame(oldItem: ServiceRequest, newItem: ServiceRequest): Boolean {
        return oldItem == newItem
    }
}