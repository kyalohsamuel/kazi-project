package ke.co.kazi.ui.v1

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.databinding.ActivityInitBinding
import ke.co.kazi.ui.v2.onboarding.CreateServiceProviderActivity
import ke.co.kazi.ui.v2.onboarding.OnBoardingActivity
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.SettingViewModel
import ke.co.kazi.vo.Status.*
import kotlinx.coroutines.*
import org.jetbrains.anko.longToast
import javax.inject.Inject

class InitActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: SettingViewModel

    private lateinit var binding: ActivityInitBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_init)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[SettingViewModel::class.java]

        viewModel.loadSettings().reObserve(this, Observer {

            it?.let {
                when (it.status) {
                    SUCCESS -> {
                        GlobalScope.launch(Dispatchers.Default, CoroutineStart.DEFAULT) {
                            delay(3000)
                            CreateServiceProviderActivity.startCreateServiceProviderActivity(this@InitActivity, CreateServiceProviderActivity.FLOW_INDIVIDUAL)
//                            startActivity(Intent(this@InitActivity, OnBoardingActivity::class.java))
                            finish()
                        }
                    }
                    LOADING -> {
                    }
                    FAILED -> {
                        longToast("Error opening application. Contact Admin.")
                        GlobalScope.launch(Dispatchers.Default, CoroutineStart.DEFAULT) {
                            delay(3000)
                            finishAffinity()
                        }
                    }
                }
            }
        })
    }

}
