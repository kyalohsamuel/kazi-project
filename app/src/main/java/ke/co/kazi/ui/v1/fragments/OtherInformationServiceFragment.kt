package ke.co.kazi.ui.v1.fragments


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.navigation.findNavController
import com.google.android.material.chip.Chip
import ke.co.kazi.R
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.model.OperationTime
import ke.co.kazi.model.Service
import ke.co.kazi.model.Social
import ke.co.kazi.ui.v2.merchant.fragment.ServiceViewFragment
import ke.co.kazi.util.toEditable
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_other_information.*
import org.jetbrains.anko.bundleOf
import timber.log.Timber
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class OtherInformationServiceFragment : ServiceViewFragment() {

    companion object {
        fun newInstance(serviceID: Int): OtherInformationServiceFragment {
            return OtherInformationServiceFragment().apply {
                arguments = bundleOf(ARG_SERVICE_ID to serviceID)
            }
        }
    }

    private var times: ArrayList<String> = arrayListOf()
    private val proficiencies: ArrayList<String> = arrayListOf()

    private var startWeekdayTime: String = ""
    private var endWeekdayTime: String = ""
    private var startWeekendTime: String = ""
    private var endWeekendTime: String = ""

    private val dateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
    private val serverDateFormat = SimpleDateFormat("h:mm a", Locale.getDefault())


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_other_information, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupTimeSpinners()
        setupProficiencyInput()
        nextButton.setOnClickListener { persist() }
        saveChangesButton.setOnClickListener { saveChanges() }
    }

    override fun isLoading(loading: Boolean) {
        super.isLoading(loading)
        when(loading){
            true -> {
                saveChangesButton.text = null
                progressBar.visibility = View.VISIBLE
            }
            false -> {
                saveChangesButton.text = getText(R.string.action_save_changes)
                progressBar.visibility = View.GONE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when(service == null){
            true -> {
                nextButton.visibility = View.VISIBLE
                saveChangesButton.visibility = View.GONE
            }
            false -> {
                nextButton.visibility = View.GONE
                saveChangesButton.visibility = View.VISIBLE
            }
        }
    }

    override fun setupViews() {
        super.setupViews()

        nextButton.visibility = View.GONE
        saveChangesButton.visibility = View.VISIBLE
    }


    private fun setupTimeSpinners() {
        times.clear()
        times.add("Closed")

        for (i in 0..23) {
            times.add("$i:00")
            times.add("$i:30")
        }
        times.add("")

        val adapter = ArrayAdapter<String>(context!!, android.R.layout.simple_list_item_1, times)

        startWeekdaySpinner.adapter = adapter
        startWeekendSpinner.adapter = adapter
        endWeekdaySpinner.adapter = adapter
        endWeekendSpinner.adapter = adapter

        startWeekdaySpinner.setSelection(19)
        endWeekdaySpinner.setSelection(35)

        startWeekendSpinner.setSelection(21)
        endWeekendSpinner.setSelection(29)

        listOf(startWeekdaySpinner, endWeekdaySpinner).forEach {
            it.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    selectedClosed(position, startWeekdaySpinner, endWeekdaySpinner)
                }
            }
        }

        listOf(startWeekendSpinner, endWeekendSpinner).forEach {
            it.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    selectedClosed(position, startWeekendSpinner, endWeekendSpinner)
                }
            }
        }
    }

    private fun selectedClosed(position: Int, startSpinner: Spinner, endSpinner: Spinner) {
        when (position == 0 || position == times.count() - 1) {
            true -> {
                startSpinner.setSelection(0)
                endSpinner.setSelection(times.count() - 1)
                endSpinner.isEnabled = false
            }
            false -> {
                endSpinner.isEnabled = true
            }
        }
    }

    private fun setupProficiencyInput() {
        proficiencyEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                when (s?.contains(",", true)) {
                    true -> createProficiency(s.toString())
                }
            }
        })

        proficiencyEditText.setOnEditorActionListener { _, actionId, event ->
            when(actionId == EditorInfo.IME_ACTION_DONE){
                true -> {
                    Timber.e("Event: $event")
                    val proficiency = proficiencyEditText.text.toString()
                    if(proficiency.isNotEmpty()){
                        createProficiency(proficiency)
                    }
                    return@setOnEditorActionListener true
                }
            }

            return@setOnEditorActionListener false
        }
    }

    private fun createProficiency(new: String) {
        val proficiency = new.removeSuffix(",")

        if(proficiencies.contains(proficiency)) {
            proficiencyEditText.text = proficiency.toEditable()
            Toast.makeText(context, "Proficiency already added", Toast.LENGTH_SHORT).show()
            return
        }

        if (proficiency.isNotEmpty()) {
            val chip = LayoutInflater.from(context).inflate(R.layout.layout_chip, chipGroup, false) as Chip
            chip.text = proficiency
            chip.isCloseIconVisible = true

            // necessary to get single selection working
            chip.isClickable = true
            chip.isCheckable = false

            proficiencies.add(proficiency)

            chipGroup.addView(chip as View)
            chip.setOnCloseIconClickListener {
                proficiencies.removeAt(chipGroup.indexOfChild(it))
                chipGroup.removeView(chip as View)
            }
        }

        proficiencyEditText.text = null
    }

    private fun persist(){
        if(!validate()){ return }

        val serviceName = arguments?.getString("serviceName") ?: ""
        val serviceDesc = arguments?.getString("serviceDesc") ?: ""
        val categoryCode = arguments?.getString("categoryCode") ?: ""
        val mobileNo = arguments?.getString("mobileNo") ?: ""
        val address = arguments?.getString("address") ?: ""
        val lat = arguments?.getString("lat") ?: ""
        val lng = arguments?.getString("lng") ?: ""
        val facebook = arguments?.getString("facebook") ?: ""
        val twitter = arguments?.getString("twitter") ?: ""
        val website = arguments?.getString("website") ?: ""

        val weekdayTimes = OperationTime("Weekday", startWeekdayTime, endWeekdayTime)
        val weekendTimes = OperationTime("Weekend", startWeekendTime, endWeekendTime)

        val social = Social(twitter, facebook, website)

        val service = Service(
                name = serviceName,
                description = serviceDesc,
                subCategoryCode = categoryCode,
                contact = mobileNo,
                address = address,
                lat = lat,
                lng = lng,
                social = social,
                operations = arrayOf(weekdayTimes, weekendTimes)
        )

        viewModel.createService(service).observe(this, androidx.lifecycle.Observer{ resource ->
            loading = false
            when(resource.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> navigate(resource.data?.serverId!!)
                Status.FAILED -> handleNetworkError(resource)
            }
        })
    }

    private fun navigate(serviceId: Int) {
        val action = OtherInformationServiceFragmentDirections
                .actionOtherInformationServiceFragmentToServiceImageUploadFragment(serviceId.toString())
        view?.findNavController()?.navigate(action)
    }

    private fun validate(): Boolean{
        var valid: Boolean

        valid = validateDates()

        when(proficiencies.size < 3){
            true -> {
                Toast.makeText(context, "Proficiencies need to be at least three", Toast.LENGTH_LONG).show()
                valid = false
            }
        }

        return valid
    }

    private fun validateDates(): Boolean {
        var valid = true

        var startDate: Date? = null
        var endDate: Date? = null
        var startWeekendDate: Date? = null
        var endWeekendDate: Date? = null

        when (startWeekdaySpinner.selectedItemPosition != 0) {
            true -> {
                try {
                    startDate = dateFormat.parse(times[startWeekdaySpinner.selectedItemPosition])
                    endDate = dateFormat.parse(times[endWeekdaySpinner.selectedItemPosition])
                } catch (e: ParseException) {
                    valid = false
                    Toast.makeText(context, "Select both start and end weekday hours", Toast.LENGTH_LONG).show()
                    Timber.e(e)
                }
            }
        }

        when (startWeekendSpinner.selectedItemPosition != 0) {
            true -> {
                try {
                    startWeekendDate = dateFormat.parse(times[startWeekendSpinner.selectedItemPosition])
                    endWeekendDate = dateFormat.parse(times[endWeekendSpinner.selectedItemPosition])
                } catch (e: ParseException) {
                    valid = false
                    Toast.makeText(context, "Select both start and end weekend hours", Toast.LENGTH_LONG).show()
                    Timber.e(e)
                }
            }
        }

        when {
            startDate?.after(endDate) == true -> {
                valid = false
                Toast.makeText(context, "Weekday Start Time should not be after End Time", Toast.LENGTH_LONG).show()
            }
            startWeekendDate?.after(endWeekendDate) == true -> {
                valid = false
                Toast.makeText(context, "Weekend Start Time should not be after End Time", Toast.LENGTH_LONG).show()
            }
        }

        if(valid && startDate != null && endDate != null) {
            startWeekdayTime = serverDateFormat.format(startDate)
            endWeekdayTime = serverDateFormat.format(endDate)
        }
        else {
            startWeekdayTime = ""
            endWeekdayTime = ""
        }

        if(valid && startWeekendDate != null && endWeekendDate != null){
            startWeekendTime = serverDateFormat.format(startWeekendDate)
            endWeekendTime = serverDateFormat.format(endWeekendDate)
        }
        else {
            startWeekendTime = ""
            endWeekendTime = ""
        }

        return valid
    }

    private fun saveChanges() {

    }
}
