package ke.co.kazi.ui.v2.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.util.BASE_URL
import kotlinx.android.synthetic.main.layout_category.view.*

class CategoriesAdapter(private val categories: List<ServiceCategory>): RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_category, parent, false))

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categories[position])
    }


    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        fun bind(category: ServiceCategory) {
            itemView.apply {
                titleTextView.text = category.categoryName.toLowerCase().capitalize()

                GlideApp.with(itemView)
                        .load(BASE_URL + category.imageUrl)
                        .into(imageView)
            }
        }
    }
}