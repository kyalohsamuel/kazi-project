package ke.co.kazi.ui.v1.adapter


import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ke.co.kazi.GlideApp
import ke.co.kazi.databinding.FragmentServiceSubCategoryBinding
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.adapter.callback.ServiceSubCategoriesDiffCallback

/**
 * [RecyclerView.Adapter] that can display a [ServiceCategory]
 */
class ServiceSubCategoriesRecyclerViewAdapter(val context: Context, val onItemSelectedListener: OnItemSelectedListener) : ListAdapter<ServiceSubCategory, ServiceSubCategoriesRecyclerViewAdapter.ViewHolder>(ServiceSubCategoriesDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = FragmentServiceSubCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(val binding: FragmentServiceSubCategoryBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ServiceSubCategory) {
            binding.root.setOnClickListener { onItemSelectedListener.onItemSelected(item) }
            binding.category = item

            GlideApp.with(context)
                    .load("http://45.79.18.37:8080"+item.imageUrl)
                    .into(binding.imageView)

            binding.executePendingBindings()
        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(item: ServiceSubCategory)
    }
}
