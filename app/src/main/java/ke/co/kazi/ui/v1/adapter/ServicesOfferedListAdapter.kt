package ke.co.kazi.ui.v1.adapter

import androidx.recyclerview.widget.ListAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import ke.co.kazi.databinding.ServicesOfferedItemBinding
import ke.co.kazi.model.ServiceRequest
import ke.co.kazi.ui.v1.adapter.callback.ServiceRequestDiffCallback

class ServicesOfferedListAdapter(val onItemSelectedListener: OnItemSelectedListener) : ListAdapter<ServiceRequest, ServicesOfferedListAdapter.ViewHolder>(ServiceRequestDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ServicesOfferedItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(val binding: ServicesOfferedItemBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ServiceRequest) {
            binding.root.setOnClickListener { onItemSelectedListener.onItemSelected(item) }
            binding.service = item
            binding.executePendingBindings()
        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(item: ServiceRequest)
    }
}