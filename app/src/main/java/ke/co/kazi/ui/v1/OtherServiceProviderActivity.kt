package ke.co.kazi.ui.v1

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import ke.co.kazi.R
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import ke.co.kazi.GlideApp
import kotlinx.android.synthetic.main.activity_other_service_provider.*
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.longToast
import org.jetbrains.anko.newTask
import timber.log.Timber

class OtherServiceProviderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_other_service_provider)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        backButton.setOnClickListener {
            onBackPressed()
        }

        doneButton.setOnClickListener {
            longToast("Added as Service Provider!")
            startActivity(intentFor<MainDrawerActivity>().newTask().clearTop())
        }

        listOf(circleImageView, uploadProfileButton).forEach {
            it.setOnClickListener {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setCropShape(CropImageView.CropShape.OVAL)
                        .setAspectRatio(1,1)
                        .setFixAspectRatio(true)
                        .start(this)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)

                when (resultCode) {
                    Activity.RESULT_OK -> {
                        GlideApp.with(this)
                                .load(result.uri)
                                .fitCenter()
                                .into(circleImageView)
                    }
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE ->
                        Timber.e(result.error, result.error.message)
                }
            }
        }
    }
}
