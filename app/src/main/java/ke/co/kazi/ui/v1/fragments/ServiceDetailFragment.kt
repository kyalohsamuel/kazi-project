package ke.co.kazi.ui.v1.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.navigation.Navigation
import dagger.android.support.AndroidSupportInjection

import ke.co.kazi.R
import ke.co.kazi.api.models.data.ServiceIdModel
import ke.co.kazi.databinding.FragmentServiceDetailBinding
import ke.co.kazi.model.Service
import ke.co.kazi.util.SERVICE_EXTRA
import ke.co.kazi.util.setGridViewHeightBasedOnChildren
import ke.co.kazi.viewmodel.ServiceViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_service_detail.*
import timber.log.Timber
import javax.inject.Inject

class ServiceDetailFragment : androidx.fragment.app.Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentServiceDetailBinding
    private lateinit var viewModel: ServiceViewModel

    private val carouselAdapter by lazy {
        CarouselAdapter(childFragmentManager)
    }

    private var service: Service? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            service = it.getSerializable(SERVICE_EXTRA) as? Service
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentServiceDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]

        binding.service = service

        btnEngage.setOnClickListener {
            viewModel.requestService(ServiceIdModel(service?.serverId
                    ?: -1, arrayOf(0.0, 0.0))).observe(this, Observer {
                it?.let { resource ->
                    binding.resource = resource
                    Timber.e("${resource.data}")
                    when (resource.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            Navigation.findNavController(view).navigateUp()
                        }
                        Status.FAILED -> {
                        }
                    }
                }
            })
        }

        btnReport.setOnClickListener {
            //Todo implement this
            Navigation.findNavController(view).navigateUp()
        }

        carousel.adapter = carouselAdapter
        carousel.pageMargin = 20
        carousel.clipToPadding = false
        carousel.offscreenPageLimit = 2
        carousel.setPadding(20,0,20,0)

        //Grid View
        val proficiencies = arrayOf("JAVA", "NodeJs", "Python", "React JS", "Kotlin", "HTML")
        proficiencyGridView.adapter = ArrayAdapter<String>(context, R.layout.list_item_proficiency, proficiencies)
        setGridViewHeightBasedOnChildren(proficiencyGridView, 3)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    inner class CarouselAdapter(supportFragmentManager: androidx.fragment.app.FragmentManager) : androidx.fragment.app.FragmentPagerAdapter(supportFragmentManager) {
        override fun getItem(position: Int): androidx.fragment.app.Fragment {
            return CarouselImageFragment.newInstance()
        }

        override fun getCount(): Int {
            return 3
        }

        override fun getPageWidth(position: Int): Float {
            return 0.95f
        }
    }

//    inner class ProficiencyAdapter:
}
