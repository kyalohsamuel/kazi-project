package ke.co.kazi.ui.v1.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import ke.co.kazi.R
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.fieldError
import ke.co.kazi.helper.requiredField
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.GET_SUB_CATEGORY
import ke.co.kazi.ui.v1.ServiceCategoryActivity
import ke.co.kazi.ui.v2.merchant.fragment.ServiceViewFragment
import ke.co.kazi.util.toEditable
import kotlinx.android.synthetic.main.fragment_service_information.*
import org.jetbrains.anko.bundleOf
import org.jetbrains.anko.longToast

class ServiceInformationFragment : ServiceViewFragment() {
    companion object {
        fun newInstance(serviceID: Int): ServiceInformationFragment {
            return ServiceInformationFragment().apply {
                arguments = bundleOf(ARG_SERVICE_ID to serviceID)
            }
        }
    }

    private var selectedSubCategoryCode: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_service_information, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nameEditText.addTextChangedListener(ClearErrorTextWatcher(nameInputLayout))
        descEditText.addTextChangedListener(ClearErrorTextWatcher(descInputLayout))
        categoryEditText.addTextChangedListener(ClearErrorTextWatcher(categoryInputLayout))

        categoryEditText.setOnClickListener {
            val intent = Intent(context, ServiceCategoryActivity::class.java)
            intent.putExtra("FORWARD_FLOW", false)
            startActivityForResult(intent, GET_SUB_CATEGORY)
        }

        nextButton.setOnClickListener { next() }
        saveChangesButton.setOnClickListener { saveChanges() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            GET_SUB_CATEGORY -> when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.apply {
                        val serviceCategory = this.getSerializableExtra(SUB_CATEGORY_CODE) as ServiceSubCategory
                        selectedSubCategoryCode = serviceCategory.subCategoryCode
                        categoryEditText.text = serviceCategory.subCategoryName.toEditable()
                    }
                }
            }
        }
    }

    override fun isLoading(loading: Boolean) {
        super.isLoading(loading)
        when(loading){
            true -> {
                saveChangesButton.text = null
                progressBar.visibility = View.VISIBLE
            }
            false -> {
                saveChangesButton.text = getText(R.string.action_save_changes)
                progressBar.visibility = View.GONE
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when(service == null){
            true -> {
                nextButton.visibility = View.VISIBLE
                saveChangesButton.visibility = View.GONE
            }
            false -> {
                nextButton.visibility = View.GONE
                saveChangesButton.visibility = View.VISIBLE
            }
        }
    }

    override fun setupViews() {
        super.setupViews()
        nameEditText.text = service?.name?.toEditable()
        descEditText.text = service?.description?.toEditable()

        selectedSubCategoryCode = service?.subCategoryCode
        categoryEditText.text = selectedSubCategoryCode?.toEditable()

        nextButton.visibility = View.GONE
        saveChangesButton.visibility = View.VISIBLE
    }

    private fun next() {
        val name = nameEditText.text.toString().trim()
        val desc = descEditText.text.toString().trim()

        when{
            name.isBlank() ->
                nameInputLayout.fieldError("Name".requiredField())
            desc.isBlank() ->
                descInputLayout.fieldError("Description".requiredField())
            selectedSubCategoryCode == null ->
                categoryInputLayout.fieldError("Choose a Service Category to continue")
            else -> {
                val action = ServiceInformationFragmentDirections
                        .actionServiceInformationFragmentToServiceContactFragment(name, desc,
                                selectedSubCategoryCode!!)

                view?.findNavController()?.navigate(action)
            }
        }
    }

    private fun saveChanges() {
        val name = nameEditText.text.toString().trim()
        val desc = descEditText.text.toString().trim()

        when {
            name == service?.name && desc == service?.description && selectedSubCategoryCode == service?.subCategoryCode
                -> context?.longToast("No Changes Detected")
            name.isBlank() ->
                nameInputLayout.fieldError("Name".requiredField())
            desc.isBlank() ->
                descInputLayout.fieldError("Description".requiredField())
            selectedSubCategoryCode == null ->
                categoryInputLayout.fieldError("Choose a Service Category to continue")
            else -> {
                service?.name = name
                service?.description = desc
                service?.subCategoryCode = selectedSubCategoryCode!!

                updateService()
            }
        }
    }

}
