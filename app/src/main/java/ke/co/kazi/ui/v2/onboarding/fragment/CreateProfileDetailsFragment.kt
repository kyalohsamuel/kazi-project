package ke.co.kazi.ui.v2.onboarding.fragment


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.api.models.data.OtherData
import ke.co.kazi.api.models.data.UpdateServiceProvider
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.fieldError
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.ui.v2.merchant.MerchantActivity
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_create_profile_details.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.newTask
import timber.log.Timber

class CreateProfileDetailsFragment : CreateServiceProviderFragment() {
    private var imageUri: Uri? = null
        set(value) {
            setCircleImageView(value)
            field = value
        }

    override fun isLoading(loading: Boolean) {
        doneButton.text = when(loading){ true -> null false -> getString(R.string.action_done) }
        progressBar.visibility = when(loading){ true -> View.VISIBLE false -> View.GONE }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                when (resultCode) {
                    Activity.RESULT_OK -> imageUri = result.uri
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> Timber.e(result.error)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_profile_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        circleImageView.setOnClickListener {
            CropImage.activity()
                    .setAspectRatio(1,1)
                    .setCropShape(CropImageView.CropShape.OVAL)
                    .start(context!!, this) }

        descEditText.addTextChangedListener(ClearErrorTextWatcher(descInputLayout))

        doneButton.setOnClickListener { updateDetails() }
    }

    private fun setCircleImageView(uri: Uri?) {
        GlideApp.with(context!!)
                .load(uri)
                .error(R.drawable.img_profile_placeholder_100dp)
                .into(circleImageView)
    }


    private fun updateDetails() {
        val description = descEditText.text.toString()

        when{
            imageUri == null -> Toast.makeText(context, getString(R.string.error_pick_service_provider_image), Toast.LENGTH_LONG).show()
            description.count() <= 5 -> descInputLayout.fieldError(getString(R.string.error_desc_short))
            description.count() > 200 -> descInputLayout.fieldError(getString(R.string.error_desc_long))
            else -> {
                updateDetails(imageUri!!, description)
            }
        }
    }

    private fun updateDetails(imageUri: Uri, description: String) {
        val updateProvider = UpdateServiceProvider("DESCRIPTION", OtherData(description))
        viewModel.updateServiceProvider(updateProvider).observe(this, Observer { resource ->
            loading = false
            when(resource.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> {
                    Toast.makeText(context, "Saved Service Provider Successfully", Toast.LENGTH_LONG).show()
                    startActivity(Intent(context, MerchantActivity::class.java).clearTask().clearTop().newTask())
                }
                Status.FAILED -> handleNetworkError(resource)
            }
        })
    }
}
