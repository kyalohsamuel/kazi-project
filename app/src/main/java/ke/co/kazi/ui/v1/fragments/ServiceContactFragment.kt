package ke.co.kazi.ui.v1.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlacePicker
import ke.co.kazi.R
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.fieldError
import ke.co.kazi.helper.requiredField
import ke.co.kazi.ui.v2.merchant.fragment.ServiceViewFragment
import ke.co.kazi.util.toEditable
import kotlinx.android.synthetic.main.fragment_service_contact.*
import org.jetbrains.anko.bundleOf


class ServiceContactFragment : ServiceViewFragment() {
    companion object {
        const val PLACE_PICKER_REQUEST = 100

        fun newInstance(serviceID: Int): ServiceContactFragment {
            return ServiceContactFragment().apply {
                arguments = bundleOf(ARG_SERVICE_ID to serviceID)
            }
        }
    }

    private var place: Place? = null

    private lateinit var serviceName: String
    private lateinit var serviceDesc: String
    private lateinit var subCategory: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.apply {
            if(!containsKey("serviceName")) return

            serviceName = getString("serviceName")!!
            serviceDesc = getString("serviceDesc")!!
            subCategory = getString("categoryCode")!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(ke.co.kazi.R.layout.fragment_service_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mobileNoEditText.addTextChangedListener(ClearErrorTextWatcher(mobileNoInputLayout))
        addressEditText.addTextChangedListener(ClearErrorTextWatcher(addressInputLayout))
        addressEditText.setOnClickListener {
            val builder = PlacePicker.IntentBuilder()
            startActivityForResult(builder.build(activity), PLACE_PICKER_REQUEST)
        }

        nextButton.setOnClickListener { next() }
        saveChangesButton.setOnClickListener { saveChanges() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            PLACE_PICKER_REQUEST -> {
                when(resultCode){
                    Activity.RESULT_OK -> {
                        place = PlacePicker.getPlace(context, data)
                        addressEditText.setText(place?.name)
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        when(service == null){
            true -> {
                nextButton.visibility = View.VISIBLE
                saveChangesButton.visibility = View.GONE
            }
            false -> {
                nextButton.visibility = View.GONE
                saveChangesButton.visibility = View.VISIBLE
            }
        }
    }

    override fun isLoading(loading: Boolean) {
        super.isLoading(loading)
        when(loading){
            true -> {
                saveChangesButton.text = null
                progressBar.visibility = View.VISIBLE
            }
            false -> {
                saveChangesButton.text = getText(R.string.action_save_changes)
                progressBar.visibility = View.GONE
            }
        }
    }

    override fun setupViews() {
        super.setupViews()
        addressEditText.text = service?.address?.toEditable()
        mobileNoEditText.text = service?.contact?.toEditable()
        facebookEditText.text = service?.social?.facebook?.toEditable()
        twitterEditText.text = service?.social?.twitter?.toEditable()
        webEditText.text = service?.social?.website?.toEditable()

        nextButton.visibility = View.GONE
        saveChangesButton.visibility = View.VISIBLE
    }

    private fun next() {
        val mobileNo = mobileNoEditText.text.toString()
        val address = addressEditText.text.toString()
        val lat = place?.latLng?.latitude.toString()
        val lng = place?.latLng?.longitude.toString()
        val facebook = facebookEditText.text.toString()
        val twitter = twitterEditText.text.toString()
        val website =  webEditText.text.toString()

        when {
            mobileNo.isBlank() ->
                mobileNoInputLayout.fieldError("Mobile Number".requiredField())
            address.isBlank() ->
                addressInputLayout.fieldError("Physical Address".requiredField())
            else -> {
                val action = ServiceContactFragmentDirections.actionServiceContactFragmentToOtherInformationServiceFragment(
                        serviceName, serviceDesc,subCategory, mobileNo,
                        address, lat, lng, facebook, twitter, website)
                view?.findNavController()?.navigate(action)
            }
        }

    }

    private fun saveChanges() {
        val mobileNo = mobileNoEditText.text.toString()
        val address = addressEditText.text.toString()
        val lat = place?.latLng?.latitude.toString()
        val lng = place?.latLng?.longitude.toString()
        val facebook = facebookEditText.text.toString()
        val twitter = twitterEditText.text.toString()
        val website = webEditText.text.toString()

        when {
            mobileNo.isBlank() ->
                mobileNoInputLayout.fieldError("Mobile Number".requiredField())
            address.isBlank() ->
                addressInputLayout.fieldError("Physical Address".requiredField())
            else -> {
                service?.contact = mobileNo
                service?.address = address
                service?.lat = lat
                service?.lng = lng
                service?.social?.facebook = facebook
                service?.social?.twitter = twitter
                service?.social?.website = website

                updateService()
            }
        }
    }
}
