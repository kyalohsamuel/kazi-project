package ke.co.kazi.ui.v2.onboarding.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import ke.co.kazi.R
import ke.co.kazi.api.models.data.LoginData
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.ui.v2.dashboard.DashboardActivity
import ke.co.kazi.ui.v2.merchant.MerchantActivity
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.reObserve
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_verification_code.*
import org.jetbrains.anko.*
import javax.inject.Inject

class LoginFragment : OnBoardingFragment() {
    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences
    private val args: LoginFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater
                .inflate(R.layout.fragment_verification_code, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setupViews() {
        descTextView.text = getString(R.string.string_login)
        secondaryDescTextView.text = null
        secondaryActionTextView.text = getText(R.string.action_forgot_pin)

        verificationOtpView.setOtpCompletionListener { login(it) }

        loginObserver()
    }

    private fun login(pin: String) {
        viewModel.loginAction(LoginData(pin, args.phoneNumber))
    }

    private fun loginObserver() {
        viewModel.userDetails.reObserve(this, Observer { response ->
            progressBar.visibility = View.GONE
            when(response.status){
                Status.LOADING -> progressBar.visibility = View.VISIBLE
                Status.SUCCESS -> {
                    response.data?.data?.let { user ->
                        viewModel.saveUser(user, response.data?.token!!).observe(this, Observer { resource ->
                            progressBar.visibility = View.GONE
                            when(resource.status){
                                Status.LOADING -> progressBar.visibility = View.VISIBLE
                                Status.SUCCESS -> navigate(user.userType)
                                Status.FAILED -> handleNetworkError(resource)
                            }

                        })
                    }
                }
                Status.FAILED -> handleNetworkError(response)
            }
        })
    }

    private fun navigate(userType: String) {
        when(userType){
            "CLIENT" -> context?.intentFor<DashboardActivity>()?.clearTask()?.clearTop()?.newTask()
            "SP" -> context?.intentFor<MerchantActivity>()?.clearTask()?.clearTop()?.newTask()
            else -> {
                context?.longToast("Unknown User Type. Contact System Admin")
                null
            }
        }?.let{
            startActivity(it)
        }
    }
}
