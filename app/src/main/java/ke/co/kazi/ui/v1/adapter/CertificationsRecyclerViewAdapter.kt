package ke.co.kazi.ui.v1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.model.Certification
import kotlinx.android.synthetic.main.layout_certification_item.view.*

class CertificationsRecyclerViewAdapter(var certifications: ArrayList<Certification>):
        RecyclerView.Adapter<CertificationsRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.layout_certification_item, parent, false))
    }

    override fun getItemCount(): Int = certifications.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(certifications[position])
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(certification: Certification){
            itemView.apply {
                nameTextView.text = certification.name
                institutionTextView.text = certification.institution
                qualificationTextView.text = certification.qualification

                GlideApp.with(itemView)
                        .load(certification.imageUri)
                        .fitCenter()
                        .into(imageView)

                progressViews.visibility = when(certification.syncing){
                    true -> View.VISIBLE
                    false -> View.GONE
                }
            }
        }
    }
}