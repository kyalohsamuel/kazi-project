package ke.co.kazi.ui.v1.fragments

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import ke.co.kazi.R
import ke.co.kazi.model.Service
import ke.co.kazi.ui.v1.ServiceDetailsActivity
import ke.co.kazi.ui.v1.adapter.ServicesRecyclerViewAdapter
import ke.co.kazi.util.SERVICE_EXTRA
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.ServiceViewModel
import javax.inject.Inject


class ServiceFragment : androidx.fragment.app.Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ServiceViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_service_list, container, false)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]


        val servicesListAdapter = ServicesRecyclerViewAdapter(object : ServicesRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: Service) {
//                startActivity<ServiceDetailsActivity>(Pair(SERVICE_EXTRA, item))
            }

        })

        // Set the adapter
        if (view is androidx.recyclerview.widget.RecyclerView) {
            with(view) {
                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
                adapter = servicesListAdapter
            }
        }

        viewModel.loadServices().reObserve(this, Observer {
            it?.data.apply {
                servicesListAdapter.submitList(this)
            }
        })

        return view
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

}
