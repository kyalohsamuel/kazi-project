package ke.co.kazi.ui.v2.merchant

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.ViewModelFactory
import ke.co.kazi.model.Service
import ke.co.kazi.ui.v2.adapter.ServicePagerAdapter
import ke.co.kazi.util.BASE_URL
import ke.co.kazi.viewmodel.ServiceViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_service.*
import org.jetbrains.anko.intentFor
import javax.inject.Inject


class ServiceActivity : AppCompatActivity() {

    companion object {
        const val ARG_SERVICE_ID = "service_id"

        fun newInstance(context: Context, serviceID: Int){
            context.startActivity(context.intentFor<ServiceActivity>().putExtra(ARG_SERVICE_ID, serviceID))
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    lateinit var viewModel: ServiceViewModel

    private var serviceID: Int = 0
        set(value) {
            if(value != 0) loadService()
            field = value
        }

    private var service: Service? = null
        set(value){
            field = value
            if(value != null) setupViews()
        }

    private var loading: Boolean = false
        set(value){
            isLoading(value)
            field = value
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ServiceViewModel::class.java)

        serviceID = when(intent.hasExtra(ARG_SERVICE_ID)){
            true -> intent.getIntExtra(ARG_SERVICE_ID, 0)
            false -> throw IllegalStateException("Invalid Service ID")
        }

        setupViews()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> return super.onOptionsItemSelected(item)
        }
        return false
    }


    private fun isLoading(loading: Boolean) {
        progressView.visibility = when(loading){
            true -> View.VISIBLE
            false -> View.GONE
        }
    }

    private fun loadService() {
        viewModel.loadServices().observe(this, Observer { resource ->
            loading = false
            when(resource.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> service = resource.data?.first { it.serverId == serviceID }
                Status.FAILED -> {}
            }
        })
    }

    private fun setupViews() {
        supportActionBar?.title = service?.name

        service?.images?.getOrNull(0)?.let { image ->
            GlideApp.with(this)
                    .load(BASE_URL + image.path)
                    .centerCrop()
                    .into(backgroundImageView)
        }

        nameTextView.text = service?.name
        descTextView.text = service?.categoryCode?.toLowerCase()?.capitalize()

        val adapter = ServicePagerAdapter(resources, supportFragmentManager, serviceID)
        viewPager.adapter = adapter

        tabLayout.setupWithViewPager(viewPager)
    }

}
