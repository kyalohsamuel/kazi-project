package ke.co.kazi.ui.v1

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import android.view.View
import android.widget.Toast
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.models.data.ForgotPasswordData
import ke.co.kazi.databinding.ActivityForgotPasswordBinding
import ke.co.kazi.util.toEditable
import ke.co.kazi.viewmodel.LoginViewModel
import ke.co.kazi.vo.Status

import org.jetbrains.anko.intentFor
import org.jetbrains.anko.longToast
import timber.log.Timber
import javax.inject.Inject

class ForgotPasswordActivity : AppCompatActivity() {

    private lateinit var viewModel: LoginViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityForgotPasswordBinding

    private val data = ForgotPasswordData()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        data.mobileNumber = intent?.getStringExtra(INTENT_USER_PHONE_NUMBER) ?: ""
    }

    fun onResetPassword(view: View){
        if (validate()) {
            resetPassword()
        } else {
            binding.editText4.text = "".toEditable()
            binding.confirmPin.text = "".toEditable()
        }
    }

    private fun validate(): Boolean {
        return when {
            data.password.isEmpty() -> {
                longToast("Enter password")
                false
            }
            binding.confirmPin.text.toString().isEmpty() -> {
                longToast("Confirm Password.")
                false
            }
            binding.confirmPin.text.toString() != data.password -> {
                longToast("The passwords do not match.")
                false
            }
            else -> true
        }
    }

    private fun resetPassword() {
        viewModel.resetPassword(data).observe(this, Observer { resource ->
            binding.resource = resource
            Timber.e("Reset Password ${resource?.data}")
            when (resource?.status) {
                Status.SUCCESS -> {
                    startActivity(intentFor<CheckMobileActivity>())
                    finish()
                }
                Status.FAILED -> Toast.makeText(this, "Failed to reset password. Please try again!", Toast.LENGTH_LONG).show()
                Status.LOADING -> {
                }
            }
        })
    }

}
