package ke.co.kazi.ui.v1

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.databinding.ActivityServicesListBinding
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.adapter.ServicesRecyclerViewAdapter
import ke.co.kazi.util.SERVICE_EXTRA
import ke.co.kazi.viewmodel.ServiceViewModel
import kotlinx.android.synthetic.main.activity_services_list.*
import org.jetbrains.anko.startActivity
import javax.inject.Inject

class ServicesListActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: ActivityServicesListBinding
    lateinit var viewModel: ServiceViewModel

    private var serviceSubCategory: ServiceSubCategory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_services_list)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]

        intent?.let {
            serviceSubCategory = it.getSerializableExtra(SUB_CATEGORY_CODE) as ServiceSubCategory
        }

        val servicesRecyclerViewAdapter = ServicesRecyclerViewAdapter(object : ServicesRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: Service) {
                startActivity<ServiceDetailsActivity>(Pair(SERVICE_EXTRA, item))
            }

        })

        with(list) {
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = mLayoutManager
            adapter = servicesRecyclerViewAdapter
        }

        serviceSubCategory?.let { serviceSubCategory ->
            viewModel.loadServicesBySubCategory(serviceSubCategory.categoryCode).observe(this, Observer { resource ->
                resource?.data?.let {
                    servicesRecyclerViewAdapter.submitList(it)
                }
            })
        }

    }

}
