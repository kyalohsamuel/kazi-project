package ke.co.kazi.ui.v1.adapter

import android.content.Context
import androidx.recyclerview.widget.ListAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ke.co.kazi.R
import ke.co.kazi.model.Message
import ke.co.kazi.model.MessageType
import ke.co.kazi.ui.v1.adapter.callback.MessageDiffCallback
import ke.co.kazi.util.DateUtils
import kotlinx.android.synthetic.main.list_item_my_message.view.*
import kotlinx.android.synthetic.main.list_item_other_message.view.*

private const val VIEW_TYPE_MY_MESSAGE = 1
private const val VIEW_TYPE_OTHER_MESSAGE = 2

class MessageAdapter(val userId: String, val myMessageType: MessageType, var context: Context): ListAdapter<Message, MessageViewHolder>(MessageDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        return if(viewType == VIEW_TYPE_MY_MESSAGE) {
            MyMessageViewHolder(
                    LayoutInflater.from(context).inflate(R.layout.list_item_my_message, parent, false)
            )
        } else {
            OtherMessageViewHolder(
                    LayoutInflater.from(context).inflate(R.layout.list_item_other_message, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val message = getItem(position)

        holder.bind(message)
    }

    override fun getItemViewType(position: Int): Int {
        val message = getItem(position)

        return if(myMessageType == message.type && userId == message.from) {
            VIEW_TYPE_MY_MESSAGE
        }
        else {
            VIEW_TYPE_OTHER_MESSAGE
        }
    }

    inner class MyMessageViewHolder (view: View) : MessageViewHolder(view) {
        private var messageText: TextView = view.txtMyMessage
        private var timeText: TextView = view.txtMyMessageTime

        override fun bind(message: Message) {
            messageText.text = message.message
            timeText.text = DateUtils.fromMillisToTimeString(message.time.toLong())
        }
    }

    inner class OtherMessageViewHolder (view: View) : MessageViewHolder(view) {
        private var messageText: TextView = view.txtOtherMessage
        private var userText: TextView = view.txtOtherUser
        private var timeText: TextView = view.txtOtherMessageTime

        override fun bind(message: Message) {
            messageText.text = message.message
            userText.text = if(userId == message.from) {message.from} else {message.to}
            timeText.text = DateUtils.fromMillisToTimeString(message.time.toLong())
        }
    }
}

open class MessageViewHolder (view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
    open fun bind(message:Message) {}
}