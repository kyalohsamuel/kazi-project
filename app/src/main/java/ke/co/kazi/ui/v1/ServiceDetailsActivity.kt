package ke.co.kazi.ui.v1

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.models.data.ServiceIdModel
import ke.co.kazi.binding.BindingConverter
import ke.co.kazi.databinding.ActivityServiceDetailsBinding
import ke.co.kazi.model.Service
import ke.co.kazi.util.SERVICE_EXTRA
import ke.co.kazi.util.SERVICE_LOCATION
import ke.co.kazi.viewmodel.ServiceViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_service_details.*
import timber.log.Timber
import javax.inject.Inject

class ServiceDetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ServiceViewModel

    private lateinit var binding: ActivityServiceDetailsBinding

    private var service: Service? = null
    private var location: DoubleArray? = doubleArrayOf(0.0, 0.0)

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_service_details)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]

        supportActionBar?.let {
            it.title = ""
        }

        intent?.let {
            service = it.getSerializableExtra(SERVICE_EXTRA) as Service
            location = it.getDoubleArrayExtra(SERVICE_LOCATION)
        }

        binding.converter = BindingConverter()
        binding.service = service


        btnRequest.setOnClickListener { _ ->
            viewModel.requestService(ServiceIdModel(service?.serverId
                    ?: -1, location?.toTypedArray() ?: arrayOf(0.0, 0.0))).observe(this, Observer {
                it?.let { resource ->
                    binding.resource = resource
                    Timber.e("${resource.data}")
                    when (resource.status) {
                        Status.LOADING -> {
                        }
                        Status.SUCCESS -> {
                            finish()
                        }
                        Status.FAILED -> {
                        }
                    }
                }
            })
        }

    }

}
