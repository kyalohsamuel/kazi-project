package ke.co.kazi.ui.v1.fragments

import android.Manifest
import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import ke.co.kazi.R
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.FileChooser
import ke.co.kazi.util.RC_CAMERA_AND_STORAGE
import kotlinx.android.synthetic.main.fragment_file_picker.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber
import javax.inject.Inject

const val ARG_VIEW_KEY = "view_key"

class FilePickerFragment : BottomSheetDialogFragment() {

    @Inject
    lateinit var preferences: CustomSharedPreferences

    private val fileChooser by lazy { FileChooser(context as Activity) }

    var viewKey: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            viewKey = it.getString(ARG_VIEW_KEY, null)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_file_picker, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        handleActions()
    }

    @AfterPermissionGranted(RC_CAMERA_AND_STORAGE)
    private fun handleActions() {
        val perms = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (EasyPermissions.hasPermissions(context!!, *perms)) {
            btnTakePhoto.setOnClickListener { _ ->
                viewKey?.let {
                    preferences.filePicked = it
                }
                fileChooser.capturePicture()
            }

            btnUploadFile.setOnClickListener { _ ->
                viewKey?.let {
                    preferences.filePicked = it
                }
                fileChooser.openFile()
            }

            btnUploadPhoto.setOnClickListener { _ ->
                viewKey?.let { it ->
                    preferences.filePicked = it
                }
                fileChooser.openPhoto()
            }
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.camera_and_location_rationale),
                    RC_CAMERA_AND_STORAGE, *perms)
        }
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    companion object {

        fun newInstance(viewKey: String?): FilePickerFragment =
                FilePickerFragment().apply {
                    arguments = Bundle().apply {
                        Timber.e("View key: $viewKey")
                        putString(ARG_VIEW_KEY, viewKey)
                    }
                }

        fun newInstance(): FilePickerFragment = FilePickerFragment()
    }
}
