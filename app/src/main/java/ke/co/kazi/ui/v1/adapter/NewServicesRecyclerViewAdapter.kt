package ke.co.kazi.ui.v1.adapter


import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ke.co.kazi.binding.BindingConverter
import ke.co.kazi.databinding.ListItemMapServiceBinding
import ke.co.kazi.databinding.ListItemServiceBinding
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.ui.v1.adapter.callback.ServiceDiffCallback

/**
 * [RecyclerView.Adapter] that can display a [ServiceCategory]
 */

class NewServicesRecyclerViewAdapter(val viewType: Int, val onItemSelectedListener: OnItemSelectedListener) : ListAdapter<Service, NewServicesRecyclerViewAdapter.BaseViewHolder<Service>>(ServiceDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Service> {
        return when(viewType){
            0 -> ViewHolder(ListItemServiceBinding.inflate(LayoutInflater.from(parent.context), parent, false))
            else -> MapViewHolder(ListItemMapServiceBinding.inflate(LayoutInflater.from(parent.context), parent, false))
        }

    }

    override fun onBindViewHolder(holder: BaseViewHolder<Service>, position: Int) = holder.bind(getItem(position))

    override fun getItemViewType(position: Int): Int {
        return viewType
    }

    inner class ViewHolder(val binding: ListItemServiceBinding) : BaseViewHolder<Service>(binding) {
        override fun bind(item: Service) {
            binding.root.setOnClickListener { onItemSelectedListener.onItemSelected(item) }
            binding.service = item
            binding.executePendingBindings()
        }
    }

    inner class MapViewHolder(val binding: ListItemMapServiceBinding): BaseViewHolder<Service>(binding){
        override fun bind(item: Service) {
            binding.root.setOnClickListener { onItemSelectedListener.onItemSelected(item) }
            binding.service = item
            binding.converter = BindingConverter()
            binding.executePendingBindings()
        }

    }

    interface OnItemSelectedListener {
        fun onItemSelected(item: Service)
    }

    abstract inner class BaseViewHolder<in T>(binding : ViewDataBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        abstract fun bind(item: T)
    }
}
