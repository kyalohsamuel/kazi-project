package ke.co.kazi.ui.v1

import android.app.Activity
import androidx.lifecycle.ViewModelProvider
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.graphics.Typeface
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.text.style.StyleSpan
import com.google.android.gms.location.places.AutocompletePredictionBufferResponse
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.tasks.Task
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.databinding.ActivityPlacesSearchBinding
import ke.co.kazi.ui.v1.adapter.PlacesListAdapter
import ke.co.kazi.vo.Place
import kotlinx.android.synthetic.main.activity_places_search.*
import timber.log.Timber
import javax.inject.Inject

private val STYLE_BOLD = StyleSpan(Typeface.BOLD)


class PlacesSearchActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityPlacesSearchBinding

    private lateinit var placesAdapter: PlacesListAdapter

    private lateinit var mGeoDataClient: GeoDataClient

    private lateinit var mBounds: LatLngBounds

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_places_search)

        mGeoDataClient = Places.getGeoDataClient(this)

        placesAdapter = PlacesListAdapter(object : OnPlaceSelectedListener {
            override fun onPlaceSelected(place: Place) {
                val intent = Intent()
                intent.putExtra(SEARCH_PLACE_RESULT, place)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        })

        imageBtnClosing.setOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        with(recyclerPlaces) {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@PlacesSearchActivity)
            adapter = placesAdapter
        }

        mBounds = LatLngBounds.builder().include(LatLng(1.2921, 36.8219)).build()

        textPlaceSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                getPlacesPredictions(s.toString())
            }
        })
    }

    fun getPlacesPredictions(constraint: String) {
        val results: Task<AutocompletePredictionBufferResponse> = mGeoDataClient.getAutocompletePredictions(constraint, mBounds, null)

        results.addOnCompleteListener(this) { task ->
            when {
                task.isSuccessful -> {
                    Timber.e("places count: ${task.result?.count}")
                    task.result?.forEach { t -> Timber.e(t.getFullText(null).toString()) }

                    placesAdapter.submitList(task.result?.map { Place.toPlace(it) })
                }
                else -> {
                    Timber.e("Error getting predictions")
                }
            }
        }
    }

    interface OnPlaceSelectedListener {
        fun onPlaceSelected(place: Place)
    }
}
