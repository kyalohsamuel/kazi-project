package ke.co.kazi.ui.v1.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import ke.co.kazi.R

/**
 * A simple [Fragment] subclass.
 * Use the [CarouselImageFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class CarouselImageFragment : androidx.fragment.app.Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_carousel_image, container, false)
    }


    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                CarouselImageFragment().apply {
                    arguments = Bundle().apply {

                    }
                }

        fun newInstance() = CarouselImageFragment()
    }
}
