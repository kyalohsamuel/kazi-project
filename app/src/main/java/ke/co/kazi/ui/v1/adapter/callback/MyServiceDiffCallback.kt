package ke.co.kazi.ui.v1.adapter.callback

import androidx.recyclerview.widget.DiffUtil
import ke.co.kazi.model.ServiceCategory

class MyServiceDiffCallback : DiffUtil.ItemCallback<ServiceCategory>() {
    override fun areItemsTheSame(oldItem: ServiceCategory, newItem: ServiceCategory): Boolean {
        return oldItem?.serviceId == newItem?.serviceId
    }

    override fun areContentsTheSame(oldItem: ServiceCategory, newItem: ServiceCategory): Boolean {
        return oldItem == newItem
    }
}