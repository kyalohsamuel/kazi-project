package ke.co.kazi.ui.v1

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.models.data.ServiceRequestIdModel
import ke.co.kazi.databinding.ActivityConfirmServiceRequestBinding
import ke.co.kazi.model.MessageType
import ke.co.kazi.model.ServiceRequest
import ke.co.kazi.ui.v1.fragments.ARG_SERVICE_FRAG_TYPE
import ke.co.kazi.ui.v1.fragments.ServiceFragmentType
import ke.co.kazi.util.MESSAGE_TYPE
import ke.co.kazi.util.SERVICE_REQUEST_EXTRA
import ke.co.kazi.viewmodel.ServiceViewModel
import kotlinx.android.synthetic.main.activity_confirm_service_request.*
import org.jetbrains.anko.startActivity
import timber.log.Timber
import javax.inject.Inject

class ConfirmServiceRequestActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ServiceViewModel
    private lateinit var binding: ActivityConfirmServiceRequestBinding

    private var request: ServiceRequest? = null
    private var serviceFragmentType: ServiceFragmentType? = null
    private var messageType: MessageType =MessageType.CLIENT

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_service_request)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]

        serviceFragmentType = intent?.getSerializableExtra(ARG_SERVICE_FRAG_TYPE) as ServiceFragmentType
        request = intent?.getSerializableExtra(SERVICE_REQUEST_EXTRA) as ServiceRequest
        binding.request = request
        binding.type = serviceFragmentType

        Timber.e("Request $request")

        messageType = if (serviceFragmentType == ServiceFragmentType.PROVIDER_REQUEST){
            MessageType.SP
        } else{
            MessageType.CLIENT
        }


//        setupConfirmed(request?.status == "CONFIRMED")
    }

    fun confirmRequest(view: View) {
        viewModel.confirmService(ServiceRequestIdModel(request?.requestId
                ?: -1)).observe(this@ConfirmServiceRequestActivity, Observer {
            Timber.e("Resource: ${it?.data}")
            binding.resource = it
            finish()
        })
    }

    fun rejectRequest(view: View) {
        finish()
    }

    fun onChat(view: View) {
        startActivity<ChatActivity>(Pair(SERVICE_REQUEST_EXTRA, request), Pair(MESSAGE_TYPE, messageType))
    }

    private fun setupConfirmed(isConfirmed: Boolean) {
        Timber.e("Is Confirmed $isConfirmed")

        if (isConfirmed) {
            button.visibility = View.GONE
            button2.visibility = View.GONE
            chatButton.visibility = View.VISIBLE
        } else {
            button.visibility = View.VISIBLE
            button2.visibility = View.VISIBLE
            chatButton.visibility = View.GONE
        }
    }

}
