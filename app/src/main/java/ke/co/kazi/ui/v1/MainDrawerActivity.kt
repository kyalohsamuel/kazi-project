package ke.co.kazi.ui.v1

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.ResultReceiver
import android.telephony.TelephonyManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.PlaceDetectionClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.navigation.NavigationView
import dagger.android.AndroidInjection
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.api.models.data.ServiceRequestData
import ke.co.kazi.databinding.ActivityMainDrawerBinding
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.model.User
import ke.co.kazi.ui.v1.adapter.ServicesRecyclerViewAdapter
import ke.co.kazi.ui.v1.fragments.ServiceCategoryFragment
import ke.co.kazi.ui.v1.services.Constants
import ke.co.kazi.ui.v1.services.FetchAddressIntentService
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.SERVICE_EXTRA
import ke.co.kazi.util.SERVICE_LOCATION
import ke.co.kazi.viewmodel.MainViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_main_drawer.*
import kotlinx.android.synthetic.main.app_bar_main_drawer.*
import kotlinx.android.synthetic.main.main_view_bottom_sheet.*
import kotlinx.android.synthetic.main.nav_header_main_drawer.view.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import timber.log.Timber
import javax.inject.Inject

const val SEARCH_PLACE = 100
const val SEARCH_PLACE_RESULT = "place_search"

// Keys for storing activity state.
private const val KEY_CAMERA_POSITION = "camera_position"
private const val KEY_LOCATION = "location"

private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
private const val READ_PHONE_PERM = 9089
private const val DEFAULT_ZOOM = 15F

class MainDrawerActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainDrawerBinding

    private val navController by lazy {
        Navigation.findNavController(this, R.id.fragment)
    }

    private lateinit var mMap: GoogleMap
    private lateinit var mGeoDataClient: GeoDataClient
    private lateinit var mPlaceDetectionClient: PlaceDetectionClient
    private lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var mCameraPosition: CameraPosition
    private lateinit var resultReceiver: ResultReceiver

    private val homeFragment by lazy { ServiceCategoryFragment() }

    private var mLastKnownLocation: Location? = null
    private var mLocationPermissionGranted: Boolean = false
    private val mDefaultLocation = LatLng(1.2921, 36.8219)
    private var mServiceLocation: Location? = null

    private var user: User? = null
    private var serviceSubCategory = ServiceSubCategory()

    private var selectedPlaceMarker: Marker? = null

    private val serviceRequest = ServiceRequestData()

    private val telephonyManager by lazy {
        getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    }

    override fun onMapReady(googleMap: GoogleMap) {
        Timber.e("Map ready")
        mMap = googleMap

        updateLocationUI()
        getDeviceLocation()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main_drawer)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION)
//            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION)
        }

        // Status bar :: Transparent
        val window = this.window

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = Color.TRANSPARENT
        }

        mGeoDataClient = Places.getGeoDataClient(this)
        mPlaceDetectionClient = Places.getPlaceDetectionClient(this)
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        resultReceiver = AddressResultReceiver(Handler())

        supportActionBar?.let {
            title = ""
        }

        sendFcmTokenToServer()

        setUpViews()
        observeData()

        nav_view.setNavigationItemSelectedListener(this)

//        supportFragmentManager.beginTransaction().replace(R.id.root, homeFragment).commit()
    }

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main_drawer, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_view_profile -> {
                navController.navigate(R.id.action_main_to_user_profile)
                true
            }
            R.id.action_change_password -> {
                true
            }
            R.id.action_logout -> {
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navController view item clicks here.
        when (item.itemId) {
            R.id.nav_your_services -> startActivity<ProviderServicesActivity>()
            R.id.nav_history -> startActivity<HistoryActivity>()
            R.id.nav_summary -> {

            }
            R.id.nav_payments -> {

            }
            R.id.nav_settings -> {

            }
            R.id.nav_help -> {

            }
            R.id.nav_change_to_service_provider -> changeToServiceProviderView()

            R.id.nav_add_service -> startActivity<NewAddServiceActivity>()
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        mLocationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true
                }
            }
            READ_PHONE_PERM -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    sendFcmTokenToServer()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            SEARCH_PLACE -> when (resultCode) {
                Activity.RESULT_OK -> {
//                    data?.let { intent ->
//                        val place = intent.getSerializableExtra(SEARCH_PLACE_RESULT) as Place
//                        textLocation.text = place.primaryText.toEditable()
//
//                        val placeTask: Task<PlaceBufferResponse> = mGeoDataClient.getPlaceById(place.placeId)
//
//                        placeTask.addOnCompleteListener(this) { task ->
//                            if (task.isSuccessful) {
//                                val places = task.result
//                                places?.let {
//                                    val placeResult = it[0]
//                                    Timber.e("${placeResult.latLng}")
//                                    if (mServiceLocation == null) mServiceLocation = Location("dummyprovider")
//                                    mServiceLocation?.longitude = placeResult.latLng.longitude
//                                    mServiceLocation?.latitude = placeResult.latLng.latitude
//
//                                    selectedPlaceMarker?.position = placeResult.latLng
//
//                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
//                                            mServiceLocation?.let { location -> LatLng(location.latitude, location.longitude) }, DEFAULT_ZOOM))
//                                }
//
//                                task.result?.release()
//                            } else {
//
//                            }
//                        }
//                    }
                }
            }
            GET_SUB_CATEGORY -> when (resultCode) {
                Activity.RESULT_OK -> {
//                    data?.apply {
//                        val selectedServiceCategory = this.getSerializableExtra(SUB_CATEGORY_CODE) as ServiceSubCategory
//
//                        serviceSubCategory = selectedServiceCategory
//                        textServiceCategory.text = serviceSubCategory.subCategoryName.toEditable()
//                    }
                }
                Activity.RESULT_CANCELED -> {

                }
            }
        }
    }

    private fun setUpViews() {
//        initMap()

//        val appBarConfiguration = AppBarConfiguration(navController.graph, drawer_layout)
//        toolbar.setupWithNavController(navController, appBarConfiguration)

//        navController.addOnNavigatedListener { _, destination ->
//            Timber.e("Destination ${destination.id}")
//            if (destination.id == R.id.serviceCategoryFragment) {
//                supportActionBar?.title = ""
//            }
//        }

        val servicesRecyclerViewAdapter = ServicesRecyclerViewAdapter(object : ServicesRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: Service) {
                Timber.e("Location: $serviceRequest")
                startActivity<ServiceDetailsActivity>(Pair(SERVICE_EXTRA, item), Pair(SERVICE_LOCATION, serviceRequest.location.toDoubleArray()))
            }
        })

        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetMain)

        with(listServices) {
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = mLayoutManager
            adapter = servicesRecyclerViewAdapter
        }

        bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        bottomSheetBehavior.peekHeight = 0

//        buttonRequestService.setOnClickListener { _ ->
//            bottomSheetBehavior.isHideable = true
//
//            serviceRequest.serviceCategoryCode = serviceSubCategory.subCategoryCode
//            mServiceLocation?.let {
//                serviceRequest.location[0] = it.latitude
//                serviceRequest.location[1] = it.longitude
//            }
//
//            viewModel.loadServicesBySubCategoryAndLocation(serviceRequest).observe(this, Observer { resource ->
//                binding.resource = resource
//                resource?.data?.let { list ->
//                    servicesRecyclerViewAdapter.submitList(list)
//
//                    bottomSheetBehavior.isHideable = false
//                    bottomSheetBehavior.peekHeight = 168
//
//                }
//            })
//        }
//
//
//        textLocation.setOnClickListener {
//            startActivityForResult<PlacesSearchActivity>(SEARCH_PLACE)
//        }
//
//        textServiceCategory.setOnClickListener {
//            startActivityForResult<ServiceCategoryActivity>(GET_SUB_CATEGORY, Pair(FORWARD_FLOW, false))
//        }
//
//        imageBtnNotification.setOnClickListener {
//            startActivity<ServiceCategoryActivity>()
//        }
    }

    private fun observeData() {
        viewModel.fetchUser().observe(this, Observer { result ->

            Timber.e("Data: ${result?.data}")
            result?.let { resource ->
                resource.data?.let { user ->
                    this@MainDrawerActivity.user = user
                    if (user.userType == "CLIENT") {
                        nav_view.menu.findItem(R.id.nav_your_services)?.let {
                            it.isVisible = false
                            it.setEnabled(false)
                        }

                        nav_view.menu.findItem(R.id.nav_summary)?.let {
                            it.isVisible = false
                            it.setEnabled(false)
                        }

                        nav_view.menu.findItem(R.id.nav_portfolio)?.let {
                            it.isVisible = false
                            it.setEnabled(false)
                        }
                        nav_view.menu.findItem(R.id.nav_change_to_service_provider)?.let {
                            it.isVisible = true
                            it.setEnabled(true)
                        }
                        nav_view.menu.findItem(R.id.nav_add_service)?.let {
                            it.isVisible = false
                            it.setEnabled(false)
                        }
                    } else {
                        nav_view.menu.findItem(R.id.nav_your_services)?.let {
                            it.isVisible = true
                            it.setEnabled(true)
                        }

                        nav_view.menu.findItem(R.id.nav_summary)?.let {
                            it.isVisible = true
                            it.setEnabled(true)
                        }

                        nav_view.menu.findItem(R.id.nav_portfolio)?.let {
                            it.isVisible = true
                            it.setEnabled(true)
                        }
                        nav_view.menu.findItem(R.id.nav_change_to_service_provider)?.let {
                            it.isVisible = false
                            it.setEnabled(false)
                        }
                        nav_view.menu.findItem(R.id.nav_add_service)?.let {
                            it.isVisible = true
                            it.setEnabled(true)
                        }
                    }

                    if (user.firstTimeLogin) updateUserSettings()

                    binding.navView.textHeaderTitle?.text = user.fullName
                    GlideApp.with(this)
                            .load("http://45.79.18.37:8080/"+user.profileImagePath)
                            .into(binding.navView.imageView)

                    binding.navView.imageView
                }
            }
        })


    }

    private fun initMap() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)
    }

    fun customDrawerButton(view: View) {
        when (drawer_layout.isDrawerOpen(nav_view)) {
            true -> drawer_layout.closeDrawer(GravityCompat.START)
            else -> drawer_layout.openDrawer(GravityCompat.START)
        }
    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                val locationResult: Task<Location> = mFusedLocationProviderClient.lastLocation

                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        Timber.e("${task.result}")
                        mLastKnownLocation = task.result

                        mLastKnownLocation?.let { location ->

                            mServiceLocation = location
                            FetchAddressIntentService.start(this, resultReceiver, mServiceLocation!!)
                            Timber.e("Location not null")
                            val lastKnownLatLong = LatLng(location.latitude, location.longitude)

                            selectedPlaceMarker = mMap.addMarker(MarkerOptions().position(lastKnownLatLong).title("Current Location"))

                            // Set the map's camera position to the current location of the device.
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    LatLng(location.latitude, location.longitude), DEFAULT_ZOOM))
                        }

                    } else {
                        Timber.e("Current location is null. Using defaults.")
                        Timber.e("Exception: ${task.exception}")
                        mMap.moveCamera(CameraUpdateFactory
                                .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM))
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Timber.e("Exception: ${e.message}")
        }
    }

    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.applicationContext,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf<String>(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        }
    }

    private fun updateLocationUI() {
        try {
            if (mLocationPermissionGranted) {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                mLastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Timber.e("Exception: ${e.message}")
        }

    }

    private fun changeToServiceProviderView() {
        startActivity(intentFor<BecomeServiceProviderActivity>().putExtra(INTENT_USER_PHONE_NUMBER, user?.username))
    }

    private fun updateUserSettings() {
        startActivity(intentFor<ProfileSettingsActivity>().putExtra(INTENT_USER_PHONE_NUMBER, user?.username))
    }

    private fun sendFcmTokenToServer() {

        if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_PHONE_STATE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.READ_PHONE_STATE),
                        READ_PHONE_PERM)

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            if (sharedPreferences.isFcmTokenChanged) {
                viewModel.sendFcmRegistration(sharedPreferences.fcmToken, telephonyManager.imei
                        ?: "").observe(this, Observer { result ->
                    Timber.e("Data: ${result?.data}")
                    result?.let {
                        when (it.status) {

                            Status.LOADING -> {
                            }
                            Status.SUCCESS -> {
                                sharedPreferences.isFcmTokenChanged = false
                            }
                            Status.FAILED -> {
                                Timber.e("Failed to send fcm token to server.")
                            }
                        }
                    }
                })
            }
        }
    }

    inner class AddressResultReceiver(handler: Handler) : ResultReceiver(handler) {

        override fun onReceiveResult(resultCode: Int, resultData: Bundle?) {

            // Display the address string
            // or an error message sent from the intent service.
            val addressOutput = resultData?.getString(Constants.RESULT_DATA_KEY) ?: ""
//            displayAddressOutput()

            // Show a toast message if an address was found.
            if (resultCode == Constants.SUCCESS_RESULT) {
                Timber.e(getString(R.string.address_found))
                Timber.e("Found address: $addressOutput")

//                textLocation.text = Editable.Factory().newEditable(addressOutput)
            } else {
                Timber.e(getString(R.string.no_address_found))
                Timber.e("Address not found: $addressOutput")
            }

        }
    }
}
