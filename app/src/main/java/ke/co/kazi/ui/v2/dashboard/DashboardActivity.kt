package ke.co.kazi.ui.v2.dashboard

import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.ViewModelFactory
import ke.co.kazi.api.models.data.ProviderType
import ke.co.kazi.ui.v2.merchant.MerchantActivity
import ke.co.kazi.ui.v2.onboarding.CreateServiceProviderActivity
import ke.co.kazi.viewmodel.ServiceViewModel
import ke.co.kazi.viewmodel.UserProfileViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_dashboard.*
import org.jetbrains.anko.*
import javax.inject.Inject


class DashboardActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: UserProfileViewModel
    private lateinit var serviceViewModel: ServiceViewModel

    private var loading: Boolean = false
//        set(value) {
//            loadingView.visibility = when(value){
//                true -> View.VISIBLE
//                false -> View.GONE
//            }
//            field = value
//        }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        setContentView(ke.co.kazi.R.layout.activity_dashboard)
        setupViews()

        viewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(UserProfileViewModel::class.java)
        serviceViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ServiceViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        showMerchantViews()
    }

    private fun becomeServiceProvider() {
        val serviceProviderType = arrayOf(getString(R.string.title_individual), getString(R.string.title_company))

        var dialog: AlertDialog? = null
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.string_choose_profile))
        builder.setItems(serviceProviderType) { _, selectedType ->
            val type = when(selectedType){
                0 -> ProviderType.TYPE_INDIVIDUAL
                1 -> ProviderType.TYPE_COMPANY
                else -> throw IllegalStateException("Unknown Type")
            }

            serviceViewModel.createServiceProvider(type).observe(this, Observer { resource ->
                when(resource.status){
                    Status.LOADING -> dialog = showProgressDialog(dialog)
                    Status.SUCCESS -> {
                        dialog?.dismiss()
                        navigate(type)
                    }
                    Status.FAILED -> {
                        dialog?.dismiss()
                        dialog = builder.show()
                    }
                }
            })
        }
        dialog = builder.show()
    }

    private fun getProgressView(): View {
        val layout = RelativeLayout(this)

        val progressBar = ProgressBar(this, null, android.R.attr.progressBarStyleLarge)
        progressBar.isIndeterminate = true
        progressBar.visibility = View.VISIBLE

        val params = RelativeLayout.LayoutParams(100, 100)
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        params.topMargin = dip(40)
        params.bottomMargin = dip(40)
        layout.addView(progressBar, params)

        return layout
    }

    private fun navigate(type: String) {
        CreateServiceProviderActivity.startCreateServiceProviderActivity(this,
                when(type){
                    ProviderType.TYPE_COMPANY -> CreateServiceProviderActivity.FLOW_COMPANY
                    ProviderType.TYPE_INDIVIDUAL -> CreateServiceProviderActivity.FLOW_INDIVIDUAL
                    else -> throw IllegalStateException("Unknown Type")
                })
    }

    private fun setupViews() {
        merchantSwitchView.setOnClickListener { startActivity(intentFor<MerchantActivity>().clearTask().clearTop().newTask()) }
        serviceProviderButton.setOnClickListener { becomeServiceProvider() }
    }

    private fun showMerchantViews() {
        viewModel.fetchUser().observe(this, Observer { resource ->
            loading = false
            when(resource.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> {
                    when(resource.data?.userType){
                        "CLIENT" -> {
                            serviceProviderButton.visibility = View.VISIBLE
                            merchantSwitchView.visibility = View.GONE
                        }
                        "SP" -> {
                            serviceProviderButton.visibility = View.GONE
                            merchantSwitchView.visibility = View.VISIBLE
                        }
                        else -> longToast("Unknown User Type. Contact System Admin")
                    }
                }
                Status.FAILED -> throw IllegalStateException("User not loaded.")
            }
        })
    }

    private fun showProgressDialog(dialog: AlertDialog?): AlertDialog? {
        if (dialog == null) return null

        val builder = AlertDialog.Builder(this)
                .setView(getProgressView())

        return builder.show()
    }
}
