package ke.co.kazi.ui.v2.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.model.Service
import ke.co.kazi.ui.v2.merchant.ServiceActivity
import ke.co.kazi.util.BASE_URL
import kotlinx.android.synthetic.main.layout_service.view.*

class ServicesAdapter(private val services: ArrayList<Service>):
        RecyclerView.Adapter<ServicesAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater
                .from(parent.context)
                .inflate(R.layout.layout_service, parent, false))
    }

    override fun getItemCount(): Int = services.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(services[position])
    }

    class ViewHolder(private val view: View): RecyclerView.ViewHolder(view) {
        fun bind(service: Service){
            view.apply {
                service.images.getOrNull(0)?.path.let { image ->
                    GlideApp.with(view.context)
                            .load(BASE_URL + image)
                            .into(circleImageView)
                }
                nameTextView.text = service.name
                descTextView.text = service.categoryCode.toLowerCase().capitalize()
                setOnClickListener { ServiceActivity.newInstance(context, service.serverId!!) }
            }
        }
    }
}