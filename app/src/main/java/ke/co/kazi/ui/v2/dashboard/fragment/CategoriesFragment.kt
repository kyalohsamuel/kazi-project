package ke.co.kazi.ui.v2.dashboard.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import droidninja.filepicker.utils.GridSpacingItemDecoration
import ke.co.kazi.R
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.ui.v2.adapter.CategoriesAdapter
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_categories.*
import org.jetbrains.anko.dip

class CategoriesFragment : CategoryFragment() {

    private lateinit var categoriesAdapter: CategoriesAdapter
    private lateinit var staticCategories: List<ServiceCategory>
    private val categories = arrayListOf<ServiceCategory>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
        loadCategories()
    }

    private fun loadCategories() {
        viewModel.loadServices().observe(this, Observer { resource ->
            when (resource.status) {
                Status.LOADING -> {}
                Status.SUCCESS -> {
                    resource.data?.let {
                        staticCategories = it
                        updateCategories(staticCategories)
                    }
                }
                Status.FAILED -> view?.let { handleNetworkError(resource, it) }
            }
        })
    }

    private fun performSearch() {
        updateCategories(staticCategories, false)

        val searchTerm = searchEditText.text.toString()
        if(searchTerm.trim().isNotBlank()){
            val searchedCategories =
                    categories.filter {
                        it.categoryName.contains(searchTerm, true) }

            updateCategories(searchedCategories, false)
        }
        categoriesAdapter.notifyDataSetChanged()
    }

    private fun setViews() {
        categoriesAdapter = CategoriesAdapter(categories)
        recyclerView.layoutManager = GridLayoutManager(context, 3)
        recyclerView.addItemDecoration(GridSpacingItemDecoration(3,
                context?.dip(8) ?: 8, false))
        recyclerView.adapter = categoriesAdapter

        searchEditText.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener when(actionId == EditorInfo.IME_ACTION_SEARCH){
                true -> {
                    performSearch()
                    true
                }
                false -> false
            }
        }
    }

    private fun updateCategories(data: Iterable<ServiceCategory>?, notifyAdapter: Boolean = true) {
        categories.apply {
            clear()
            data?.let { addAll(it) }
        }

        if(notifyAdapter) categoriesAdapter.notifyDataSetChanged()
    }

}
