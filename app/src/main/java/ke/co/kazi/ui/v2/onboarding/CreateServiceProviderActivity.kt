package ke.co.kazi.ui.v2.onboarding

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import ke.co.kazi.R
import kotlinx.android.synthetic.main.activity_service_provider.*

class CreateServiceProviderActivity : AppCompatActivity() {

    companion object {
        const val ARG_FLOW = "ARG_FLOW"

        const val FLOW_INDIVIDUAL = 100
        const val FLOW_COMPANY = 200

        fun startCreateServiceProviderActivity(context: Context?, flow: Int){
            val intent = Intent(context, CreateServiceProviderActivity::class.java)
            intent.putExtra(ARG_FLOW, flow)
            context?.startActivity(intent)
        }
    }

    private lateinit var navController: NavController
    var flow: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_provider)

        intent?.extras?.getInt(ARG_FLOW)?.let {
            flow = it
        }

        titleTextView.text = when(flow){
            FLOW_INDIVIDUAL -> getString(R.string.title_individual_service_provider)
            FLOW_COMPANY -> getString(R.string.title_company_service_provider)
            else -> throw IllegalAccessException()
        }

        setupActionBar()
        setupNavController()
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp()

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = null
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupNavController() {
        navController = Navigation.findNavController(this, R.id.navigationFragment)

        navController.popBackStack()
        navController.navigate(when (flow) {
            FLOW_INDIVIDUAL -> R.id.createIndividualServiceProvider
            FLOW_COMPANY -> R.id.createCompanyServiceProvider
            else -> throw IllegalAccessException()
        })

        setupNavControllerListener()
    }

    private fun setupNavControllerListener() {
        navController.addOnDestinationChangedListener { _, destination, _ ->
            val (subTitle, stepTitle) = when(destination.id){
                R.id.createIndividualServiceProvider, R.id.createCompanyServiceProvider ->
                    Pair(getString(R.string.title_identification),String.format(getString(R.string.string_step_count), 1, 3))
                R.id.createCertificationsServiceProvider -> Pair(getString(R.string.title_certifications), String.format(getString(R.string
                        .string_step_count), 2, 3))
                R.id.createProfileDetails -> Pair(getString(R.string.title_profile_details),
                        String.format(getString(R.string.string_step_count), 3, 3))
                else -> throw IllegalStateException()
            }

            subTitleTextView.text = subTitle
            stepCounterTextView.text = stepTitle

            val(firstResource, secondResource, thirdResource) = when(destination.id){
                R.id.createIndividualServiceProvider, R.id.createCompanyServiceProvider ->
                    Triple(R.drawable.ic_step_undone, R.drawable.ic_step_undone, R.drawable.ic_step_undone)
                R.id.createCertificationsServiceProvider ->
                    Triple(R.drawable.ic_step_done_14dp, R.drawable.ic_step_undone, R.drawable.ic_step_undone)
                R.id.createProfileDetails ->
                    Triple(R.drawable.ic_step_done_14dp, R.drawable.ic_step_done_14dp, R.drawable.ic_step_undone)
                else -> throw IllegalStateException()
            }

            step1View.setImageResource(firstResource)
            step2View.setImageResource(secondResource)
            step3View.setImageResource(thirdResource)
        }
    }
}
