package ke.co.kazi.ui.v1

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.models.data.CertificationDetails
import ke.co.kazi.api.models.data.PersonalDetails
import ke.co.kazi.api.models.data.ProfessionalDetails
import ke.co.kazi.api.models.data.ServiceProviderDetail
import ke.co.kazi.binding.BindingConverter
import ke.co.kazi.databinding.ActivityServiceProviderInfoBinding
import ke.co.kazi.ui.v1.views.OneButtonDialogFragment
import ke.co.kazi.viewmodel.UserProfileViewModel

import kotlinx.android.synthetic.main.activity_service_provider_info.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.startActivity
import timber.log.Timber
import javax.inject.Inject

class ServiceProviderInfoActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: UserProfileViewModel

    private lateinit var binding: ActivityServiceProviderInfoBinding

    private val serviceProviderDetail: ServiceProviderDetail = ServiceProviderDetail()
    private val personalDetails = PersonalDetails()
    private val professionalDetails = ProfessionalDetails()
    private val certificationDetails = CertificationDetails()

    private val documentTypes = arrayOf("NATIONAL ID", "PASSPORT")
    private val professions = arrayOf("DOCTOR", "MECHANIC", "TEACHER", "DRIVER")

    private var username: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_service_provider_info)
        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[UserProfileViewModel::class.java]
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.personal = personalDetails
        binding.professional = professionalDetails
        binding.certificate = certificationDetails
        binding.convertor = BindingConverter()

        username = intent?.getStringExtra(INTENT_USER_PHONE_NUMBER) ?: ""

        spinnerDocumentType.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, documentTypes)
        spinnerProfession.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, professions)

        observeViews()
    }

    private fun observeViews() {
        spinnerDocumentType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                if (position == Spinner.INVALID_POSITION) {
                    return
                }
                personalDetails.identificationType = parent.getItemAtPosition(position) as String
            }

        }

        spinnerProfession.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                if (position == Spinner.INVALID_POSITION) {
                    return
                }
                professionalDetails.professionalName = parent.getItemAtPosition(position) as String
            }

        }
    }


    fun onDetailSubmit(view: View) {
        if (validate()) {
            serviceProviderDetail.personalDetails = personalDetails
            serviceProviderDetail.certificationDetails = certificationDetails
            serviceProviderDetail.professionalDetails = professionalDetails

            viewModel.sendServiceProviderDetails(serviceProviderDetail).observe(this, Observer { resource ->
                resource?.let {
                    binding.resource = it
                }
                resource?.data?.let {
                    Timber.e("Sent service provider details")

                    OneButtonDialogFragment.newInstance(
                            loadAnimation = true,
                            titleRes = R.string.title_success,
                            messageRes = R.string.title_success_add_provider,
                            buttonTextRes = R.string.action_success_add_provider,
                            buttonDialogAction = object : OneButtonDialogFragment.ButtonDialogAction {
                                override fun onSecondActionClicked() {

                                }

                                override fun onButtonClicked() {
                                    if (username.isEmpty()) {
                                        startActivity<CheckMobileActivity>()
                                    } else {
                                        startActivity(intentFor<LoginActivity>().putExtra(INTENT_USER_PHONE_NUMBER, username))
                                    }
                                    finishAffinity()
                                }

                            }).show(supportFragmentManager, "")
                }
            })
        }
    }

    private fun validate(): Boolean {
        textDocumentNumber.error = null
        textShortDescription.error = null
        textInputCourse.error = null
        textInputLayoutCertificateNumber.error = null
        textInputLayoutCertificateDetails.error = null
        textInputLayoutInstitutionName.error = null
        textLayoutDescription.error = null

        return when {
            personalDetails.identificationNumber.isEmpty() -> {
                textDocumentNumber.error = "Enter document type."
                textDocumentNumber.requestFocus()
                false
            }
            personalDetails.description.isEmpty() -> {
                textShortDescription.error = "Enter Description."
                textShortDescription.requestFocus()
                false
            }
            professionalDetails.details.isEmpty() -> {
                textLayoutDescription.error = "Enter professional description"
                textLayoutDescription.requestFocus()
                false
            }
            certificationDetails.certificateName.isEmpty() -> {
                textInputCourse.error = "Enter course name."
                textInputCourse.requestFocus()
                false
            }
            certificationDetails.institution.isEmpty() -> {
                textInputLayoutInstitutionName.error = "Enter institution name."
                textInputLayoutInstitutionName.requestFocus()
                false
            }
            certificationDetails.certificateNumber.isEmpty() -> {
                textInputLayoutCertificateNumber.error = "Enter certificate number."
                textInputLayoutCertificateNumber.requestFocus()
                false
            }
            certificationDetails.details.isEmpty() -> {
                textInputLayoutCertificateDetails.error = "Enter certificate details."
                textInputLayoutCertificateDetails.requestFocus()
                false
            }

            else -> true
        }
    }
}
