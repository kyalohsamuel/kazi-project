package ke.co.kazi.ui.v2.adapter

import android.content.res.Resources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import ke.co.kazi.R
import ke.co.kazi.ui.v2.onboarding.TutorialFragment
import java.lang.IllegalStateException

class TutorialPagerAdapter(private val res: Resources,
                           fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager){
    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> TutorialFragment.newInstance(
                    res.getString(R.string.title_visibility),
                    res.getString(R.string.string_visibility),
                    R.drawable.img_visibility)
            1 -> TutorialFragment.newInstance(
                    res.getString(R.string.title_connect),
                    res.getString(R.string.string_connect),
                    R.drawable.img_connect)
            2 -> TutorialFragment.newInstance(
                    res.getString(R.string.title_visibility_2),
                    res.getString(R.string.string_visibility_2),
                    R.drawable.img_visibility)
            else -> throw IllegalStateException("Invalid tutorial fragment position")
        }
    }

    override fun getCount(): Int {
        return 3
    }

}