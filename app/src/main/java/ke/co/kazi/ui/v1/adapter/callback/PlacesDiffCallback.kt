package ke.co.kazi.ui.v1.adapter.callback

import androidx.recyclerview.widget.DiffUtil
import ke.co.kazi.vo.Place

class PlacesDiffCallback : DiffUtil.ItemCallback<Place>() {
    override fun areItemsTheSame(oldItem: Place, newItem: Place): Boolean {
        return oldItem?.placeId == newItem?.placeId
    }

    override fun areContentsTheSame(oldItem: Place, newItem: Place): Boolean {
        return oldItem == newItem
    }
}