package ke.co.kazi.ui.v2.onboarding


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment

import ke.co.kazi.R
import kotlinx.android.synthetic.main.fragment_tutorial.*
import org.jetbrains.anko.bundleOf

class TutorialFragment : Fragment() {

    private lateinit var title: String
    private lateinit var desc: String
    private var imageRes: Int = 0

    companion object {
        private const val ARG_TITLE = "title"
        private const val ARG_DESC = "desc"
        private const val ARG_DRAWABLE = "drawable"

        fun newInstance(title: String, desc: String, @DrawableRes drawable: Int): TutorialFragment{
            val fragment = TutorialFragment()
            val bundle = bundleOf(
                    ARG_TITLE to title,
                    ARG_DESC to desc,
                    ARG_DRAWABLE to drawable
            )

            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            title = getString(ARG_TITLE, "")
            desc = getString(ARG_DESC, "")
            imageRes = getInt(ARG_DRAWABLE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tutorial, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleTextView.text = title
        descTextView.text = desc
        headerImageView.setImageDrawable(resources.getDrawable(imageRes))
    }
}
