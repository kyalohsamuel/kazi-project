package ke.co.kazi.ui.v1

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.google.android.gms.location.places.ui.PlacePicker
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.databinding.ActivityAddServiceBinding
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.util.toEditable
import ke.co.kazi.viewmodel.ServiceViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_add_service.*
import org.jetbrains.anko.startActivityForResult
import timber.log.Timber
import javax.inject.Inject


private const val PLACE_PICKER_REQUEST = 1

const val GET_SUB_CATEGORY = 1001

class AddServiceActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ServiceViewModel

    lateinit var binding: ActivityAddServiceBinding

    private val service = Service()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_service)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.service = service

        binding.textLocation.setOnClickListener {
            openPlacePicker()
        }

        binding.textServiceCategory.setOnClickListener {
            startActivityForResult<ServiceCategoryActivity>(GET_SUB_CATEGORY, Pair(FORWARD_FLOW, false))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PLACE_PICKER_REQUEST -> when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = PlacePicker.getPlace(data, this)
                    Timber.e("Place: ${place.name}")
                    service.locationName = place.name.toString()
                    service.location.coordinates[0] = place.latLng.latitude
                    service.location.coordinates[1] = place.latLng.longitude

                    textLocation.text = place.name.toString().toEditable()
                }
                Activity.RESULT_CANCELED -> {

                }
            }
            GET_SUB_CATEGORY -> when (resultCode) {
                Activity.RESULT_OK -> {
                    data?.apply {
                        val serviceCategory = this.getSerializableExtra(SUB_CATEGORY_CODE) as ServiceSubCategory
                        service.subCategoryCode = serviceCategory.categoryCode
                        textServiceCategory.text = serviceCategory.subCategoryName.toEditable()
                    }
                }
                Activity.RESULT_CANCELED -> {

                }
            }
        }
    }

    fun onAddService(view: View) {
        viewModel.createService(service).observe(this, Observer { resource ->
            Timber.e(resource.toString())
            binding.resource = resource
            resource?.let {
                when (it.status) {

                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> {
                        Toast.makeText(this, "Service created successfully.", Toast.LENGTH_LONG).show()
                        finish()
                    }
                    Status.FAILED -> Toast.makeText(this, "Failed to create service..", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun openPlacePicker() {
        val builder = PlacePicker.IntentBuilder()
        startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST)
    }
}
