package ke.co.kazi.ui.v2.onboarding.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import ke.co.kazi.R
import ke.co.kazi.api.models.data.MobileCheckRequest
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.model.Country
import ke.co.kazi.model.User
import ke.co.kazi.ui.v2.adapter.CountrySpinnerAdapter
import ke.co.kazi.util.reObserve
import ke.co.kazi.vo.LoginStatus
import ke.co.kazi.vo.Resource
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_check_number.*
import org.jetbrains.anko.AnkoLogger

class CheckNumberFragment : OnBoardingFragment(), AnkoLogger {

    private lateinit var phoneNumber: String
    private lateinit var countryCode: String
    private lateinit var requestOTPObserver: Observer<Resource<User>>

    private var loading: Boolean = false
        set(value) {
            isLoading(value)
            field = value
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_check_number, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupCountryCodes()
        getStartedButton.setOnClickListener {
            checkNumber()
        }

        phoneNoEditText.addTextChangedListener(ClearErrorTextWatcher(phoneNoInputLayout))
        checkNumberObserver()
    }



    private fun setupCountryCodes() {
        val adapter = CountrySpinnerAdapter(context!!)
        countryCodeSpinner.adapter = adapter

        viewModel.loadCountries().reObserve(this, Observer {
            it?.data?.let{ countries ->
                adapter.countries.clear()
                adapter.countries.addAll(countries)
                adapter.notifyDataSetChanged()
            }
        })
    }

    private fun checkNumber() {
        val phoneNumber = phoneNoEditText.text.toString()

        when(phoneNumber.isEmpty()){
            true -> phoneNoInputLayout.error = getString(R.string.error_phone_no_required)
            false -> {
                val (number, countryPrefix, _) = formatNumber(phoneNumber)

                this.phoneNumber = number
                this.countryCode = (countryCodeSpinner.selectedItem as Country).code
                checkNumber(number, countryPrefix)
            }
        }
    }

    private fun checkNumber(number: String, countryPrefix: String) {
        viewModel.checkMobileNumberStatus(MobileCheckRequest(number, countryPrefix))

        requestOTPObserver = Observer { response ->
            loading = false
            when(response.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> {
                    viewModel.requestOtp.removeObserver(requestOTPObserver)
                    view?.findNavController()?.navigate(CheckNumberFragmentDirections
                            .actionCheckNumberFragmentToVerificationCodeFragment(phoneNumber, countryCode))
                }
                Status.FAILED -> handleNetworkError(response)
            }
        }

        viewModel.requestOtp.observe(this, requestOTPObserver)
    }

    private fun checkNumberObserver() {
        viewModel.mobileCheckResponse.reObserve(this, Observer { response ->
            loading = false
            when(response.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> response.data?.code?.let { navigate(it) }
                Status.FAILED -> handleNetworkError(response)
            }
        })
    }

    private fun formatNumber(phoneNumber: String): Triple<String, String, String> {
        val countryPrefix = (countryCodeSpinner.selectedItem as Country).prefix
        return when(phoneNumber.startsWith("0")){
            true -> Triple(phoneNumber, countryPrefix, countryPrefix + phoneNumber.removePrefix("0"))
            false -> Triple("0$phoneNumber", countryPrefix, countryPrefix + phoneNumber)
        }
    }

    private fun navigate(status: String) {
        when (status) {
            LoginStatus.NEW_USER.value -> viewModel.requestOtp(phoneNumber)
            LoginStatus.USER_FOUND.value -> view?.findNavController()
                    ?.navigate(CheckNumberFragmentDirections.actionCheckNumberFragmentToLoginFragment(phoneNumber))
            else -> throw IllegalArgumentException()
        }

    }

    private fun isLoading(loading: Boolean){
        getStartedButton.text = when(loading){ true -> null false -> getString(R.string.action_get_started)}
        progressBar.visibility = when(loading){ true -> View.VISIBLE false -> View.GONE}
    }
}
