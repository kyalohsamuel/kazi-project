package ke.co.kazi.ui.v1

import androidx.lifecycle.ViewModelProvider
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.model.Message
import ke.co.kazi.model.MessageType
import ke.co.kazi.model.ServiceRequest
import ke.co.kazi.notification.KaziSocket
import ke.co.kazi.ui.v1.adapter.MessageAdapter
import ke.co.kazi.util.ChatTopics
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.MESSAGE_TYPE
import ke.co.kazi.util.SERVICE_REQUEST_EXTRA
import kotlinx.android.synthetic.main.activity_chat.*
import org.jetbrains.anko.longToast
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class ChatActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private var request: ServiceRequest? = null
    private lateinit var messageType: MessageType

    private var gson = Gson()

    private var messages = arrayListOf<Message>()

    private val messageAdapter by lazy {
        MessageAdapter(sharedPreferences.userId!!, messageType, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        request = intent?.getSerializableExtra(SERVICE_REQUEST_EXTRA) as ServiceRequest
        messageType = intent?.getSerializableExtra(MESSAGE_TYPE) as MessageType

        Timber.e(sharedPreferences.userId)
        Timber.e(messageType.toString())
        Timber.e(request?.requestId.toString())

        init()

        with(messageList) {
            layoutManager = androidx.recyclerview.widget.LinearLayoutManager(this@ChatActivity)
            adapter = messageAdapter
        }

    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        KaziSocket.reconnect(sharedPreferences)
    }

    private fun init() {

        requestChats()

//        KaziSocket.socket?.on(ChatTopics.REGISTRATION_SUCCESS) {
//            Timber.e("Registration Success: ${Arrays.toString(it)}")
//        }
//
//        KaziSocket.socket?.on(ChatTopics.REGISTRATION_ERROR) {
//            Timber.e("Registration Error: ${Arrays.toString(it)}")
//        }

        KaziSocket.socket?.on(ChatTopics.PRIVATE_MESSAGE) {
            Timber.e("Private message: ${Arrays.toString(it)}")

            runOnUiThread {
                val message = gson.fromJson(it[0].toString(), Message::class.java)

                if (message.type != messageType && message.requestId == request?.requestId?.toString()) {

                    message?.apply {
                        messages.add(message)

                        messages.forEach { Timber.e("Message List: $it") }

                        updateChatListView()
                    }
                    resetInput()
                } else {

                }
            }
        }

        KaziSocket.socket?.on(ChatTopics.CHAT_LIST) {
            Timber.e("Chat List: ${Arrays.toString(it)}")

            runOnUiThread {
                val listType = object : TypeToken<List<Message>>() {}.type
                messages = gson.fromJson(it[0].toString(), listType)
                updateChatListView()
            }

        }

        KaziSocket.socket?.on(Socket.EVENT_MESSAGE) {
            Timber.e("Sent message: ${Arrays.toString(it)}")

            runOnUiThread {
                val message = gson.fromJson(it[0].toString(), Message::class.java)

                message?.apply {
                    messages.add(message)

                    messages.forEach { Timber.e("Message List: $it") }

                    updateChatListView()
                }
                resetInput()
            }
        }
    }

    fun onSendMessage(view: View) {
        val to = if (!messages.isEmpty()) messages.first().from else null

        if (messageType == MessageType.SP && to == null) {
            longToast("The client must text you first")
            return
        }

        if (txtMessage.text.isNotEmpty()) {
            val message = Message(
                    "",
                    if (messageType == MessageType.SP) to!! else request?.serviceProvider?.userId.toString(),
                    sharedPreferences.userId!!,
                    txtMessage.text.toString(),
                    messageType,
                    request?.requestId.toString(),
                    Calendar.getInstance().timeInMillis.toString()
            )

            Timber.e("Message: %s", message.toString())

            KaziSocket.socket?.send(gson.toJson(message))
        }
    }

    private fun resetInput() {
        // Clean text box
        txtMessage.text.clear()

        // Hide keyboard
        val inputManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(
                currentFocus!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    private fun registration() {
        KaziSocket.socket?.emit(ChatTopics.REGISTER, sharedPreferences.userId)
    }

    private fun requestChats() {
        KaziSocket.socket?.emit(ChatTopics.REQUEST_CHATS, gson.toJson(ChatsRequest(request?.requestId.toString())))
    }

    private fun updateChatListView() {
        messageAdapter.submitList(null)
        messageAdapter.submitList(messages)
        if (messages.isNotEmpty())
            messageList.scrollToPosition(messages.lastIndex)
    }

    private inner class ChatsRequest(@SerializedName("request_id") val requestId: String)
}
