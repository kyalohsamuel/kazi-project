package ke.co.kazi.ui.v1.adapter


import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import ke.co.kazi.databinding.ServiceListItemBinding
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.ui.v1.adapter.callback.ServiceDiffCallback

/**
 * [RecyclerView.Adapter] that can display a [ServiceCategory]
 */
class ServicesRecyclerViewAdapter(val onItemSelectedListener: OnItemSelectedListener) : ListAdapter<Service, ServicesRecyclerViewAdapter.ViewHolder>(ServiceDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ServiceListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(val binding: ServiceListItemBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Service) {
            binding.root.setOnClickListener { onItemSelectedListener.onItemSelected(item) }
            binding.service = item
            binding.executePendingBindings()
        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(item: Service)
    }
}
