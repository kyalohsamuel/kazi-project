package ke.co.kazi.ui.v1

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.databinding.ActivityServiceCategoryBinding
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.adapter.MyServiceRecyclerViewAdapter
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.ServiceCategoryViewModel
import kotlinx.android.synthetic.main.activity_service_category.*
import org.jetbrains.anko.intentFor
import timber.log.Timber
import javax.inject.Inject

const val SUB_CATEGORY_CODE = "SUB_CATEGORY_CODE"
const val CATEGORY_CODE = 1
const val FORWARD_FLOW = "FORWARD_FLOW"

class ServiceCategoryActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private lateinit var categoryViewModel: ServiceCategoryViewModel

    lateinit var binding: ActivityServiceCategoryBinding

    private var forward: Boolean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_service_category)
        setSupportActionBar(toolbar)

        if (savedInstanceState != null) {
            with(savedInstanceState) {
                forward = getBoolean(FORWARD_FLOW)
            }

            Timber.e("On restore: $forward")
        } else {
            forward = intent?.getBooleanExtra(FORWARD_FLOW, true)!!

            Timber.e("On get intent: $forward")
        }

        //sharedPreferences.saveBoolean(FORWARD_FLOW, forward!!)

        categoryViewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceCategoryViewModel::class.java]

        val myServiceRecyclerViewAdapter = MyServiceRecyclerViewAdapter(object : MyServiceRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: ServiceCategory) {
                Timber.e("Calling activity: ${callingActivity?.className} &&&&&&&&& ${MainDrawerActivity::class.java.canonicalName}")
//                if (callingActivity?.className == MainDrawerActivity::class.java.canonicalName ||
//                        callingActivity?.className == AddServiceActivity::class.java.canonicalName) {
//                    forward = false
//                }
                Timber.e("Forward: $forward")
                startActivityForResult(intentFor<ServiceSubCategoryActivity>().putExtra(CATEGORY_EXTRA_DATA, item).putExtra(FORWARD_FLOW, forward!!), CATEGORY_CODE)

            }

        })
        with(list) {
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = mLayoutManager
            adapter = myServiceRecyclerViewAdapter


            val dividerItemDecoration = androidx.recyclerview.widget.DividerItemDecoration(this@ServiceCategoryActivity, mLayoutManager.orientation)
            addItemDecoration(dividerItemDecoration)
        }

        categoryViewModel.loadServices().reObserve(this, Observer {
            it?.data.apply {
                myServiceRecyclerViewAdapter.submitList(this)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CATEGORY_CODE -> {
                val intent = Intent()
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        data?.let {
                            val subCategory = data.getSerializableExtra(SUB_CATEGORY_CODE) as ServiceSubCategory
                            intent.putExtra(SUB_CATEGORY_CODE, subCategory)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                    }
                    else -> {
                        setResult(Activity.RESULT_CANCELED, intent)
                        finish()
                    }
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        Timber.e("On Save: $forward")
        outState?.run {
            putBoolean(FORWARD_FLOW, forward!!)
        }

        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onPause() {
        Timber.e("Forward: $forward")
        forward?.run {
            Timber.e("On Pause run: $this")
            sharedPreferences.saveBoolean(FORWARD_FLOW, this@run)
        }
        super.onPause()
    }

    override fun onResume() {
        if (forward == true)
            forward = sharedPreferences.getBoolean(FORWARD_FLOW, true)

        Timber.e("Forward $forward")
        super.onResume()
    }
}
