package ke.co.kazi.ui.v2.onboarding.fragment

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import ke.co.kazi.R
import ke.co.kazi.api.models.data.ValidateOtpData
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.model.User
import ke.co.kazi.util.reObserve
import ke.co.kazi.vo.Resource
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_verification_code.*
import timber.log.Timber


class VerificationCodeFragment : OnBoardingFragment() {

    private val args: VerificationCodeFragmentArgs by navArgs()
    private var sentCount: Long = 0
    private var countDownTimer: CountDownTimer? = null
    private var setupView: Boolean = false

    private lateinit var requestOTPObserver: Observer<Resource<User>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verification_code, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        verificationOtpView.setOtpCompletionListener { validateOTP() }
        setupObservers()
        setupResendOTPViews()
    }

    override fun onStop() {
        super.onStop()
        countDownTimer?.cancel()
    }

    private fun setupObservers() {
        requestOTPObserver = Observer { response ->
            when {
                setupView -> when(response.status){
                    Status.LOADING -> secondaryDescTextView.text = getText(R.string.string_desc_resending_otp)
                    Status.SUCCESS -> {
                        viewModel.requestOtp.removeObserver(requestOTPObserver)

                        setupResendOTPViews()
                        Toast.makeText(context, getString(R.string.string_resent_verification_code), Toast.LENGTH_SHORT).show()
                    }
                    Status.FAILED -> handleNetworkError(response)
                }
                else -> setupView = true
            }
        }
    }

    private fun setupResendOTPViews() {
        if (countDownTimer != null) countDownTimer?.cancel()

        countDownTimer = object : CountDownTimer(60 * (sentCount + 1) * 1000, 1000){
            override fun onFinish() {
                secondaryDescTextView.text = getText(R.string.string_desc_not_sent_sms)
                secondaryActionTextView.text = getText(R.string.action_resend)
                secondaryActionTextView.visibility = View.VISIBLE

                secondaryActionTextView.setOnClickListener { resendOTP() }
            }

            override fun onTick(millisUntilFinished: Long) {
                val minutes = millisUntilFinished / 1000 / 60
                val seconds = millisUntilFinished / 1000 % 60

                secondaryDescTextView.text = String.format(getString(R.string.desc_resend_text), minutes, seconds)
            }
        }
        sentCount+=1
        countDownTimer?.start()
    }

    private fun resendOTP(){
        secondaryActionTextView.visibility = View.GONE
        viewModel.requestOtp(args.phoneNumber)
        viewModel.requestOtp.reObserve(this, requestOTPObserver)
    }

    private fun validateOTP() {
        val otp = verificationOtpView.text.toString()
        val userLiveData = viewModel.validateOtp(ValidateOtpData(otp, args.phoneNumber))

        when(otp.isEmpty()){
            true -> verificationOtpView.error = getString(R.string.error_invalid_verification_code)
            false -> userLiveData.observe(this, Observer { response ->
                progressBar.visibility = View.GONE
                when(response.status){
                    Status.LOADING -> progressBar.visibility = View.VISIBLE
                    Status.SUCCESS -> {
                        userLiveData.removeObservers(this)
                        verificationOtpView.text = null
                        navigate()
                    }
                    Status.FAILED -> handleNetworkError(response)
                }
            })
        }
    }

    private fun navigate() {
        val action = VerificationCodeFragmentDirections
                .actionVerificationCodeFragmentToRegistrationFragment(args.phoneNumber, args.countryCode)

        view?.findNavController()?.navigate(action)
    }
}
