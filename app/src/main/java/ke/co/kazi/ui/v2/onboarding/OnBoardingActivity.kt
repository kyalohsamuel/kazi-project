package ke.co.kazi.ui.v2.onboarding

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import com.squareup.phrase.Phrase
import ke.co.kazi.R
import kotlinx.android.synthetic.main.activity_onboarding.*

class OnBoardingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ke.co.kazi.R.layout.activity_onboarding)

        setupNavControllerListener()
    }

    override fun onSupportNavigateUp(): Boolean =
            Navigation.findNavController(this, R.id.navigationFragment).navigateUp()

    private fun setupNavControllerListener() {
        Navigation.findNavController(this, R.id.navigationFragment)
                .addOnDestinationChangedListener { _, destination, arguments ->
            val(title, desc) = when(destination.id){
                R.id.checkNumberFragment -> Pair(getText(R.string.title_get_started),
                        getText(R.string.string_desc_get_started))
                R.id.verificationCodeFragment -> {
                    val phoneNumber = arguments?.getString("phoneNumber")
                            ?: getString(R.string.string_unknown_number)

                    val slicedPhoneNumber = when(phoneNumber.startsWith("+" )){
                        true -> phoneNumber.take(4)+"...."+phoneNumber.takeLast(2)
                        false -> phoneNumber
                    }

                    val desc = Phrase.from(getText(R.string.string_desc_verification_code))
                            .put("number", slicedPhoneNumber)
                            .format()

                    Pair(getText(R.string.title_verification_code), desc)
                }
                R.id.registrationFragment -> Pair(getString(R.string.title_register),
                        getString(R.string.string_desc_register))
                R.id.loginFragment -> Pair(getString(R.string.title_login),
                        null)
                else -> Pair(null, null)
            }

            when(title != null || desc != null){
                true -> {
                    titleTextView.visibility = View.VISIBLE
                    descTextView.visibility = View.VISIBLE

                    titleTextView.text = title
                    descTextView.text = desc
                }
                false -> {
                    titleTextView.visibility = View.GONE
                    descTextView.visibility = View.GONE
                }
            }

        }
    }
}
