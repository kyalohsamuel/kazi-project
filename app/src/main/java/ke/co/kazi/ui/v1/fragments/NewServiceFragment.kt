package ke.co.kazi.ui.v1.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import dagger.android.support.AndroidSupportInjection
import ke.co.kazi.GlideApp

import ke.co.kazi.R
import ke.co.kazi.databinding.FragmentNewServiceBinding
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.adapter.NewServicesRecyclerViewAdapter
import ke.co.kazi.util.SERVICE_EXTRA
import ke.co.kazi.viewmodel.ServiceViewModel
import kotlinx.android.synthetic.main.fragment_new_service.*
import org.jetbrains.anko.bundleOf
import timber.log.Timber
import javax.inject.Inject

const val SUB_CATEGORY_CODE = "SUB_CATEGORY_CODE"

class NewServiceFragment : androidx.fragment.app.Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: FragmentNewServiceBinding
    private lateinit var viewModel: ServiceViewModel

    var category: ServiceCategory? = null
    var serviceSubCategory: ServiceSubCategory? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            category = it.getSerializable(CATEGORY_EXTRA_DATA) as? ServiceCategory
            serviceSubCategory = it.getSerializable(SUB_CATEGORY_CODE) as? ServiceSubCategory
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentNewServiceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]

        binding.service = serviceSubCategory

        val bundle = bundleOf(SUB_CATEGORY_CODE to serviceSubCategory)
        btnSwitchToMap.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_newServiceFragment_to_serviceMapFragment, bundle))

        val servicesRecyclerViewAdapter = NewServicesRecyclerViewAdapter(0, object : NewServicesRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: Service) {
//                startActivity<ServiceDetailsActivity>(Pair(SERVICE_EXTRA, item))
                Timber.e("Click service: $item")

                val serviceBundle = bundleOf(SERVICE_EXTRA to item)
                Navigation.findNavController(view).navigate(R.id.action_newServiceFragment_to_serviceDetailFragment, serviceBundle)
            }

        })

        with(list){
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = mLayoutManager
            adapter = servicesRecyclerViewAdapter
        }

        serviceSubCategory?.let { serviceSubCategory ->
            viewModel.loadServicesBySubCategory(serviceSubCategory.categoryCode).observe(this, Observer { resource ->
                resource?.data?.let {
                    servicesRecyclerViewAdapter.submitList(it)
                }
            })
        }

        GlideApp.with(this)
                .load("http://45.79.18.37:8080"+category?.imageUrl)
                .fitCenter()
                .into(binding.circleImageView)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}
