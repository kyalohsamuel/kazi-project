package ke.co.kazi.ui.v1.fragments

import android.os.Bundle

interface NextPreviousListener {
    fun next(): Pair<Boolean, Bundle?>
    fun back(): Boolean
}