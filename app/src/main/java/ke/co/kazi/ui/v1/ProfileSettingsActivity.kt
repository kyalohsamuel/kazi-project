package ke.co.kazi.ui.v1

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import dagger.android.AndroidInjection
import droidninja.filepicker.FilePickerConst
import ke.co.kazi.R
import ke.co.kazi.api.models.data.ProfileSettingsData
import ke.co.kazi.databinding.ActivityProfileSettingsBinding
import ke.co.kazi.ui.v1.fragments.FilePickerFragment
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.FileUploadUtil
import ke.co.kazi.util.REQUEST_IMAGE_CAPTURE
import ke.co.kazi.viewmodel.MainViewModel
import ke.co.kazi.vo.AccountType
import ke.co.kazi.vo.Status

import kotlinx.android.synthetic.main.activity_profile_settings.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import timber.log.Timber
import java.io.File
import javax.inject.Inject

private val REQUEST_COVER_IMAGE = "REQUEST_COVER_IMAGE"

class ProfileSettingsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferences: CustomSharedPreferences

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityProfileSettingsBinding

    private val files = mutableListOf<Pair<String?, String>>()
    private val settings = ProfileSettingsData()
    private var username: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_settings)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        viewModel = ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]

        username = intent?.getStringExtra(INTENT_USER_PHONE_NUMBER) ?: ""

        setupViews()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            FilePickerConst.REQUEST_CODE_PHOTO -> {
                if (resultCode == Activity.RESULT_OK) {
                    val file = Pair(preferences.filePicked, data?.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA)?.get(0)
                            ?: "")
                    files.add(file)

                    updateView(preferences.filePicked, file.second)
                }
            }
            REQUEST_IMAGE_CAPTURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val file = Pair(preferences.filePicked, preferences.photoPath!!)
                    files.add(file)

                    updateView(preferences.filePicked, file.second)
                }
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupViews() {

        profile_image.setOnClickListener {
            pickUserImage()
        }

        spinnerAccountType.adapter = ArrayAdapter<AccountType>(this, android.R.layout.simple_spinner_dropdown_item, AccountType.values())
        spinnerAccountType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                if (position == Spinner.INVALID_POSITION) {
                    return
                }

                settings.accountType = (parent.getItemAtPosition(position) as AccountType)
            }

        }

        switchSMS.setOnCheckedChangeListener { _, isChecked -> settings.notificationSettings.sms = isChecked }
        switchNotifications.setOnCheckedChangeListener { _, isChecked -> settings.notificationSettings.inApp = isChecked }
        switchEmail.setOnCheckedChangeListener { _, isChecked -> settings.notificationSettings.email = isChecked }
    }

    private fun pickUserImage() {
        files.clear()
        FilePickerFragment.newInstance(REQUEST_COVER_IMAGE).show(supportFragmentManager, "Fragment")
    }

    private fun updateView(picked: String?, path: String) {
        Timber.e("Sring $picked")
        when (picked) {
            REQUEST_COVER_IMAGE -> {
                profile_image.setImageBitmap(BitmapFactory.decodeFile(files.get(0).second))
            }
        }
    }

    fun onDone(view: View) {
        saveSettingsData()
    }

    private fun saveSettingsData() {
        if (validate()) {
            viewModel.updateProfileSettings(settings).observe(this, Observer { resource ->
                binding.resource = resource
                resource?.status?.apply {
                    when (this) {
                        Status.SUCCESS -> {
                            if (files.isNotEmpty()) {
                                toast("User settings saved successfully, uploading Profile pic")
                                uploadProfilePhoto()
                            } else {
                                toast("User settings saved successfully.")
                                startActivity(intentFor<BecomeServiceProviderActivity>().putExtra(INTENT_USER_PHONE_NUMBER, username))
                                finish()
                            }
                        }
                        Status.FAILED -> {
                            toast("Failed to save user settings. Please try again.")
                        }
                        Status.LOADING -> {
                        }
                    }
                }
            })
        }
    }

    private fun validate(): Boolean {
        return when {
            files.isEmpty() -> {
                toast("Profile Picture must be set")
                false
            }
            else -> true
        }
    }

    private fun uploadProfilePhoto() {
        viewModel.uploadProfilePhoto(FileUploadUtil.prepareFilePart("image", File(files[0].second), this))
                .observe(this, Observer { resource ->
                    binding.resource = resource
                    resource?.status?.apply {
                        when (this) {
                            Status.SUCCESS -> {
                                toast("Profile picture uploaded successfully")
                                startActivity(intentFor<BecomeServiceProviderActivity>().putExtra(INTENT_USER_PHONE_NUMBER, username))
                                finish()
                            }
                            Status.FAILED -> {
                                toast("Failed to upload profile pic. Please try again.")
                                startActivity(intentFor<BecomeServiceProviderActivity>().putExtra(INTENT_USER_PHONE_NUMBER, username))
                                finish()
                            }
                            Status.LOADING -> {
                            }
                        }
                    }
                })
    }
}
