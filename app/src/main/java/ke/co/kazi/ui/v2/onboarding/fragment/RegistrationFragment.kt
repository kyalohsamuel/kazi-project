package ke.co.kazi.ui.v2.onboarding.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import ke.co.kazi.R
import ke.co.kazi.api.models.data.RegisterRequest
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.helper.isNotEmail
import ke.co.kazi.helper.requiredField
import ke.co.kazi.util.reObserve
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_registration.*

class RegistrationFragment : OnBoardingFragment() {

    private val args: VerificationCodeFragmentArgs by navArgs()
    private lateinit var pin: String

    private var loading: Boolean = false
        set(value) {
            isLoading(value)
            field = value
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        nameEditText.addTextChangedListener(ClearErrorTextWatcher(nameInputLayout))
        emailEditText.addTextChangedListener(ClearErrorTextWatcher(emailInputLayout))
        pinEditText.addTextChangedListener(ClearErrorTextWatcher(pinInputLayout))

        setupRegistrationObserver()
        getStartedButton.setOnClickListener { registration() }
    }

    private fun setupRegistrationObserver() {
        viewModel.register.reObserve(this, Observer { resource ->
            loading = false

            resource?.let {
                when (it.status) {
                    Status.LOADING -> loading = true
                    Status.SUCCESS -> {
                        viewModel.register.removeObservers(this)
                        navigate()
                    }
                    Status.FAILED -> handleNetworkError(resource)
                }
            }
        })
    }

    private fun registration() {
        val name = nameEditText.text.toString()
        val email = emailEditText.text.toString()
        pin = pinEditText.text.toString()
        val termsAccepted = tandcsCheckBox.isChecked

        when {
            name.isEmpty() -> nameInputLayout.error = getString(R.string.hint_full_name).requiredField()
            email.isEmpty() -> emailInputLayout.error = getString(R.string.label_email).requiredField()
            email.isNotEmail() -> emailInputLayout.error = getString(R.string.error_invalid_email)
            pin.isEmpty() -> pinInputLayout.error = getString(R.string.hint_pin).requiredField()
            !termsAccepted -> Snackbar.make(constraintLayout, R.string.error_tns_not_accepted, Snackbar.LENGTH_LONG).show()
            else -> registration(name, email, pin, termsAccepted)
        }
    }

    private fun registration(name: String, email: String, pin: String, termsAccepted: Boolean) {
        val request = RegisterRequest(pin, args.phoneNumber, name, email, args.countryCode, termsAccepted)
        viewModel.register(request)
    }

    private fun isLoading(loading: Boolean){
        pointedArrowImageView.visibility = when(loading){ true -> View.GONE false -> View.VISIBLE}
        progressBar.visibility = when(loading){ true -> View.VISIBLE false -> View.GONE}
    }

    private fun navigate(){
        view?.findNavController()?.navigate(RegistrationFragmentDirections.
                actionRegistrationFragmentToProfileFragment(args.phoneNumber, pin))
    }
}
