package ke.co.kazi.ui.v1

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.text.TextUtils
import android.view.View
import android.widget.Toast
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.models.data.RegisterRequest
import ke.co.kazi.databinding.ActivityRegisterBinding
import ke.co.kazi.viewmodel.LoginViewModel
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.longToast
import org.jetbrains.anko.startActivity
import timber.log.Timber
import javax.inject.Inject

class RegisterActivity : AppCompatActivity() {

    lateinit var binding: ActivityRegisterBinding

    private lateinit var viewModel: LoginViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val registerRequest = RegisterRequest()

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[LoginViewModel::class.java]
        binding.register = registerRequest

        registerRequest.contact = intent?.getStringExtra(INTENT_USER_PHONE_NUMBER) ?: ""
        registerRequest.countryCode = intent?.getStringExtra(INTENT_USER_COUNTRY_CODE) ?: ""

        btnReadTerms.setOnClickListener {
            onReadTnC()
        }

        subscribeUi()
    }

    private fun subscribeUi() {
        viewModel.register.observe(this, Observer { resource ->
            binding.resource = resource
            Timber.e("Register: $resource")
            resource?.let {
                when (it.status) {
                    Status.LOADING -> {
                    }
                    Status.SUCCESS -> startActivity(intentFor<LoginActivity>().putExtra(INTENT_USER_PHONE_NUMBER, registerRequest.contact))
                    Status.FAILED -> Toast.makeText(this, "An Error occurred while registering.", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    fun onSignUp(view: View) {
        if (validate()) viewModel.register(registerRequest)
    }

    fun onBackPressed(view: View) {
        finish()
    }

    private fun onReadTnC(){
        startActivity<TermAndConditionsActivity>()
    }

    private fun validate(): Boolean {
        when {
            TextUtils.isEmpty(registerRequest.name) -> {
                Toast.makeText(this, "Enter name.", Toast.LENGTH_LONG).show()
                return false
            }
            TextUtils.isEmpty(registerRequest.contact) -> {
                Toast.makeText(this, "Enter phone Number.", Toast.LENGTH_LONG).show()
                return false
            }
            TextUtils.isEmpty(registerRequest.email) -> {
                Toast.makeText(this, "Enter email.", Toast.LENGTH_LONG).show()
                return false
            }
            TextUtils.isEmpty(registerRequest.password) -> {
                Toast.makeText(this, "Enter password.", Toast.LENGTH_LONG).show()
                return false
            }
            TextUtils.isEmpty(binding.confirmPin.text.toString()) -> {
                Toast.makeText(this, "Enter name.", Toast.LENGTH_LONG).show()
                return false
            }
            registerRequest.tnc.not() -> {
                longToast("You have to agree with the terms and conditions to continue.")
                return false
            }
            else -> return if (binding.confirmPin.text.toString() != registerRequest.password) {
                Toast.makeText(this, "Passwords do not match!", Toast.LENGTH_LONG).show()
                false
            } else true
        }
    }
}
