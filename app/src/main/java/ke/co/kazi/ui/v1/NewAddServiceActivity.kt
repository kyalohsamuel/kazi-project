package ke.co.kazi.ui.v1

import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import ke.co.kazi.R
import kotlinx.android.synthetic.main.activity_new_add_service.*

class NewAddServiceActivity : AppCompatActivity() {

    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_add_service)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupViews()
    }

    override fun onSupportNavigateUp(): Boolean = navController.navigateUp()

    private fun setupViews(){
        navController = Navigation.findNavController(this, R.id.navigationFragment)

        navController.addOnDestinationChangedListener { _, destination, _ ->
            val step = when(destination.id){
                R.id.serviceInformationFragment -> 1
                R.id.serviceContactFragment -> 2
                R.id.otherInformationServiceFragment -> 3
                R.id.serviceImageUploadFragment -> 4
                else -> null
            } ?: return@addOnDestinationChangedListener

            val title = "Step $step of 4"

            stepCounterTextView.text = title
            subTitleTextView.text = destination.label

            @DrawableRes val firstResource: Int
            @DrawableRes val secondResource: Int
            @DrawableRes val thirdResource: Int
            @DrawableRes val fourthResource: Int


            when(destination.id){
                R.id.serviceInformationFragment -> {
                    firstResource = R.drawable.ic_step_undone
                    secondResource = R.drawable.ic_step_undone
                    thirdResource = R.drawable.ic_step_undone
                    fourthResource = R.drawable.ic_step_undone
                }
                R.id.serviceContactFragment -> {
                    firstResource = R.drawable.ic_step_done_14dp
                    secondResource = R.drawable.ic_step_undone
                    thirdResource = R.drawable.ic_step_undone
                    fourthResource = R.drawable.ic_step_undone
                }
                R.id.otherInformationServiceFragment -> {
                    firstResource = R.drawable.ic_step_done_14dp
                    secondResource = R.drawable.ic_step_done_14dp
                    thirdResource = R.drawable.ic_step_undone
                    fourthResource = R.drawable.ic_step_undone
                }
                R.id.serviceImageUploadFragment -> {
                    firstResource = R.drawable.ic_step_done_14dp
                    secondResource = R.drawable.ic_step_done_14dp
                    thirdResource = R.drawable.ic_step_done_14dp
                    fourthResource = R.drawable.ic_step_undone
                }
                else -> throw IllegalStateException()
            }

            step1View.setImageResource(firstResource)
            step2View.setImageResource(secondResource)
            step3View.setImageResource(thirdResource)
            step4View.setImageResource(fourthResource)

            return@addOnDestinationChangedListener
        }
    }
}
