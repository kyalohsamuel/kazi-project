package ke.co.kazi.ui.v1.fragments


import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import dagger.android.support.AndroidSupportInjection

import ke.co.kazi.R
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.ui.v1.adapter.ServiceCategoryRecyclerViewAdapter
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.ServiceCategoryViewModel
import kotlinx.android.synthetic.main.fragment_service_category.*
import org.jetbrains.anko.act
import org.jetbrains.anko.bundleOf
import timber.log.Timber
import javax.inject.Inject

class ServiceCategoryFragment : androidx.fragment.app.Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private lateinit var categoryViewModel: ServiceCategoryViewModel

    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_service_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryViewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceCategoryViewModel::class.java]

        val myServiceRecyclerViewAdapter = ServiceCategoryRecyclerViewAdapter(activity?.act!!, object : ServiceCategoryRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: ServiceCategory, view: View) {

                Timber.e("Clicked: $item")

                val bundle = bundleOf(CATEGORY_EXTRA_DATA to item)
                Navigation.findNavController(view).navigate(R.id.action_serviceCategoryFragment_to_serviceCategoryDetailsFragment, bundle)

//                Navigation.createNavigateOnClickListener(R.id.serviceCategoryDetailsFragment, null)
//                Timber.e("Calling activity: ${callingActivity?.className} &&&&&&&&& ${MainDrawerActivity::class.java.canonicalName}")
//                if (callingActivity?.className == MainDrawerActivity::class.java.canonicalName ||
//                        callingActivity?.className == AddServiceActivity::class.java.canonicalName) {
//                    forward = false
//                }
//                Timber.e("Forward: $forward")
//                startActivityForResult(intentFor<ServiceSubCategoryActivity>().putExtra(CATEGORY_EXTRA_DATA, item).putExtra(FORWARD_FLOW, forward!!), CATEGORY_CODE)

            }

        })
        with(list) {
            val mLayoutManager = androidx.recyclerview.widget.GridLayoutManager(context, 3)
            layoutManager = mLayoutManager
            adapter = myServiceRecyclerViewAdapter
        }

        categoryViewModel.loadServices().reObserve(this, Observer {
            it?.data.apply {
                myServiceRecyclerViewAdapter.submitList(this)
            }
        })
    }


    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

}
