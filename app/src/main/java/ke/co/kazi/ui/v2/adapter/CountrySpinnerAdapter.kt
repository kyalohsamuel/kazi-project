package ke.co.kazi.ui.v2.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.model.Country
import ke.co.kazi.ui.v1.services.Constants
import ke.co.kazi.util.BASE_URL
import kotlinx.android.synthetic.main.layout_country_code_picker.view.*

class CountrySpinnerAdapter(context: Context,
                            val countries: ArrayList<Country> = arrayListOf()):
        ArrayAdapter<Country>(context, R.layout.layout_country_code_picker, countries) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View
            = getCustomView(position, parent)

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View
            = getCustomView(position, parent)

    private fun getCustomView(position: Int, parent: ViewGroup?): View {
        return ViewHolder(LayoutInflater
                .from(parent?.context)
                .inflate(R.layout.layout_country_code_picker, parent, false)).apply {
            bind(countries[position])
        }.view
    }

    inner class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun bind(country: Country) {
            view.apply {
                countryCodeTextView.text = country.prefix

                val image = BASE_URL + country.imagePath
                GlideApp.with(view)
                        .load(image)
                        .circleCrop()
                        .into(countryFlagImageView)
            }
        }
    }
}