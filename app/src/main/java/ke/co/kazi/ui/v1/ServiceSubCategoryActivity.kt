package ke.co.kazi.ui.v1

import android.app.Activity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Intent
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.databinding.ActivityServiceSubCategoryBinding
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.ui.v1.adapter.ServiceSubCategoriesRecyclerViewAdapter
import ke.co.kazi.util.reObserve
import ke.co.kazi.viewmodel.ServiceCategoryViewModel

import kotlinx.android.synthetic.main.activity_service_sub_category.*
import org.jetbrains.anko.startActivity
import javax.inject.Inject

const val CATEGORY_EXTRA_DATA = "CATEGORY_EXTRA_DATA"

class ServiceSubCategoryActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var categoryViewModel: ServiceCategoryViewModel

    lateinit var binding: ActivityServiceSubCategoryBinding

    var category: ServiceCategory? = null
    var forward: Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_service_sub_category)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState != null) {
            with(savedInstanceState) {
                category = getSerializable(CATEGORY_EXTRA_DATA) as ServiceCategory
                forward = getBoolean(FORWARD_FLOW)
            }
        } else {
            category = intent?.getSerializableExtra(CATEGORY_EXTRA_DATA) as ServiceCategory
            forward = intent?.getBooleanExtra(FORWARD_FLOW, false)
        }

        category?.apply {
            supportActionBar?.let {
                it.title = this.categoryName
            }
        }

        categoryViewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceCategoryViewModel::class.java]

        val serviceSubCategoryRecyclerViewAdapter = ServiceSubCategoriesRecyclerViewAdapter(this, object : ServiceSubCategoriesRecyclerViewAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: ServiceSubCategory) {
                if (forward == true) {
                    startActivity<ServicesListActivity>(Pair(SUB_CATEGORY_CODE, item))
                } else {
                    val intent = Intent()
                    intent.putExtra(SUB_CATEGORY_CODE, item)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            }
        })

        with(list) {
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = mLayoutManager
            adapter = serviceSubCategoryRecyclerViewAdapter


            val dividerItemDecoration = androidx.recyclerview.widget.DividerItemDecoration(this@ServiceSubCategoryActivity, mLayoutManager.orientation)
            addItemDecoration(dividerItemDecoration)
        }

        categoryViewModel.loadServiceSubCategories(category?.categoryCode
                ?: "").reObserve(this, Observer {
            it?.data.apply {
                serviceSubCategoryRecyclerViewAdapter.submitList(this)
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        outState?.run {
            putSerializable(CATEGORY_EXTRA_DATA, category)
            putBoolean(FORWARD_FLOW, forward ?: false)
        }

        super.onSaveInstanceState(outState, outPersistentState)
    }
}
