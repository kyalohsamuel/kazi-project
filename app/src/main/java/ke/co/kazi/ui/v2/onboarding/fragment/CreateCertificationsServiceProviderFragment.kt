package ke.co.kazi.ui.v2.onboarding.fragment


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import ke.co.kazi.R
import ke.co.kazi.api.models.data.Certificate
import ke.co.kazi.helper.*
import ke.co.kazi.model.Certification
import ke.co.kazi.ui.v1.adapter.CertificationsRecyclerViewAdapter
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_create_certifications_service.*
import timber.log.Timber

class CreateCertificationsServiceProviderFragment : CreateServiceProviderFragment() {

    var certifications = arrayListOf<Certification>()
    lateinit var certificationsAdapter: CertificationsRecyclerViewAdapter

    var resultUri: Uri? = null

    override fun isLoading(loading: Boolean) {
        doneButton.isEnabled = !loading
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        resultUri = result.uri
                        qualificationImageEditText.setText(result.uri.lastPathSegment ?: null)
                    }
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> Timber.e(result.error)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_certifications_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
        doneButton.setOnClickListener {
            when {
                certNameEditText.text.toString().isNotEmpty() ||
                institutionEditText.text.toString().isNotEmpty() ||
                qualificationImageEditText.text.toString().isNotEmpty() -> addCertification()
                else -> view.findNavController()
                        .navigate(R.id.action_createCertificationsServiceProvider_to_createProfileDetails)
            }

        }
    }

    private fun setupViews() {
        addCertificationButton.setOnClickListener {
            addCertification()
        }

        certNameEditText.addTextChangedListener(ClearErrorTextWatcher(certNameInputLayout))
        institutionEditText.addTextChangedListener(ClearErrorTextWatcher(institutionInputLayout))
        qualificationTypeEditText.addTextChangedListener(ClearErrorTextWatcher(qualificationTypeInputLayout))
        qualificationImageEditText.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) return@setOnFocusChangeListener

            activity?.hideKeyboard()

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(context!!, this)
        }

        certificationsAdapter = CertificationsRecyclerViewAdapter(certifications)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = certificationsAdapter
    }

    private fun addCertification() {
        val name = certNameEditText.text.toString()
        val institution = institutionEditText.text.toString()
        val qualification = qualificationTypeEditText.text.toString()

        when {
            name.isBlank() -> certNameInputLayout.fieldError(getString(R.string.title_cert_name).requiredField())
            institution.isBlank() -> institutionInputLayout.fieldError(getString(R.string.title_institution).requiredField())
            qualification.isBlank() -> qualificationTypeInputLayout.fieldError(getString(R.string.title_qualification).requiredField())
            else -> {
                val certification = Certification(name, institution, qualification, resultUri)

                certifications.add(certification)
                certificationsAdapter.notifyItemChanged(certifications.size - 1)
                addCertification(certification, certifications.size - 1)

                titleCertificationsTextView.visibility = when(certifications.size > 0){
                    true -> View.VISIBLE
                    false -> View.GONE
                }

                //Reset Views
                listOf(certNameEditText, institutionEditText, qualificationTypeEditText, qualificationImageEditText)
                        .forEach { it.text = null }
                resultUri = null
            }
        }
    }

    private fun addCertification(certification: Certification, position: Int) {
        val certificate = Certificate(certification.name, null, certification.institution, certification.qualification)
        viewModel.addServiceProviderCertification(certificate).observe(this, Observer { response ->
            loading = false
            when(response.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> {
                    certification.syncing = false
                    certificationsAdapter.notifyItemChanged(position)
                }
                Status.FAILED -> {
                    certification.syncing = false
                    certificationsAdapter.notifyItemChanged(position)
                    handleNetworkError(response)
                }
            }
        })
    }
}