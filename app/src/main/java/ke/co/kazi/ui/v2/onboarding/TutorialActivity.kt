package ke.co.kazi.ui.v2.onboarding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ImageView
import androidx.viewpager.widget.ViewPager
import ke.co.kazi.R
import ke.co.kazi.ui.v2.adapter.TutorialPagerAdapter
import kotlinx.android.synthetic.main.activity_tutorial.*
import org.jetbrains.anko.*

class TutorialActivity : AppCompatActivity() {

    private lateinit var tutorialPagerAdapter: TutorialPagerAdapter
    private var sectionsCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)
        setupViewPager()
    }

    private fun setupViewPager() {
        tutorialPagerAdapter = TutorialPagerAdapter(resources, supportFragmentManager)
        viewPager.adapter = tutorialPagerAdapter
        setupProgressIndicator()

        continueButton.setOnClickListener {
            val relativePosition = viewPager.currentItem + 1
            when {
                relativePosition < sectionsCount ->
                    viewPager.setCurrentItem(relativePosition, true)
                else -> navigate()
            }
        }

        skipButton.setOnClickListener { navigate() }
    }

    private fun setupProgressIndicator() {
        sectionsCount = viewPager.adapter?.count ?: 0
        val imageSections = arrayListOf<ImageView>()

        for (x in 1..sectionsCount){
            val view = LayoutInflater
                    .from(this).inflate(R.layout.layout_section_view, progressLayout)
            val imageView = view.findViewById<ImageView>(R.id.imageView)
            imageSections.add(imageView)

            if(x == 1) setImageSection(imageView)
        }


        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                for((index, imageView) in imageSections.withIndex().reversed()){
                    setImageSection(imageView, position, index)
                }
            }

        })
    }

    private fun setImageSection(imageView: ImageView)
            = setImageSection(imageView, 0, 0)

    private fun setImageSection(imageView: ImageView, position: Int, index: Int) {
        imageView.setImageDrawable(when (position == index) {
            true -> resources.getDrawable(R.drawable.ic_active_view_10dp)
            false -> resources.getDrawable(R.drawable.ic_inactive_view_10dp)
        })
    }

    private fun navigate() {
        startActivity(intentFor<OnBoardingActivity>().clearTask().clearTop().newTask())
    }
}
