package ke.co.kazi.ui.v2.merchant

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import ke.co.kazi.R
import ke.co.kazi.ui.v1.NewAddServiceActivity
import ke.co.kazi.ui.v2.dashboard.DashboardActivity
import kotlinx.android.synthetic.main.activity_merchant.*
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask

class MerchantActivity : AppCompatActivity() {

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant)
        setSupportActionBar(toolbar)

        navController = Navigation.findNavController(this, R.id.navigationFragment)
        navigation.setupWithNavController(navController)
        collapsingToolbarLayout.setupWithNavController(toolbar, navController, AppBarConfiguration(setOf(
                R.id.serviceRequestsFragment,
                R.id.servicesFragment,
                R.id.profileFragment
        )))

        navController.addOnDestinationChangedListener { _, destination, _ ->
            when(destination.id == R.id.servicesFragment){
                true -> fab.show()
                false -> fab.hide()
            }
        }

        clientSwitchView.setOnClickListener { startActivity(intentFor<DashboardActivity>().clearTop().clearTask().newTask()) }
        fab.setOnClickListener { startActivity(intentFor<NewAddServiceActivity>()) }
    }

    override fun onSupportNavigateUp() = navController.navigateUp()
}
