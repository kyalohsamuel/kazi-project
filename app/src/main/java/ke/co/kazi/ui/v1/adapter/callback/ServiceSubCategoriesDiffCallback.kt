package ke.co.kazi.ui.v1.adapter.callback

import androidx.recyclerview.widget.DiffUtil
import ke.co.kazi.model.ServiceSubCategory

class ServiceSubCategoriesDiffCallback : DiffUtil.ItemCallback<ServiceSubCategory>() {
    override fun areItemsTheSame(oldItem: ServiceSubCategory, newItem: ServiceSubCategory): Boolean {
        return oldItem?.serviceId == newItem?.serviceId
    }

    override fun areContentsTheSame(oldItem: ServiceSubCategory, newItem: ServiceSubCategory): Boolean {
        return oldItem == newItem
    }
}