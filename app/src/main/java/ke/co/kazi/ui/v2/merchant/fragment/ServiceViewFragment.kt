package ke.co.kazi.ui.v2.merchant.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.model.Service
import ke.co.kazi.vo.Status
import org.jetbrains.anko.longToast

open class ServiceViewFragment: ServiceFragment(){
    companion object {
        const val ARG_SERVICE_ID = "service_id"
    }

    private var serviceID: Int = 0
        set(value) {
            if(value != 0) loadService()
            field = value
        }

    protected var service: Service? = null
        set(value){
            field = value
            if(value != null) setupViews()
        }

    protected var loading: Boolean = false
        set(value){
            isLoading(value)
            field = value
        }

    private fun loadService() {
        if(service != null) return

        viewModel.loadServices().observe(this, Observer { resource ->
            loading = false
            when(resource.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> service = resource.data?.first { it.serverId == serviceID }
                Status.FAILED -> handleNetworkError(resource)
            }
        })
    }

    protected fun updateService(){
        viewModel.updateService(service!!).observe(this, Observer { resource ->
            loading = false
            when(resource.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> context?.longToast("Changes Saved")
                Status.FAILED -> handleNetworkError(resource)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        serviceID = arguments?.getInt(ARG_SERVICE_ID) ?: 0
    }

    open fun setupViews() {}

    open fun isLoading(loading: Boolean) {}
}