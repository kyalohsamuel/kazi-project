package ke.co.kazi.ui.v2.merchant.fragment

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import ke.co.kazi.GlideApp
import ke.co.kazi.R
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.util.FileUploadUtil
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_service_image_upload.*
import timber.log.Timber
import java.io.File

class ServiceImageUploadFragment : ServiceFragment() {

    private val args by navArgs<ServiceImageUploadFragmentArgs>()
    private var imageUri: Uri? = null
        set(value) {
            setCircleImageView(value)
            field = value
        }
    private var loading: Boolean = false
        set(value) {
            progressLayout.visibility = when(loading){
                true -> View.VISIBLE
                false -> View.GONE
            }
            field = value
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)
                when (resultCode) {
                    Activity.RESULT_OK -> imageUri = result.uri
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> Timber.e(result.error)
                }
            }
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_service_image_upload, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    private fun setCircleImageView(uri: Uri?) {
        GlideApp.with(context!!)
                .load(uri)
                .into(imageView)

        when(uri != null){
            true -> uploadImage(uri)
            false -> Toast.makeText(context, R.string.error_pick_image, Toast.LENGTH_LONG).show()
        }
    }

    private fun setupViews() {
        imageView.setOnClickListener {
            CropImage.activity()
                    .setCropShape(CropImageView.CropShape.RECTANGLE)
                    .start(context!!, this) }
    }

    private fun uploadImage(uri: Uri) {
        viewModel.uploadServiceImage(args.serviceId,
                FileUploadUtil.prepareFilePart("image", File(uri.path), context!!))
                .observe(this, Observer { resource ->
                    loading = false
                    when(resource.status){
                        Status.LOADING -> loading = true
                        Status.SUCCESS -> navigate()
                        Status.FAILED -> handleNetworkError(resource)
                    }
                })
    }

    private fun navigate() {
        Toast.makeText(context, "Added Service successfully", Toast.LENGTH_LONG).show()
        activity?.finish()
    }


}
