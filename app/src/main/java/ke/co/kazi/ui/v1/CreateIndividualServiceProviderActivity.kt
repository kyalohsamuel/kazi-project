package ke.co.kazi.ui.v1

import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import ke.co.kazi.R
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Response
import ke.co.kazi.api.models.data.IdentityData
import ke.co.kazi.api.models.data.UpdateServiceProvider
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.util.CustomSharedPreferences
import kotlinx.android.synthetic.main.activity_create_individual_service_provider.*
import kotlinx.android.synthetic.main.content_create_individual_service_provider.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CreateIndividualServiceProviderActivity : AppCompatActivity() {

    lateinit var webService: Webservice

    var updateServiceProviderCall: Call<ApiResponse<Response<Unit>>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_individual_service_provider)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupViews()

        val sharedPreferences = CustomSharedPreferences(this)

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addNetworkInterceptor { chain ->
                    val token = sharedPreferences.getAccessToken()
                    val request = chain.request().newBuilder().addHeader("token", token).build()

                    chain.proceed(request)
                }
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://45.79.18.37:8080")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        webService = retrofit.create(Webservice::class.java)
    }

    override fun onStop() {
        super.onStop()
        updateServiceProviderCall?.cancel()
    }

    private fun setupViews() {
        doneButton.setOnClickListener {
            next()
        }

        docIdEditText.addTextChangedListener(ClearErrorTextWatcher(docIdInputLayout))

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1).apply {
            addAll("ID Card Number", "Passport Number")
        }
        docTypeSpinner.adapter = adapter
    }

    private fun next(){
        val documentType = when(docTypeSpinner.selectedItemPosition){
            0 -> "IDNUM"
            else -> "PASSPORTNUM"
        }
        val documentID = docIdEditText.text.toString()
        val twitterUsername = twitterEditText.text.toString()
        val facebookUsername = facebookEditText.text.toString()

        if(documentID.isBlank()){
            docIdInputLayout.isErrorEnabled = true
            docIdInputLayout.error = "This is a required field"
            return
        }

        next(documentType, documentID, twitterUsername, facebookUsername)
    }

    private fun next(documentType: String, documentID: String, twitterUsername: String, facebookUsername: String) {
        val updateData = IdentityData(documentType, documentID, twitterUsername, facebookUsername)
        val updateServiceProvider = UpdateServiceProvider(data = updateData)
//        updateServiceProviderCall = webService
//                .updateServiceProvider(Request.add(updateServiceProvider, Command.UPDATE_SERVICE_PROVIDER.value))
//
//        updateServiceProviderCall?.enqueue(object: Callback<ApiResponse<Response<Unit>>>{
//            override fun onResponse(call: Call<ApiResponse<Response<Unit>>>, response: retrofit2.Response<ApiResponse<Response<Unit>>>) {
//                startActivity(intentFor<CreateCertificationsActivity>())
//            }
//
//            override fun onFailure(call: Call<ApiResponse<Response<Unit>>>, t: Throwable) {
//                Timber.e(t, t.localizedMessage)
//                startActivity(intentFor<CreateCertificationsActivity>())
//            }
//
//        })


    }
}
