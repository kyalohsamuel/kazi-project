package ke.co.kazi.ui.v1

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Response
import ke.co.kazi.databinding.ActivityBecomeServiceProviderBinding
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_become_service_provider.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class BecomeServiceProviderActivity : AppCompatActivity() {

    companion object {
        const val TYPE_INDIVIDUAL = "INDIVIDUAL"
        const val TYPE_COMPANY = "COMPANY"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private lateinit var viewModel: MainViewModel

    private lateinit var binding: ActivityBecomeServiceProviderBinding

    private var username: String = ""

    lateinit var webService: Webservice

    var addProviderCall: Call<ApiResponse<Response<Unit>>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_become_service_provider)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""

        viewModel = ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]

        username = intent?.getStringExtra(INTENT_USER_PHONE_NUMBER) ?: ""

        binding.profileImageUrl = sharedPreferences.userProfileImage

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addNetworkInterceptor { chain ->
                    val token = sharedPreferences.getAccessToken()
                    val request = chain.request().newBuilder().addHeader("token", token).build()

                    chain.proceed(request)
                }
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://45.79.18.37:8080")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        webService = retrofit.create(Webservice::class.java)
    }

    override fun onStop() {
        super.onStop()
        addProviderCall?.cancel()
    }

    fun onProceed(view: View){
        when {
            radioYes.isChecked -> {
                val type = when(radioGroupProfile.checkedRadioButtonId){
                    radioProfileIndividual.id -> TYPE_INDIVIDUAL
                    radioProfileCompany.id -> TYPE_COMPANY
                    else -> throw IllegalStateException("Invalid group id")
                }

                createProviderType(type)
            }
            else -> finish()
        }
    }

    private fun createProviderType(type: String){
//        addProviderCall = webService.addServiceProvider(Request.add(ProviderType(type), Command.ADD_SERVICE_PROVIDER.value))
//
//        isLoading()
//        addProviderCall?.enqueue(object : Callback<ApiResponse<Response<Unit>>>{
//            override fun onResponse(call: Call<ApiResponse<Response<Unit>>>, response: retrofit2.Response<ApiResponse<Response<Unit>>>) {
//                when(type) {
//                    TYPE_INDIVIDUAL -> startActivity(intentFor<CreateIndividualServiceProviderActivity>()
//                            .putExtra(INTENT_USER_PHONE_NUMBER, username))
//                    TYPE_COMPANY -> startActivity(intentFor<CreateCompanyServiceProviderActivity>()
//                            .putExtra(INTENT_USER_PHONE_NUMBER, username))
//                }
//                isLoading(false)
//            }
//
//            override fun onFailure(call: Call<ApiResponse<Response<Unit>>>, t: Throwable) {
//                toast(getString(R.string.unknown_error))
//                isLoading(false)
//            }
//        })
    }

    private fun isLoading(loading: Boolean = true){
        when(loading){
            true -> {
                btnProceed.text = null
                progressBar.visibility = View.VISIBLE
            }
            false -> {
                btnProceed.text = "Proceed"
                progressBar.visibility = View.GONE
            }
        }
    }
}
