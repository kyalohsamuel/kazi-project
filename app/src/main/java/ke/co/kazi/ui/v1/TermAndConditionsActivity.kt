package ke.co.kazi.ui.v1

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.view.View
import ke.co.kazi.R

import kotlinx.android.synthetic.main.activity_term_and_conditions.*

class TermAndConditionsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_and_conditions)
    }

    fun onDoneReading(view: View){
        super.onBackPressed()
        finish()
    }

}
