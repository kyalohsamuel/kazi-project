package ke.co.kazi.ui.v2.adapter

import android.content.res.Resources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import ke.co.kazi.R
import ke.co.kazi.ui.v1.fragments.OtherInformationServiceFragment
import ke.co.kazi.ui.v1.fragments.ServiceContactFragment
import ke.co.kazi.ui.v1.fragments.ServiceInformationFragment
import ke.co.kazi.ui.v2.merchant.ReviewsFragment

class ServicePagerAdapter(private val res: Resources,
                          fragmentManager: FragmentManager,
                          private val serviceID: Int): FragmentPagerAdapter(fragmentManager){
    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> ServiceInformationFragment.newInstance(serviceID)
            1 -> ServiceContactFragment.newInstance(serviceID)
            2 -> OtherInformationServiceFragment.newInstance(serviceID)
            3 -> ReviewsFragment()
            else -> throw IllegalStateException("Invalid fragment position")
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> res.getString(R.string.title_general)
            1 -> res.getString(R.string.title_contact_info)
            2 -> res.getString(R.string.title_working_hrs)
            3 -> res.getString(R.string.title_reviews)
            else -> super.getPageTitle(position)
        }
    }

    override fun getCount(): Int {
        return 4
    }

}