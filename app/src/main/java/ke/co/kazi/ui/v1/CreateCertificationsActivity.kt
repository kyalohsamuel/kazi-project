package ke.co.kazi.ui.v1

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Response
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.fieldError
import ke.co.kazi.helper.hideKeyboard
import ke.co.kazi.helper.requiredField
import ke.co.kazi.model.Certification
import ke.co.kazi.ui.v1.adapter.CertificationsRecyclerViewAdapter
import ke.co.kazi.util.CustomSharedPreferences
import kotlinx.android.synthetic.main.activity_create_certifications.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.intentFor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber


class CreateCertificationsActivity : AppCompatActivity() {

    var certifications = arrayListOf<Certification>()
    lateinit var certificationsAdapter: CertificationsRecyclerViewAdapter

    var resultUri: Uri? = null

    private lateinit var webService: Webservice
    private var addCertificateProviderCall: Call<ApiResponse<Response<Unit>>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ke.co.kazi.R.layout.activity_create_certifications)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupRetrofit()
        setupViews()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                val result = CropImage.getActivityResult(data)

                when (resultCode) {
                    Activity.RESULT_OK -> {
                        resultUri = result.uri

                        pickImageEditTExt.setText(resultUri?.lastPathSegment ?: "")
                        pickImageEditTExt.clearFocus()
                    }
                    CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE ->
                        Timber.e(result.error, result.error.message)
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        addCertificateProviderCall?.cancel()
    }

    private fun setupViews() {
        backButton.setOnClickListener {
            onBackPressed()
        }

        doneButton.setOnClickListener {
            startActivity(intentFor<OtherServiceProviderActivity>())
        }

        addCertificationButton.setOnClickListener {
            addCertification()
        }

        certNameEditText.addTextChangedListener(ClearErrorTextWatcher(certNameInputLayout))
        institutionEditText.addTextChangedListener(ClearErrorTextWatcher(institutionInputLayout))
        qualificationEditText.addTextChangedListener(ClearErrorTextWatcher(qualificationInputLayout))
        pickImageEditTExt.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus) return@setOnFocusChangeListener

            hideKeyboard()

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
        }

        certificationsAdapter = CertificationsRecyclerViewAdapter(certifications)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = certificationsAdapter
    }

    private fun setupRetrofit() {
        val sharedPreferences = CustomSharedPreferences(this)

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addNetworkInterceptor { chain ->
                    val token = sharedPreferences.getAccessToken()
                    val request = chain.request().newBuilder().addHeader("token", token!!).build()

                    chain.proceed(request)
                }
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://45.79.18.37:8080")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        webService = retrofit.create(Webservice::class.java)
    }

    private fun addCertification() {
        val name = certNameEditText.text.toString()
        val institution = institutionEditText.text.toString()
        val qualification = qualificationEditText.text.toString()

        when {
            name.isBlank() -> certNameInputLayout.fieldError("Certification Name".requiredField())
            institution.isBlank() -> institutionInputLayout.fieldError("Institution".requiredField())
            qualification.isBlank() -> qualificationInputLayout.fieldError("Qualification".requiredField())
            else -> {
                val certification = Certification(name, institution, qualification, resultUri)

                certifications.add(certification)
                certificationsAdapter.notifyItemChanged(certifications.size - 1)
                addCertification(certification, certifications.size - 1)

                //Reset Views
                listOf(certNameEditText, institutionEditText, qualificationEditText, pickImageEditTExt)
                        .forEach { it.text = null }
                resultUri = null
            }
        }
    }

    private fun addCertification(certification: Certification, position: Int) {
//        val certificate = Certificate(certification.name, null, certification.institution, certification.qualification)
//        addCertificateProviderCall = webService.addCertification(Request.add(certificate, Command.ADD_CERTIFICATE.value))
//
//        addCertificateProviderCall?.enqueue(object: Callback<ApiResponse<Response<Unit>>> {
//            override fun onResponse(call: Call<ApiResponse<Response<Unit>>>, response: retrofit2.Response<ApiResponse<Response<Unit>>>) {
//                certification.syncing = false
//                certificationsAdapter.notifyItemChanged(position)
//            }
//
//            override fun onFailure(call: Call<ApiResponse<Response<Unit>>>, t: Throwable) {
//                Timber.e(t, t.localizedMessage)
//            }
//
//        })
    }


}
