package ke.co.kazi.ui.v2.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

class MockAdapter(@LayoutRes private val resource: Int): RecyclerView.Adapter<MockAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(resource, parent, false))

    override fun getItemCount() = 10

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(null)
    }

    class ViewHolder(private val view: View): RecyclerView.ViewHolder(view){
        fun bind(action: (() -> Unit)?){
            view.setOnClickListener { action }
        }
    }
}