package ke.co.kazi.ui.v2.onboarding.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import ke.co.kazi.R
import ke.co.kazi.api.models.data.UpdateCompanyData
import ke.co.kazi.api.models.data.UpdateCompanyServiceProvider
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.helper.requiredField
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_company_service.*


class CompanyServiceProviderFragment : CreateServiceProviderFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_company_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupSpinner()
        setupTextInputs()
        doneButton.setOnClickListener {
            updateDetails()
        }
    }

    override fun isLoading(loading: Boolean) {
        doneButton.text = when(loading){ true -> null false -> getText(R.string.action_next)}
        progressBar.visibility = when(loading){ true -> View.VISIBLE false -> View.GONE}
    }

    private fun navigate() {
        view?.findNavController()
                ?.navigate(R.id.action_createCompanyServiceProvider_to_createCertificationsServiceProvider)
    }

    private fun setupSpinner() {
        val adapter = ArrayAdapter<String>(context!!, R.layout.layout_simple_text_item).apply {
            addAll(getString(R.string.title_limited_company), getString(R.string.title_partnership), getString(R.string.title_sole_proprietorship))
        }
        typeSpinner.adapter = adapter
    }

    private fun setupTextInputs() {
        val views = listOf(
                Pair(kraPINEditText, kraPINInputLayout),
                Pair(companyNameEditText, companyNameInputLayout),
                Pair(officeLocationEditText, officeLocationInputLayout),
                Pair(emailEditText, emailInputLayout),
                Pair(phoneNoEditText, phoneNoInputLayout),
                Pair(postalCodeEditText, postalCodeInputLayout))

        for(view in views){
            view.first.addTextChangedListener(ClearErrorTextWatcher(view.second))
        }
    }

    private fun updateDetails() {
        val companyType = when(typeSpinner.selectedItemPosition){
            0 -> "Limited Company"
            1 -> "Partnership"
            2 -> "Sole Proprietorship"
            else -> throw IllegalStateException("Illegal company type spinner position")
        }

        val kraPin = kraPINEditText.text.toString()
        val companyName = companyNameEditText.text.toString()
        val officeLocation = officeLocationEditText.text.toString()
        val email = emailEditText.text.toString()
        val phoneNo = phoneNoEditText.text.toString()
        val postalAddress = postalCodeEditText.text.toString()

        when{
            kraPin.isEmpty() -> kraPINInputLayout.error = getString(R.string.title_kra_pin).requiredField()
            companyName.isEmpty() -> companyNameInputLayout.error = getString(R.string.title_company_name).requiredField()
            officeLocation.isEmpty() -> officeLocationInputLayout.error = getString(R.string.title_office_location).requiredField()
            email.isEmpty() -> emailInputLayout.error = getString(R.string.title_email).requiredField()
            phoneNo.isEmpty() -> phoneNoInputLayout.error = getString(R.string.title_phone_no).requiredField()
            postalAddress.isEmpty() -> postalCodeInputLayout.error = getString(R.string.title_postal_address).requiredField()
            else -> {
                val updateCompanyData = UpdateCompanyData(
                        docType = companyType,
                        kraPIN = kraPin,
                        name = companyName,
                        location = officeLocation,
                        email = email,
                        contacts = phoneNo,
                        address = postalAddress
                )

                val updateData = UpdateCompanyServiceProvider(data = updateCompanyData)
                updateDetails(updateData)
            }
        }
    }

    private fun updateDetails(updateData: UpdateCompanyServiceProvider) {
        viewModel.updateCompanyServiceProvider(updateData).observe(this, Observer { response ->
            loading = false
            when(response.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> navigate()
                Status.FAILED -> handleNetworkError(response)
            }
        })
    }

}
