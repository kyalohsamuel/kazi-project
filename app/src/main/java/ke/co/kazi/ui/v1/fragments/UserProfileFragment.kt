package ke.co.kazi.ui.v1.fragments

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.AndroidSupportInjection
import ke.co.kazi.viewmodel.UserProfileViewModel
import org.jetbrains.anko.AnkoLogger
import javax.inject.Inject
import ke.co.kazi.databinding.FragmentUserProfileBinding
import ke.co.kazi.model.User


/**
 * A simple [Fragment] subclass.
 *
 */
class UserProfileFragment : Fragment(), AnkoLogger {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val UID_KEY = "uid"

    var user: User? = null

    lateinit var viewModel: UserProfileViewModel
    lateinit var binding: FragmentUserProfileBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentUserProfileBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(UserProfileViewModel::class.java)

        viewModel.fetchUser().observe(this, Observer { result ->
            binding.user = result.data
            binding.invalidateAll()
        })
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}
