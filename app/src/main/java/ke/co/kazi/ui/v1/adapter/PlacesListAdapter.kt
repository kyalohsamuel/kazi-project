package ke.co.kazi.ui.v1.adapter

import androidx.recyclerview.widget.ListAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import ke.co.kazi.databinding.ItemPlacesBinding
import ke.co.kazi.ui.v1.PlacesSearchActivity
import ke.co.kazi.ui.v1.adapter.callback.PlacesDiffCallback
import ke.co.kazi.vo.Place

class PlacesListAdapter(val onPlaceSelectedListener: PlacesSearchActivity.OnPlaceSelectedListener) : ListAdapter<Place, PlacesListAdapter.ViewHolder>(PlacesDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPlacesBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(val binding: ItemPlacesBinding) : androidx.recyclerview.widget.RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Place) {
            binding.root.setOnClickListener { onPlaceSelectedListener.onPlaceSelected(item) }
            binding.place = item
            binding.executePendingBindings()
        }
    }
}