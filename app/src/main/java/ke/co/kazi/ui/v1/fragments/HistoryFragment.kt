package ke.co.kazi.ui.v1.fragments

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.AndroidSupportInjection
import ke.co.kazi.databinding.FragmentHistoryListBinding
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceRequest
import ke.co.kazi.ui.v1.ConfirmServiceRequestActivity
import ke.co.kazi.ui.v1.adapter.ServicesOfferedListAdapter
import ke.co.kazi.ui.v1.fragments.ServiceFragmentType.*
import ke.co.kazi.util.SERVICE_REQUEST_EXTRA
import ke.co.kazi.viewmodel.ServiceViewModel
import kotlinx.android.synthetic.main.fragment_history_list.*
import javax.inject.Inject

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [HistoryFragment.OnListFragmentInteractionListener] interface.
 */

const val ARG_SERVICE_FRAG_TYPE = "ARG_SERVICE_FRAG_TYPE"

enum class ServiceFragmentType {
    CLIENT_REQUEST, CLIENT_HISTORY, PROVIDER_REQUEST
}

class HistoryFragment : Fragment() {
    private val columnCount = 1

    private lateinit var binding: FragmentHistoryListBinding

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: ServiceViewModel
    private var listener: OnListFragmentInteractionListener? = null
    private lateinit var servicesOfferedListAdapter: ServicesOfferedListAdapter

    private var serviceFragmentType: ServiceFragmentType? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            serviceFragmentType = it.getSerializable(ARG_SERVICE_FRAG_TYPE) as ServiceFragmentType
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentHistoryListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[ServiceViewModel::class.java]

        servicesOfferedListAdapter = ServicesOfferedListAdapter(object : ServicesOfferedListAdapter.OnItemSelectedListener {
            override fun onItemSelected(item: ServiceRequest) {
//                startActivity<ConfirmServiceRequestActivity>(Pair(SERVICE_REQUEST_EXTRA, item), Pair(ARG_SERVICE_FRAG_TYPE, serviceFragmentType))

                /*if (serviceFragmentType == PROVIDER_REQUEST) {
                    OneButtonDialogFragment.newInstance(
                            loadAnimation = false,
                            titleRes = R.string.title_confirm_service,
                            messageRes = R.string.message_confirm_service,
                            buttonTextRes = R.string.action_success_confirm_service,
                            secondButtonTextRes = R.string.action_reject_confirm_service,
                            buttonDialogAction = object : OneButtonDialogFragment.ButtonDialogAction {
                                override fun onSecondActionClicked() {
                                    Timber.e("Second Action clicked")
                                }

                                override fun onButtonClicked() {
                                    Timber.e("Primary action clicked")
                                    viewModel.confirmService(ServiceRequestIdModel(item.requestId)).observe(this@HistoryFragment, Observer {
                                        Timber.e("Resource: ${it?.data}")
                                        loadByServiceType()
                                    })
                                }

                            }
                    ).show(childFragmentManager, "")
                }*/
            }
        })

        with(list) {
            val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
            layoutManager = when {
                columnCount <= 1 -> mLayoutManager
                else -> androidx.recyclerview.widget.GridLayoutManager(context, columnCount)
            }
            adapter = servicesOfferedListAdapter

            val dividerItemDecoration = androidx.recyclerview.widget.DividerItemDecoration(this@HistoryFragment.context, mLayoutManager.orientation)
            addItemDecoration(dividerItemDecoration)
        }

        loadByServiceType()

//        viewModel.loadServices().reObserve(this@HistoryFragment, Observer {
//            it?.data.apply {
//                servicesOfferedListAdapter.submitList(this)
//            }
//        })
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onResume() {
        super.onResume()
        loadByServiceType()
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson
     * [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Service)
    }

    private fun loadByServiceType() {
        when (serviceFragmentType) {

            CLIENT_REQUEST -> {
                viewModel.getMyServiceRequest("UNCONFIRMED").observe(this, Observer {
                    it?.data.apply {
                        servicesOfferedListAdapter.submitList(this)
                    }
                })
            }
            PROVIDER_REQUEST -> {
                viewModel.getClientServiceRequest().observe(this, Observer {
                    it?.data.apply {
                        servicesOfferedListAdapter.submitList(this)
                    }
                })
            }
            null -> {
            }
            CLIENT_HISTORY -> {
                viewModel.getMyServiceRequest().observe(this, Observer {
                    it?.data.apply {
                        servicesOfferedListAdapter.submitList(this)
                    }
                })
            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(serviceFragmentType: ServiceFragmentType) = HistoryFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARG_SERVICE_FRAG_TYPE, serviceFragmentType)
            }
        }
    }
}
