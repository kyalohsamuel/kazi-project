package ke.co.kazi.ui.v2.onboarding.fragment

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import ke.co.kazi.R
import ke.co.kazi.api.models.data.IdentityData
import ke.co.kazi.api.models.data.UpdateServiceProvider
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.handleNetworkError
import ke.co.kazi.helper.requiredField
import ke.co.kazi.vo.Status
import kotlinx.android.synthetic.main.fragment_individual_service.*

class IndividualServiceProviderFragment : CreateServiceProviderFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_individual_service, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupSpinner()
        documentNoEditText.addTextChangedListener(ClearErrorTextWatcher(documentNoInputLayout))
        doneButton.setOnClickListener {
            next()
        }
    }

    override fun isLoading(loading: Boolean) {
        doneButton.text = when(loading){ true -> null false -> getText(R.string.action_next)}
        progressBar.visibility = when(loading){ true -> View.VISIBLE false -> View.GONE}
    }

    private fun navigate(){
        view?.findNavController()
                ?.navigate(R.id.action_createIndividualServiceProvider_to_createCertificationsServiceProvider)
    }

    private fun next() {
        val documentType = when(typeSpinner.selectedItemPosition){
            0 -> "IDNUM"
            else -> "PASSPORTNUM"
        }

        val documentNumber = documentNoEditText.text.toString()
        val facebook = facebookEditText.text.toString()
        val twitter = twitterEditText.text.toString()

        when(documentNumber.isEmpty()){
            true -> {
                documentNoInputLayout.error = getString(R.string.label_doc_number).requiredField()
                documentNoEditText.findFocus()
            }
            false -> updateServiceProvider(documentType, documentNumber, facebook, twitter)
        }
    }

    private fun setupSpinner() {
        val adapter = ArrayAdapter<String>(context!!, R.layout.layout_simple_text_item).apply {
            addAll(getString(R.string.string_id_number), getString(R.string.string_passport_no))
        }
        typeSpinner.adapter = adapter

        typeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                documentNoEditText.inputType = when(position) {
                    0 -> InputType.TYPE_CLASS_NUMBER
                    1 -> InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
                    else -> throw IllegalStateException("Unhandled item position")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun updateServiceProvider(documentType: String, documentNumber: String, facebook: String, twitter: String) {
        val updateData = IdentityData(documentType, documentNumber, twitter, facebook)
        val updateServiceProvider = UpdateServiceProvider(data = updateData)

        viewModel.updateServiceProvider(updateServiceProvider).observe(this, Observer { response ->
            loading = false
            when(response.status){
                Status.LOADING -> loading = true
                Status.SUCCESS -> navigate()
                Status.FAILED -> handleNetworkError(response)
            }
        })
    }

}
