package ke.co.kazi.ui.v1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import com.google.android.material.textfield.TextInputLayout
import ke.co.kazi.R
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Response
import ke.co.kazi.api.models.data.*
import ke.co.kazi.helper.ClearErrorTextWatcher
import ke.co.kazi.helper.isEmail
import ke.co.kazi.util.CustomSharedPreferences
import kotlinx.android.synthetic.main.activity_create_company_service_provider.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.IllegalStateException

class CreateCompanyServiceProviderActivity : AppCompatActivity() {

    lateinit var webService: Webservice

    var updateServiceProviderCall: Call<ApiResponse<Response<UpdateProvider>>>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_company_service_provider)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupViews()

        val sharedPreferences = CustomSharedPreferences(this)

        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .addNetworkInterceptor { chain ->
                    val token = sharedPreferences.getAccessToken()
                    val request = chain.request().newBuilder().addHeader("token", token).build()

                    chain.proceed(request)
                }
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://45.79.18.37:8080")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        webService = retrofit.create(Webservice::class.java)
    }

    override fun onStop() {
        super.onStop()
        updateServiceProviderCall?.cancel()
    }

    private fun setupViews() {
        doneButton.setOnClickListener {
//            startActivity(intentFor<CreateCertificationsActivity>())
            next()
        }

        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1)
        adapter.addAll("Limited Company", "Partnership", "Sole Proprietorship")

        coTypeSpinner.adapter = adapter

        companyPINEditText.addTextChangedListener(ClearErrorTextWatcher(companyPINInputLayout))
        companyNameEditText.addTextChangedListener(ClearErrorTextWatcher(companyNameInputLayout))
        officeLocationEditText.addTextChangedListener(ClearErrorTextWatcher(officeLocationInputLayout))
        emailEditText.addTextChangedListener(ClearErrorTextWatcher(emailInputLayout))
        phoneNoEditText.addTextChangedListener(ClearErrorTextWatcher(phoneNoInputLayout))
        addressEditText.addTextChangedListener(ClearErrorTextWatcher(addressInputLayout))
    }

    private fun next(){
        val companyType = when(coTypeSpinner.selectedItemPosition){
            0 -> "Limited Company"
            1 -> "Partnership"
            2 -> "Sole Proprietorship"
            else -> throw IllegalStateException("Illegal company type spinner position")
        }
        val kraPIN = companyPINEditText.text.toString()
        val name = companyNameEditText.text.toString()
        val location = officeLocationEditText.text.toString()
        val email = emailEditText.text.toString()
        val phoneNo = phoneNoEditText.text.toString()
        val address = addressEditText.text.toString()

        when {
            kraPIN.isBlank() -> fieldError(companyPINInputLayout, requiredField("KRA PIN"))
            name.isBlank() -> fieldError(companyNameInputLayout, requiredField("Company Name"))
            location.isBlank() -> fieldError(officeLocationInputLayout, requiredField("Office Location"))
            email.isBlank() -> fieldError(emailInputLayout, requiredField("E-mail"))
            !email.isEmail() -> fieldError(emailInputLayout, "Enter a valid e-mail")
            phoneNo.isBlank() -> fieldError(phoneNoInputLayout, requiredField("Phone Number"))
            address.isBlank() -> fieldError(addressInputLayout, requiredField("Postal Address"))
        }

        val updateCompanyData = UpdateCompanyData(
                docType = companyType,
                kraPIN = kraPIN,
                name = name,
                location = location,
                email = email,
                contacts = phoneNo,
                address = address
        )

        val updateData = UpdateCompanyServiceProvider(data = updateCompanyData)
        next(updateData)
    }

    private fun requiredField(field: String): String {
        return "$field a required field"
    }

    private fun fieldError(textInputLayout: TextInputLayout, error: String){
        textInputLayout.error = error
        textInputLayout.isErrorEnabled = true
    }

    private fun next(updateProvider: UpdateCompanyServiceProvider){
//        updateServiceProviderCall = webService.updateCoServiceProvider(
//                Request.add(updateProvider, Command.UPDATE_SERVICE_PROVIDER.value))
//
//        updateServiceProviderCall?.enqueue(object : Callback<ApiResponse<Response<UpdateProvider>>> {
//            override fun onResponse(call: Call<ApiResponse<Response<UpdateProvider>>>, response: retrofit2.Response<ApiResponse<Response<UpdateProvider>>>) {
//                startActivity(intentFor<CreateCertificationsActivity>())
//            }
//
//            override fun onFailure(call: Call<ApiResponse<Response<UpdateProvider>>>, t: Throwable) {
//                startActivity(intentFor<CreateCertificationsActivity>())
//                Timber.e(t, t.localizedMessage)
//            }
//        })
    }
}
