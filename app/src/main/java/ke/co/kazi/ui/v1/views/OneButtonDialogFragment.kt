package ke.co.kazi.ui.v1.views


import android.content.Context
import android.graphics.Point
import android.os.Bundle
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.RawRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import android.view.*
import android.view.inputmethod.InputMethodManager
import ke.co.kazi.databinding.FragmentOneButtonDialogBinding
import kotlinx.android.synthetic.main.fragment_one_button_dialog.*


const val ARG_BUTTON_TEXT = "ARG_BUTTON_TEXT"
const val ARG_SECOND_BUTTON_TEXT = "ARG_SECOND_BUTTON_TEXT"
const val ARG_COLOR_RESOURCE_ID = "ARG_COLOR_RESOURCE_ID"
const val ARG_TITLE = "ARG_TITLE"
const val ARG_MESSAGE = "ARG_MESSAGE"
const val ARG_IMAGE_RESOURCE_ID = "ARG_IMAGE_RESOURCE_ID"
const val ARG_IS_ANIMATION = "ARG_IS_ANIMATION"
const val ARG_ANIMATION_RESOURCE_ID = "ARG_ANIMATION_RESOURCE_ID"

private const val DIALOG_WINDOW_WIDTH = 0.85

/**
 * A simple [Fragment] subclass.
 * Use the [OneButtonDialogFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class OneButtonDialogFragment : androidx.fragment.app.DialogFragment() {

    private var buttonDialogAction: ButtonDialogAction? = null
    private lateinit var binding: FragmentOneButtonDialogBinding

    private var titleRes: Int? = null
    private var messageRes: Int? = null
    private var buttonTextRes: Int? = null
    private var image: Int? = null
    private var color: Int? = null
    private var animationRes: Int? = null
    private var isAnimation = false
    private var secondActionButtonRes: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            titleRes = it.getInt(ARG_TITLE)
            messageRes = it.getInt(ARG_MESSAGE)
            buttonTextRes = it.getInt(ARG_BUTTON_TEXT)
            secondActionButtonRes = it.getInt(ARG_SECOND_BUTTON_TEXT)
            image = it.getInt(ARG_IMAGE_RESOURCE_ID)
            color = it.getInt(ARG_COLOR_RESOURCE_ID)
            animationRes = it.getInt(ARG_ANIMATION_RESOURCE_ID)
            isAnimation = it.getBoolean(ARG_IS_ANIMATION)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        binding = FragmentOneButtonDialogBinding.inflate(inflater, container, false)

        binding.buttonOk.setOnClickListener { onButtonClicked() }
        binding.btnSecondaryAction.setOnClickListener { onSecondButtonClicked() }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (isAnimation) {
            imageIcon.visibility = View.GONE
            animationIcon.visibility = View.VISIBLE
        } else {
            imageIcon.visibility = View.VISIBLE
            animationIcon.visibility = View.GONE
        }

        if (secondActionButtonRes == null) {
            btnSecondaryAction.visibility = View.GONE
        } else {
            if (secondActionButtonRes != 0) secondActionButtonRes?.let { btnSecondaryAction.setText(it) }
        }

        if (titleRes != 0) titleRes?.let { labelTitle.setText(it) }
        if (messageRes != 0) messageRes?.let { labelMessage.setText(it) }
        if (buttonTextRes != 0) buttonTextRes?.let { buttonOk.setText(it) }
        if (image != 0) image?.let { imageIcon.setImageResource(it) }
        if (color != 0) color?.let { labelTitle.setTextColor(it) }
        if (animationRes != 0) animationRes?.let { animationIcon.setAnimation(it) }
    }

    override fun onStart() {
        super.onStart()
        setDialogWindowWidth(DIALOG_WINDOW_WIDTH)
    }

    fun onButtonClicked() {
        closeDialog()
        buttonDialogAction?.onButtonClicked()
    }

    fun onSecondButtonClicked() {
        closeDialog()
        buttonDialogAction?.onSecondActionClicked()
    }

    fun closeDialog() {
        if (dialog?.isShowing == true) {
            closeKeyboard()
            dialog?.dismiss()
        }
    }

    private fun closeKeyboard() {
        val imm = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(
                activity!!.findViewById<View>(android.R.id.content).windowToken, 0)
    }

    private fun setDialogWindowWidth(width: Double) {
        val window = dialog?.window
        val size = Point()
        val display: Display
        if (window != null) {
            display = window.windowManager.defaultDisplay
            display.getSize(size)
            val maxWidth = size.x
            window.setLayout((maxWidth * width).toInt(), WindowManager.LayoutParams.WRAP_CONTENT)
            window.setGravity(Gravity.CENTER)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(@StringRes titleRes: Int? = null,
                        @StringRes messageRes: Int? = null,
                        @StringRes buttonTextRes: Int? = null,
                        @StringRes secondButtonTextRes: Int? = null,
                        @DrawableRes imageResId: Int? = null,
                        @ColorRes colorResId: Int? = null,
                        @RawRes animationRes: Int? = null,
                        loadAnimation: Boolean = false,
                        buttonDialogAction: ButtonDialogAction? = null
        ): OneButtonDialogFragment = OneButtonDialogFragment().apply {
            arguments = Bundle().apply {
                titleRes?.let { putInt(ARG_TITLE, it) }
                messageRes?.let { putInt(ARG_MESSAGE, it) }
                buttonTextRes?.let { putInt(ARG_BUTTON_TEXT, it) }
                secondButtonTextRes?.let { putInt(ARG_SECOND_BUTTON_TEXT, it) }
                imageResId?.let { putInt(ARG_IMAGE_RESOURCE_ID, it) }
                colorResId?.let { putInt(ARG_COLOR_RESOURCE_ID, it) }
                animationRes?.let { putInt(ARG_ANIMATION_RESOURCE_ID, it) }
                putBoolean(ARG_IS_ANIMATION, loadAnimation)
            }
            this.buttonDialogAction = buttonDialogAction
        }

    }

    interface ButtonDialogAction {
        fun onButtonClicked()
        fun onSecondActionClicked()
    }
}
