package ke.co.kazi.util

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.preference.PreferenceManager
import android.provider.MediaStore
import androidx.core.content.FileProvider
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.models.sort.SortingTypes
import droidninja.filepicker.utils.Orientation
import ke.co.kazi.R
import timber.log.Timber
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Singleton

@Singleton
class FileChooser(private val activity: Activity) {

    private val photoPaths = arrayListOf<String>()
    private val docPaths = arrayListOf<String>()

    private val preferenceManager = CustomSharedPreferences(activity.application)

    private fun createIntent() = when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT -> Intent(Intent.ACTION_OPEN_DOCUMENT)
        else -> Intent(Intent.ACTION_GET_CONTENT).apply { Intent.createChooser(this, "Select File") }
    }

    fun openFile() {
        FilePickerBuilder.instance.setMaxCount(1)
                .setSelectedFiles(docPaths)
                .setActivityTheme(R.style.LibAppTheme)
                .setMaxCount(1)
                .showFolderView(true)
                .sortDocumentsBy(SortingTypes.name)
                .withOrientation(Orientation.UNSPECIFIED)
                .pickFile(activity)
    }

    fun openPhoto() {
        FilePickerBuilder.instance.setMaxCount(1)
                .setSelectedFiles(photoPaths)
                .setActivityTheme(R.style.LibAppTheme)
                .setMaxCount(1)
                .setSelectedFiles(photoPaths)
                .setActivityTitle("Please select photo")
                .enableVideoPicker(false)
                .enableCameraSupport(true)
                .showGifs(false)
                .showFolderView(true)
                .enableSelectAll(false)
                .enableImagePicker(true)
                .withOrientation(Orientation.UNSPECIFIED)
                .pickPhoto(activity)
    }

    fun capturePicture() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            // Ensure that there's a camera activity to handle the intent
            takePictureIntent.resolveActivity(activity.packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    Timber.e(ex)
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                            activity,
                            "com.payster.fileprovider",
                            it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
                "JPEG_${timeStamp}_", /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            preferenceManager.photoPath = absolutePath
        }
    }
}