package ke.co.kazi.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.SystemClock
import androidx.annotation.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import android.text.Editable
import android.text.Html
import android.text.Spanned
import android.view.View
import ke.co.kazi.R
import java.util.concurrent.atomic.AtomicInteger
import android.view.ViewGroup
import android.widget.GridView




fun View.clickWithDebounce(debounceTime: Long = 600L, action: () -> Unit) {
    this.setOnClickListener(object : View.OnClickListener {
        private var lastClickTime: Long = 0

        override fun onClick(v: View) {
            when {
                SystemClock.elapsedRealtime() - lastClickTime < debounceTime -> return
                else -> action.invoke()
            }

            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}

/*
@SuppressLint("RestrictedApi")
fun BottomNavigationView.disableShiftMode() {
    val menuView = this.getChildAt(0) as BottomNavigationMenuView
    try {
        val shiftingMode = menuView::class.java.getDeclaredField("mShiftingMode")
        shiftingMode.isAccessible = true
        shiftingMode.setBoolean(menuView, false)
        shiftingMode.isAccessible = false
        for (i in 0..(menuView.childCount - 1)) {
            val item = menuView.getChildAt(i) as BottomNavigationItemView
            item.setShiftingMode(false)
            // set once again checked value, so view will be updated
            item.setChecked(item.itemData.isChecked)
        }
    } catch (e: NoSuchFieldException) {
        Timber.e("Unable to get shift mode field")
    } catch (e: IllegalAccessException) {
        Timber.e("Unable to change value of shift mode")
    }
}
*/

fun <T> LiveData<T>.reObserve(owner: LifecycleOwner, observer: Observer<T>) {
    removeObserver(observer)
    observe(owner, observer)
}

fun generateAllViewId(): Int {
    return when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1 -> View.generateViewId()
        else -> generateViewId()
    }
}

private val sNextGeneratedId = AtomicInteger(1)

private fun generateViewId(): Int {
    while (true) {
        val result = sNextGeneratedId.get()
        // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
        var newValue = result + 1
        if (newValue > 0x00FFFFFF) newValue = 1 // Roll over to 1, not 0.
        if (sNextGeneratedId.compareAndSet(result, newValue)) return result
    }
}

/**
 * Return a drawable object associated with a particular resource ID. It factors all sdk versions.
 */
fun getDrawableRes(@DrawableRes id: Int, context: Context): Drawable {
    return when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP -> context.resources.getDrawable(id, null)
        else -> context.resources.getDrawable(id)
    }
}

/**
 * Return a color object associated with a particular resource ID. It factors all sdk versions.
 */
fun getColor(@ColorRes id: Int, context: Context): Int {
    return when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> context.resources.getColor(id, null)
        else -> context.resources.getColor(id)
    }
}

/**
 * Specify a color to be the color filter of the drawable.
 */
fun Drawable.changeColor(@ColorInt id: Int): Drawable {
    this.setColorFilter(id, PorterDuff.Mode.SRC_ATOP)
    return this
}

//fun ActionBar.setHomeAsUpCustomIndicator(context: Context) {
//    this.setHomeAsUpIndicator(ke.co.kazi.util.getDrawable(R.drawable.home_as_up_indicator, context)
//            .changeColor(ke.co.kazi.util.getColor(R.color.white, context)))
//}

/**
 * show a fragment with the container layout being R.id.container
 */
fun androidx.fragment.app.Fragment.show(supportFragmentManager: androidx.fragment.app.FragmentManager): androidx.fragment.app.Fragment {
    return show(supportFragmentManager, R.id.container)
}


/**
 * Replace a fragment with the container layout being R.id.container
 */
fun androidx.fragment.app.Fragment.replace(supportFragmentManager: androidx.fragment.app.FragmentManager): androidx.fragment.app.Fragment {
    return replace(supportFragmentManager, R.id.container)
}

fun androidx.fragment.app.Fragment.show(supportFragmentManager: androidx.fragment.app.FragmentManager, @IdRes containerId: Int): androidx.fragment.app.Fragment {
    supportFragmentManager.beginTransaction().add(containerId, this).commit()
    return this
}

fun androidx.fragment.app.Fragment.replace(supportFragmentManager: androidx.fragment.app.FragmentManager, @IdRes containerId: Int): androidx.fragment.app.Fragment {
    supportFragmentManager.beginTransaction().replace(containerId, this).addToBackStack(null).commit()
    return this
}

fun getFormattedStringRes(context: Context, @StringRes stringId: Int, vararg args: Any): Spanned? {
    val text = context.getString(stringId, *args)
    return when {
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.N -> Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        else -> Html.fromHtml(text)
    }
}

fun String.toEditable(): Editable {
    return Editable.Factory().newEditable(this)
}

fun setGridViewHeightBasedOnChildren(gridView: GridView, columns: Int) {
    val listAdapter = gridView.adapter
            ?: // pre-condition
            return

    var totalHeight = 0
    val items = listAdapter.count
    var rows = 0

    val listItem = listAdapter.getView(0, null, gridView)
    listItem.measure(0, 0)
    totalHeight = listItem.measuredHeight

    var x = 1f
    if (items > columns) {
        x = (items / columns).toFloat()
        rows = (x + 1).toInt()
        totalHeight *= rows
    }

    val params = gridView.layoutParams
    params.height = totalHeight
    gridView.layoutParams = params

}
