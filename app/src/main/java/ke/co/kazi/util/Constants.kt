package ke.co.kazi.util

const val SERVICE_EXTRA = "SERVICE_EXTRA"
const val SERVICE_REQUEST_EXTRA = "SERVICE_REQUEST_EXTRA"
const val SERVICE_LOCATION = "SERVICE_LOCATION"
const val MESSAGE_TYPE = "MESSAGE_TYPE"

const val BASE_URL = "http://45.79.18.37:8080"

const val RC_CAMERA_AND_LOCATION = 2001
const val RC_CAMERA_AND_STORAGE = 2002

const val IMAGE = "image"
const val TEXT = "text"
const val IMAGE_CHOOSER_INTENT = 10001
const val PDF_CHOOSER_INTENT = 10002
const val REQUEST_IMAGE_CAPTURE = 10003
const val REQUEST_INVENTORY_ITEM = 10004