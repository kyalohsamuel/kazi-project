package ke.co.kazi.util

object ChatTopics {
    const val REGISTER = "register"
    const val REGISTRATION_SUCCESS = "registration_success"
    const val REGISTRATION_ERROR = "registration_error"
    const val CHAT_LIST = "chat_list"
    const val PRIVATE_MESSAGE = "private_message"
    const val REQUEST_CHATS = "request_chats"
}