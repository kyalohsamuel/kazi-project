package ke.co.kazi.util

import android.text.format.DateUtils
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

object DateUtils {

    @JvmStatic
    fun toSimpleDateString(date: String): String {
        val format = SimpleDateFormat( "yyyy-MM-dd",  Locale.ENGLISH)
        val date = format.parse(date)

        return format.format(date)
    }

    @JvmStatic
    fun fromMillisToTimeString(millis: Long) : String {
        val format = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return format.format(millis)
    }
}