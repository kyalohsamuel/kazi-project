package ke.co.kazi.util

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Singleton

const val KEY_PREFERENCES_NAME = "com.example.myapp.PREFERENCE_FILE_KEY"

private const val PREF_ACCESS_TOKEN = "pref_access_token"

@Singleton
class CustomSharedPreferences(context: Context) {

    private var sharedPreferences: SharedPreferences = context.getSharedPreferences(KEY_PREFERENCES_NAME, Context.MODE_PRIVATE)

    var userProfileImage: String?
        get() = getString(PREF_KEY_USER_PROFILE_IMAGE)
        set(value) = sharedPreferences.edit().putString(PREF_KEY_USER_PROFILE_IMAGE, value).apply()

    var notificationSms: Boolean
        get() = getBoolean(PREF_KEY_NOTIFICATION_SMS)
        set(value) = sharedPreferences.edit().putBoolean(PREF_KEY_NOTIFICATION_SMS, value).apply()

    var notificationEmail: Boolean
        get() = getBoolean(PREF_KEY_NOTIFICATION_EMAIL)
        set(value) = sharedPreferences.edit().putBoolean(PREF_KEY_NOTIFICATION_EMAIL, value).apply()

    var notificationApp: Boolean
        get() = getBoolean(PREF_KEY_NOTIFICATION_APP)
        set(value) = sharedPreferences.edit().putBoolean(PREF_KEY_NOTIFICATION_APP, value).apply()

    var userId: String?
        get() = getString(PREF_KEY_USER_ID)
        set(value) = sharedPreferences.edit().putString(PREF_KEY_USER_ID, value).apply()

    var fcmToken: String?
        get() = getString(PREF_KEY_FCM_TOKEN)
        set(value) {
            saveString(PREF_KEY_FCM_TOKEN, value)
            isFcmTokenChanged = true
        }

    var isFcmTokenChanged: Boolean
        get() = getBoolean(PREF_KEY_IS_FCM_TOKEN_CHANGED)
        set(value) = saveBoolean(PREF_KEY_IS_FCM_TOKEN_CHANGED, value)

    var filePicked: String?
        get() = getString(PREF_KEY_FILE_PICKED)
        set(value) = sharedPreferences.edit().putString(PREF_KEY_FILE_PICKED, value).apply()

    var photoPath: String?
        get() = getString(PREF_KEY_PHOTO_PATH)
        set(value) = sharedPreferences.edit().putString(PREF_KEY_PHOTO_PATH, value).apply()

    fun getString(key: String) = getString(key, "")

    fun getString(key: String, default: String): String? = sharedPreferences.getString(key, default)

    private fun saveString(key: String, value: String?): Unit = with(sharedPreferences.edit()) {
        putString(key, value)
        commit()
    }

    private fun getBoolean(key: String) = getBoolean(key, false)

    fun getBoolean(key: String, default: Boolean) = sharedPreferences.getBoolean(key, default)

    fun saveBoolean(key: String, value: Boolean): Unit = with(sharedPreferences.edit()) {
        putBoolean(key, value)
        commit()
    }

    fun getAccessToken(): String? = getString(PREF_ACCESS_TOKEN)

    fun setAccessToken(token: String) = saveString(PREF_ACCESS_TOKEN, token)

    companion object {
        const val PREF_KEY_USER_PROFILE_IMAGE = "PREF_KEY_USER_PROFILE_IMAGE"
        const val PREF_KEY_NOTIFICATION_SMS = "PREF_KEY_NOTIFICATION_SMS"
        const val PREF_KEY_NOTIFICATION_EMAIL = "PREF_KEY_NOTIFICATION_EMAIL"
        const val PREF_KEY_NOTIFICATION_APP = "PREF_KEY_NOTIFICATION_APP"
        const val PREF_KEY_USER_ID = "PREF_KEY_USER_ID"
        const val PREF_KEY_FCM_TOKEN = "PREF_KEY_FCM_TOKEN"
        const val PREF_KEY_IS_FCM_TOKEN_CHANGED = "PREF_KEY_IS_FCM_TOKEN_CHANGED"
        const val PREF_KEY_FILE_PICKED = "PREF_KEY_FILE_PICKED"
        const val PREF_KEY_PHOTO_PATH = "PREF_KEY_PHOTO_PATH"
    }

}