package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName


data class ProviderType(
        @SerializedName("profile_type") val profileType: String = "",
        @SerializedName("account_type") val accountType: String = "PREMIUM"
){
    companion object {
        const val TYPE_INDIVIDUAL = "INDIVIDUAL"
        const val TYPE_COMPANY = "COMPANY"
    }
}