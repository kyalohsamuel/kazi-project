package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName
import java.util.*

data class ServiceRequestData(
        @SerializedName("service_sub_category_code") var serviceCategoryCode: String = "",
        var location: Array<Double> = arrayOf(0.0, 0.0)
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ServiceRequestData

        if (serviceCategoryCode != other.serviceCategoryCode) return false
        if (!Arrays.equals(location, other.location)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = serviceCategoryCode.hashCode()
        result = 31 * result + Arrays.hashCode(location)
        return result
    }

}