package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName

data class ServiceProviderDetail(
        @SerializedName("personal_details") var personalDetails: PersonalDetails? = null,
        @SerializedName("professional_details") var professionalDetails: ProfessionalDetails? = null,
        @SerializedName("certification_details") var certificationDetails: CertificationDetails? = null
)

data class PersonalDetails(
        @SerializedName("identification_num") var identificationNumber: String = "",
        @SerializedName("identification_type") var identificationType: String = "",
        var description: String = ""
)

data class ProfessionalDetails(
        @SerializedName("professional_name") var professionalName: String = "",
        @SerializedName("yearsofservice") var yearsOfService: Float = 0F,
        var details: String = ""
)

class CertificationDetails(
        @SerializedName("certificate_name") var certificateName: String = "",
        @SerializedName("certifying_institution") var institution: String = "",
        @SerializedName("details") var details: String = "",
        @SerializedName("certificate_number") var certificateNumber: String = ""
)