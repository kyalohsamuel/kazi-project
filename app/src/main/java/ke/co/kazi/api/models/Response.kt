package ke.co.kazi.api.models

import com.google.gson.annotations.SerializedName
import ke.co.kazi.api.models.data.Message

data class Response<T>(
        @SerializedName("response_code") var responseCode: String = "",
        var module: String = "",
        @SerializedName("status_code") var statusCode: String = "",
        @SerializedName("dev_message") var developerMessage: Boolean = false,
        var data: T? = null,
        var message: Message = Message()
)
