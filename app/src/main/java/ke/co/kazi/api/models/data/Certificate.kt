package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName

data class Certificate(
        @SerializedName("certificate_name") val name: String,
        @SerializedName("certificate_number") val number: String?,
        @SerializedName("certifying_institution") val institution: String,
        val details: String
)