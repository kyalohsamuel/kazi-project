package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName
import java.util.*

data class ServiceIdModel(@SerializedName("service_id") var serviceId: Int, var location: Array<Double> = arrayOf(0.0, 0.0)) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ServiceIdModel

        if (serviceId != other.serviceId) return false
        if (!Arrays.equals(location, other.location)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = serviceId
        result = 31 * result + Arrays.hashCode(location)
        return result
    }
}

data class ServiceRequestIdModel(@SerializedName("service_request_id") var requestId: Int)