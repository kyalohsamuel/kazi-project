package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName

data class ForgotPasswordData(
        @SerializedName("contact") var mobileNumber: String = "",
        var password: String = ""
)