package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName
import ke.co.kazi.vo.AccountType

data class ProfileSettingsData(
        @SerializedName("account_type") var accountType: AccountType = AccountType.STANDARD,
        @SerializedName("notification_settings") var notificationSettings: NotificationSettings = NotificationSettings()
        )

data class NotificationSettings(
        @SerializedName("Email") var email: Boolean = false,
        @SerializedName("Sms") var sms: Boolean = false,
        @SerializedName("Inapp") var inApp: Boolean = false
)