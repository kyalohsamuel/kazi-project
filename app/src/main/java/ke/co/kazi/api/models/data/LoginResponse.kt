package ke.co.kazi.api.models.data

import ke.co.kazi.model.User


/**
 *  {
 *      "status_code": 0,
 *      "requestid": 1234567,
 *      "data": {
 *          "code": 10012,
 *          "data": {
 *              "current_loc": {
 *                  "coordinates": []
 *              },
 *              "user_type": "CLIENT",
 *             "registered_by": "SELF",
 *              "status": "ACTIVE",
 *              "login_attempts": 0,
 *              "first_time_login": false,
 *              "user_category": "STANDARD",
 *              "_id": "5b8609d05d3335424fc45f12",
 *              "username": "0713632002",
 *              "password": "AAAAEAABhqDHc4idg25Quq3soM5krCuRdFxmavYg9zQ+a0iiFUWxUtgInFjUuw90k9gfQ6U3jRc=",
 *              "profile_image_path": "/images/profile/avatar.png",
 *              "names": "Samuel Kyalo",
 *              "email": "gatihgai@gmail.com",
 *              "contact": "0713632002",
 *              "country_code": "254",
 *             "tnc": true,
 *              "registration_date": "2018-08-29T02:49:52.143Z",
 *              "user_id": 1,
 *              "__v": 0,
 *              "last_login": "2018-08-29T19:08:10.109Z"
 *          },
 *          "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJLYXppIiwiZXhwIjoxNTM2MTc0NTgyODgxLCJkYXRhIjp7InVzZXJfaWQiOjEsInVzZXJfdHlwZSI6IkNMSUVOVCIsImNvbnRhY3QiOiIwNzE2MDEwNjE3In19.H3HIlLlYGNDlG6pFNQ64Eop5FSbeCc2dSx3dwVTS4Kk"
 *      }
 *  }
 */
data class LoginResponse(
        var code: String = "",
        var data: User = User(),
        var token: String = ""
)