package ke.co.kazi.api.models.data

data class LoginData(
        var password: String = "",
        var username: String = "",
        @Transient var profileImageUrl: String = ""
)