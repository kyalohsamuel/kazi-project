package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName

data class FcmDeviceRegistration(
        @SerializedName("device_token")
        var deviceToken: String? = "",
        @SerializedName("device_type")
        var deviceType: String = "ANDROID",
        @SerializedName("device_uuid")
        var deviceUuid: String = ""
)