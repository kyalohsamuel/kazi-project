package ke.co.kazi.api.models.response

import com.google.gson.annotations.SerializedName


data class MobileCheckResponse(
        var code: String = "",
        @SerializedName("profile_image_path") var profileImageUrl: String = "")