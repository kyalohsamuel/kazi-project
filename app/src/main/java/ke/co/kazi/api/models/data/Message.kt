package ke.co.kazi.api.models.data


data class Message(val tile: String = "", val body: String = "")
