package ke.co.kazi.api.models

import com.google.gson.annotations.SerializedName
import ke.co.kazi.vo.Action


class Request<T>(var data: T? = null, var command: String = "", var action: String = "", @SerializedName("meta_data") var metaData: MetaData = MetaData()) {

    companion object {
        fun <T> post(data: T?, command: String): Request<T> = Request(data = data, command = command, action = Action.POST.toString().toLowerCase())
        fun <T> get(data: T?, command: String): Request<T> = Request(data = data, command = command, action = Action.GET.toString().toLowerCase())
        fun <T> put(data: T?, command: String): Request<T> = Request(data = data, command = command, action = Action.PUT.toString().toLowerCase())
        fun <T> add(data: T?, command: String): Request<T> = Request(data = data, command = command, action = Action.ADD.toString().toLowerCase())
        fun <T> list(data: T?, command: String): Request<T> = Request(data = data, command = command, action = Action.LIST.toString().toLowerCase())
    }

    fun sort(key: String, order: SortingOrder): Request<T> {
        metaData.sort[key] = order
        return this
    }

    fun filter(key: String, value: String): Request<T> {
        metaData.filter[key] = value
        return this
    }

    fun filter(key: String, values: Array<String>): Request<T> {
        for (value in values) {
            metaData.filter[key] = value
        }
        return this
    }
}


class MetaData(var sort: MutableMap<String, SortingOrder> = mutableMapOf(),
               var filter: MutableMap<String, String> = mutableMapOf())


enum class SortingOrder {
    ASC, DESC
}