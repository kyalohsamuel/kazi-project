package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName
import ke.co.kazi.vo.DeviceType

data class MobileCheckRequest(
        @SerializedName("contact") var mobileNumber: String = "",
        @SerializedName("country_code") var countryCode: String = "",
        var pin: String? = null,
        var deviceId: String? = null,
        var deviceType: DeviceType? = null)