package ke.co.kazi.api.models.data

data class ValidateOtpData(
        val code: String,
        val contact: String
)