package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName

open class UpdateData

data class IdentityData(
        @SerializedName("identification_type") var docType: String = "",
        @SerializedName("identification_num") var docNum: String = "",
        var twitter: String = "",
        var facebook: String = ""
): UpdateData()

data class OtherData(
    var description: String = ""
): UpdateData()

data class UpdateServiceProvider(
        @SerializedName("update_field") var field: String = "IDENTIFICATION",
        @SerializedName("update_data") var data: UpdateData
)

data class UpdateCompanyData(
        @SerializedName("company_type") var docType: String = "",
        @SerializedName("kra_pin") var kraPIN: String = "",
        @SerializedName("company_name") var name: String = "",
        @SerializedName("office_location") var location: String = "",
        var email: String = "",
        var contacts: String = "",
        var address: String = "",
        @SerializedName("web_site_link") var website: String = ""
)

data class UpdateCompanyServiceProvider(
        @SerializedName("update_field") var field: String = "IDENTIFICATION",
        @SerializedName("update_data") var data: UpdateCompanyData
)

data class UpdateProvider(
        var status: String = "",
        @SerializedName("provider_status") var providerStatus: String = ""
)