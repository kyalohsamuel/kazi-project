package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName

/**
 *
 *
 * {
 * "action":"add",
 * "command":"register_user",
 * "data":{
 *      "password":"pass123!" ,
 *      "contact":"0725596227" ,
 *      "names":"robert njuguma",
 *      "email" :"kk@ymail.com" ,
 *      "country_code":"+254" ,
 *      "tnc":true
 *  }
 * }
 */
data class RegisterRequest(
        var password: String = "",
        var contact: String = "",
        @SerializedName("names") var name: String = "",
        var email: String = "",
        @SerializedName("country_code") var countryCode: String = "",
        var tnc: Boolean = false
)