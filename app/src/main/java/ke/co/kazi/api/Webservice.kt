package ke.co.kazi.api

import androidx.lifecycle.LiveData
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.Response
import ke.co.kazi.api.models.data.*
import ke.co.kazi.api.models.response.MobileCheckResponse
import ke.co.kazi.model.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Webservice {
    @GET("users/{userId}")
    fun getUserById(@Path("userId") userId: String): LiveData<ApiResponse<Response<User>>>

    @POST("mobile/v1")
    fun checkPhoneStatus(@Body requestData: Request<MobileCheckRequest>?): LiveData<ApiResponse<Response<MobileCheckResponse>>>

    @POST("mobile/v1")
    fun loginCall(@Body body: Request<LoginData>?): LiveData<ApiResponse<Response<LoginResponse>>>

    @POST("mobile/v1")
    fun register(@Body body: Request<RegisterRequest>?): LiveData<ApiResponse<Response<User>>>

    @POST("mobile/v1")
    fun requestOtp(@Body body: Request<MobileCheckRequest>?): LiveData<ApiResponse<Response<User>>>

    @POST("mobile/v1")
    fun validateOtp(@Body body: Request<ValidateOtpData>?): LiveData<ApiResponse<Response<User>>>

    @POST("mobile/v1")
    fun getServices(@Body request: Request<Nothing>): LiveData<ApiResponse<Response<List<Service>>>>

    @POST("mobile/v1")
    fun getServicesByCategory(@Body request: Request<ServiceByCategoryData>): LiveData<ApiResponse<Response<List<Service>>>>

    @POST("mobile/v1")
    fun getServicesByCategoryAndLocation(@Body request: Request<ServiceRequestData>): LiveData<ApiResponse<Response<List<Service>>>>

    @POST("mobile/v1")
    fun addServices(@Body request: Request<Service>): LiveData<ApiResponse<Response<Service>>>

    @POST("mobile/v1")
    fun getMyServices(@Body request: Request<Nothing>): LiveData<ApiResponse<Response<List<ServiceCategory>>>>

    @POST("mobile/v1")
    fun getServiceSubCategories(@Body request: Request<Nothing>): LiveData<ApiResponse<Response<List<ServiceSubCategory>>>>

    @POST("mobile/v1")
    fun getInitSettings(@Body request: Request<Nothing>): LiveData<ApiResponse<Response<Setting>>>

    @POST("mobile/v1")
    fun sendServiceProviderDetails(@Body request: Request<ServiceProviderDetail>): LiveData<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun confirmServiceRequest(@Body request: Request<ServiceRequestIdModel>): LiveData<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun requestService(@Body request: Request<ServiceIdModel>): LiveData<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun getServiceRequests(@Body request: Request<Nothing>): LiveData<ApiResponse<Response<List<ServiceRequest>>>>

    @POST("mobile/v1")
    fun sentFcmRegistrationToServer(@Body request: Request<FcmDeviceRegistration>): LiveData<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun resetPassword(@Body request: Request<ForgotPasswordData>): LiveData<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun updateProfileSettings(@Body request: Request<ProfileSettingsData>): LiveData<ApiResponse<Response<Unit>>>

    @Multipart
    @POST("upload/profile")
    fun uploadProfilePic(@Part photo: MultipartBody.Part): LiveData<ApiResponse<Response<Unit>>>

//    @POST("mobile/v1")
//    fun updateCoServiceProvider(@Body request: Request<UpdateCompanyServiceProvider>): Call<ApiResponse<Response<UpdateProvider>>>

//    @POST("mobile/v1")
//    fun addServiceProvider(@Body request: Request<ProviderType>): Call<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun addCertification(@Body request: Request<Certificate>): LiveData<ApiResponse<Response<Unit>>>

//    @POST("mobile/v1")
//    fun <T> addService(@Body request: Request<T>): Call<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun addService(@Body request: Request<DataService>): Call<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun addServiceProvider(@Body request: Request<ProviderType>): LiveData<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun updateServiceProvider(@Body request: Request<UpdateServiceProvider>): LiveData<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun updateCoServiceProvider(@Body request: Request<UpdateCompanyServiceProvider>): LiveData<ApiResponse<Response<Unit>>>

    @Multipart
    @POST("upload/service")
    fun uploadServiceImage(@Part serviceId: MultipartBody.Part, @Part photo: MultipartBody.Part): LiveData<ApiResponse<Response<Unit>>>

    @POST("mobile/v1")
    fun updateService(@Body request: Request<Service>): LiveData<ApiResponse<Response<Unit>>>
}