package ke.co.kazi.api.models.data

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
//import kotlinx.android.parcel.Parcelize

//"service_sub_category_code":"ADVOCATE" ,
//"business_service_name":"DANIEL AND SONS ADVOCATES" ,
//"service_description":"Our legal service" ,
//"service_contacts" :" 0725596227" ,
//"service_address":" P.O.BOX 56422 NAIROBI ",
//"service_social":{
//    "web":"www.dani.co.ke" ,
//    "facebook":"facebook",
//    "twitter":"#daniandsons"
//},
//"lat":-1.2916937,
//"lng":36.7981071 ,
//"service_operations":[ {
//    "Day":"MONDAY" ,
//    "from":"8:00 AM",
//    "to":"7:00 PM"
//},
//{
//    "Day":"TEUSDAY" ,
//    "from":"8:00 AM",
//    "to":"7:00 PM"
//}
//],
//"proficiency" :["JUDGES","UNDERWRITTING","CRIMINAL_CASES"]

//@Parcelize
//data class SocialService(
//        var web: String? = null,
//        var facebook: String? = null,
//        var twitter: String? = null): Parcelable
//
//@Parcelize
//data class OperationTime(
//        @SerializedName("Day") var day: String,
//        var isOpen: Boolean = true,
//        var from: String? = null,
//        var to: String? = null): Parcelable
//
//@Parcelize
//data class NewService(
//        @SerializedName("service_sub_category_code") var categoryCode: String,
//        @SerializedName("business_service_name") var name: String,
//        @SerializedName("service_description") var description: String,
//        @SerializedName("service_contacts") var mobileNo: String? = null,
//        @SerializedName("service_address") var address: String? = null,
//        @SerializedName("service_social") var social: SocialService? = null,
//        val lat: Double? = null,
//        var lng: Double? = null,
//        @SerializedName("service_operations") var operationTimes: ArrayList<OperationTime>? = null,
//        @SerializedName("proficiency") var proficiencies: ArrayList<String>? = null):Parcelable
