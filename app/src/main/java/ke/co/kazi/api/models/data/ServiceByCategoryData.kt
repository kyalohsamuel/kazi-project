package ke.co.kazi.api.models.data

import com.google.gson.annotations.SerializedName

data class ServiceByCategoryData(@SerializedName("category_code") var categoryCode: String = "")