package ke.co.kazi.notification

import android.content.SharedPreferences
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import ke.co.kazi.util.BASE_URL
import ke.co.kazi.util.ChatTopics
import ke.co.kazi.util.CustomSharedPreferences
import timber.log.Timber
import java.util.*

object KaziSocket {

    val socket by lazy {
        try {
            IO.socket(BASE_URL)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun init(sharedPreferences: CustomSharedPreferences){
        Timber.e("Socket init")
        socket?.connect()

        socket?.on(Socket.EVENT_CONNECT){
            registration(sharedPreferences)
        }

        socket?.on(ChatTopics.REGISTRATION_SUCCESS) {
            Timber.e("Registration Success: ${Arrays.toString(it)}")
        }

        socket?.on(ChatTopics.REGISTRATION_ERROR) {
            Timber.e("Registration Error: ${Arrays.toString(it)}")
        }
    }

    fun disconnect(){
        Timber.e("Socket disconnect")
        socket?.disconnect()
    }

    fun reconnect(sharedPreferences: CustomSharedPreferences){
        when(socket){
            null -> {
                init(sharedPreferences)
            }
            else -> {
                if (socket!!.connected().not()){
                    socket?.connect()
                }
            }
        }
    }

    private fun registration(sharedPreferences: CustomSharedPreferences) {
        KaziSocket.socket?.emit(ChatTopics.REGISTER, sharedPreferences.userId)
    }
}