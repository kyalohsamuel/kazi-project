package ke.co.kazi.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import dagger.android.AndroidInjection
import ke.co.kazi.R
import ke.co.kazi.model.Message
import ke.co.kazi.repository.LoginRepository
import ke.co.kazi.ui.v1.CheckMobileActivity
import ke.co.kazi.util.CustomSharedPreferences
import timber.log.Timber
import javax.inject.Inject
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject


class MyFirebaseMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var loginRepository: LoginRepository

    @Inject
    lateinit var httpLoggingInterceptor: HttpLoggingInterceptor

    @Inject
    lateinit var sharedPreferences: CustomSharedPreferences

    private val gson = Gson()

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onNewToken(token: String?) {
        Timber.e("Refreshed token: $token")

        sharedPreferences.fcmToken = token
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Timber.e("From: ${remoteMessage?.from}")
        remoteMessage?.data?.isNotEmpty()?.let {

            Timber.e("Message data payload: %s", remoteMessage.data)

            val params = remoteMessage.data
            val json = JSONObject(params)
            val message = gson.fromJson(json.toString(), Message::class.java)

            Timber.e("Message: ${message?.message}")

            sendNotification(message?.message?:"")

//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
//                scheduleJob()
//            } else {
//                // Handle remoteMessage within 10 seconds
//                handleNow()
//            }
        }

        // Check if remoteMessage contains a notification payload.
        remoteMessage?.notification?.let {
            Timber.e("Message Notification Body: ${it.body}")
        }


    }

    private fun sendNotification(messageBody: String) {
        val intent = Intent(this, CheckMobileActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(getString(R.string.fcm_new_chat))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT)
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

}
