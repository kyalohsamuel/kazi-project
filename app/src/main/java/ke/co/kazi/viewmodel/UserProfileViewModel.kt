package ke.co.kazi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.data.ServiceProviderDetail
import ke.co.kazi.model.User
import ke.co.kazi.repository.UserRepository
import ke.co.kazi.util.AbsentLiveData
import ke.co.kazi.vo.Command
import ke.co.kazi.vo.Resource
import javax.inject.Inject


class UserProfileViewModel @Inject constructor(var userRepository: UserRepository) : ViewModel() {
    private var _user = MutableLiveData<String>()

    val user: LiveData<Resource<User>> = Transformations
            .switchMap(_user) { userId ->
                if (userId == null) {
                    AbsentLiveData.create()
                } else {
                    userRepository.loadUser(userId)
                }
            }

    fun fetchUser() = userRepository.fetchAnyOne()

    fun sendServiceProviderDetails(serviceProviderDetail: ServiceProviderDetail) =
            userRepository.sendServiceProviderDetails(Request.add(serviceProviderDetail, Command.ADD_SERVICE_PROVIDER.value))
}