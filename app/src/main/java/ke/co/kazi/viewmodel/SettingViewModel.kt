package ke.co.kazi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ke.co.kazi.api.models.Request
import ke.co.kazi.model.Setting
import ke.co.kazi.repository.SettingRepository
import ke.co.kazi.vo.Command
import ke.co.kazi.vo.Resource
import javax.inject.Inject

class SettingViewModel @Inject constructor(var settingRepository: SettingRepository) : ViewModel() {

    fun loadSettings(): LiveData<Resource<Setting>> = settingRepository.loadSetting(Request.get(null, Command.GET_SETTINGS.value))
}