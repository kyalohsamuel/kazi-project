package ke.co.kazi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ke.co.kazi.api.models.Request
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.repository.ServiceCategoryRepository
import ke.co.kazi.vo.Command
import ke.co.kazi.vo.Resource
import javax.inject.Inject

class ServiceCategoryViewModel @Inject constructor(val serviceCategoryRepository: ServiceCategoryRepository) : ViewModel() {

    fun loadServices(): LiveData<Resource<List<ServiceCategory>>> {
        return serviceCategoryRepository.loadServices(Request.add(null, Command.LIST_SERVICE_CATEGORIES.value))
    }

    fun loadServiceSubCategories(filterValue: String): LiveData<Resource<List<ServiceSubCategory>>> {
        return serviceCategoryRepository.loadServiceSubCategories(Request.list(null, Command.LIST_SERVICE_SUB_CATEGORIES.value)
                .filter("category_code", filterValue))
    }

}