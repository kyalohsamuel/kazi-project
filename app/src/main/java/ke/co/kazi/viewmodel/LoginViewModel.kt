package ke.co.kazi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.data.*
import ke.co.kazi.api.models.response.MobileCheckResponse
import ke.co.kazi.model.User
import ke.co.kazi.repository.LoginRepository
import ke.co.kazi.repository.SettingRepository
import ke.co.kazi.repository.UserRepository
import ke.co.kazi.util.AbsentLiveData
import ke.co.kazi.vo.Command
import ke.co.kazi.vo.Resource
import okhttp3.MultipartBody
import timber.log.Timber
import javax.inject.Inject


class LoginViewModel @Inject constructor(
        var loginRepository: LoginRepository,
        var userRepository: UserRepository,
        var settingRepository: SettingRepository) : ViewModel() {
    private var mobileNumberTemp = MutableLiveData<MobileCheckRequest>()
    private var _requestOtp = MutableLiveData<String>()
    private var _userDetails = MutableLiveData<LoginData>()
    private var _register = MutableLiveData<RegisterRequest>()

    val mobileCheckResponse: LiveData<Resource<MobileCheckResponse>> = Transformations.switchMap(mobileNumberTemp) { mobileNumber ->
        when (mobileNumber) {
            null -> AbsentLiveData.create()
            else -> loginRepository.checkMobileNumber(Request
                    .get(mobileNumber, Command.MOBILE_CHECK.value))
        }
    }

    val requestOtp: LiveData<Resource<User>> = Transformations.switchMap(_requestOtp) { mobileNumber ->
        when (mobileNumber) {
            null -> AbsentLiveData.create()
            else -> loginRepository.requestOtp(Request
                    .get(MobileCheckRequest(mobileNumber), Command.REQUEST_OTP.value))
        }
    }

    val userDetails: LiveData<Resource<LoginResponse>> = Transformations.switchMap(_userDetails) {
        when (it) {
            null -> AbsentLiveData.create()
            else -> loginRepository.login(Request.get(it, Command.LOGIN.value))
        }
    }

    val register: LiveData<Resource<User>> = Transformations.switchMap(_register) {
        when (it) {
            null -> AbsentLiveData.create()
            else -> loginRepository.register(Request.add(it, Command.REGISTER.value))
        }
    }

    fun checkMobileNumberStatus(mobileCheckRequest: MobileCheckRequest) {
        Timber.e("Mobile number: $mobileCheckRequest")
        mobileNumberTemp.value = MobileCheckRequest()
        when {
            mobileNumberTemp.value?.countryCode != mobileCheckRequest.countryCode || mobileNumberTemp.value?.mobileNumber != mobileCheckRequest.mobileNumber -> mobileNumberTemp.value = mobileCheckRequest
        }
    }

    fun loginAction(userDetails: LoginData) {
        _userDetails.value = userDetails

    }

    fun register(registerRequest: RegisterRequest) {
        _register.value = registerRequest
    }

    fun requestOtp(mobileNumber: String) {
        _requestOtp.value = mobileNumber
    }

    fun validateOtp(validateOtpData: ValidateOtpData) = loginRepository.validateOtp(Request.get(validateOtpData, Command.VALIDATE_OTP.value))

    fun saveUser(user: User, token: String): LiveData<Resource<Long>> = userRepository.saveUser(user, token)

    fun loadUserById(id: Long) = userRepository.loadUser(id)

    fun loadCountries() = settingRepository.loadCountries()

    fun resetPassword(data: ForgotPasswordData) = loginRepository.resetPassword(Request.post(data, Command.RESET_PASSWORD.value ))

    fun uploadProfilePhoto(photo: MultipartBody.Part) = userRepository.uploadProfilePhoto(photo)

    fun login(userDetails: LoginData) = loginRepository.login(Request.get(userDetails, Command.LOGIN.value))
}