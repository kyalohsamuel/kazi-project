package ke.co.kazi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.data.*
import ke.co.kazi.model.Service
import ke.co.kazi.repository.ServiceRepository
import ke.co.kazi.vo.Command
import ke.co.kazi.vo.Resource
import okhttp3.MultipartBody
import javax.inject.Inject


class ServiceViewModel @Inject constructor(var serviceRepository: ServiceRepository) : ViewModel() {

    var services = MutableLiveData<Resource<List<Service>>>()

    fun loadServices(): LiveData<Resource<List<Service>>> {
        return serviceRepository.loadServices(Request.get(null, Command.LIST_PROVIDER_SERVICES.value))
    }

    fun createService(service: Service): LiveData<Resource<Service>> = serviceRepository.createServices(Request.add(service, Command.ADD_SERVICE.value))

    fun loadServicesBySubCategory(subCategoryCode: String) = serviceRepository.loadServicesForSubCategory(Request.get(ServiceByCategoryData(subCategoryCode), Command.LIST_SERVICES_BY_SUB_CATEGORY.value))

    fun confirmService(serviceIdModel: ServiceRequestIdModel) = serviceRepository.confirmService(Request.add(serviceIdModel, Command.CONFIRM_SERVICE.value))

    fun requestService(serviceIdModel: ServiceIdModel) = serviceRepository.requestService(Request.add(serviceIdModel, Command.REQUEST_SERVICE.value))

    fun updateService(service: Service) = serviceRepository.updateService(Request.add(service, Command.UPDATE_SERVICE.value))

    fun getMyServiceRequest() = serviceRepository.getServiceRequests(Request.list(null, Command.LIST_CLIENT_REQUEST.value))

    fun getMyServiceRequest(status: String) = serviceRepository.getServiceRequests(Request.list(null, Command.LIST_CLIENT_REQUEST.value).filter("status", status))

    fun getClientServiceRequest() = serviceRepository.getServiceRequests(Request.list(null, Command.LIST_SP_CLIENT_REQUEST.value))

    fun getClientServiceRequest(status: String) = serviceRepository.getServiceRequests(Request.list(null, Command.LIST_SP_CLIENT_REQUEST.value).filter("status", status))

    fun createServiceProvider(type: String) =
            serviceRepository.createServiceProvider(Request.add(ProviderType(type), Command.ADD_SERVICE_PROVIDER.value))

    fun updateServiceProvider(updateServiceProvider: UpdateServiceProvider) =
            serviceRepository.updateIndividualServiceProvider(Request.add(updateServiceProvider, Command.UPDATE_SERVICE_PROVIDER.value))

    fun updateCompanyServiceProvider(updateServiceProvider: UpdateCompanyServiceProvider) =
            serviceRepository.updateCompanyServiceProvider(Request.add(updateServiceProvider, Command.UPDATE_SERVICE_PROVIDER.value))

    fun addServiceProviderCertification(certificate: Certificate) =
            serviceRepository.addServiceProviderCertificate(Request.add(certificate, Command.ADD_CERTIFICATE.value))

    fun uploadServiceImage(serviceId: String, photo: MultipartBody.Part) = serviceRepository.uploadServiceImage(serviceId, photo)

}