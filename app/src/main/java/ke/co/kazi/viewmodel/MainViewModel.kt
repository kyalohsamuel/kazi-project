package ke.co.kazi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.data.ProfileSettingsData
import ke.co.kazi.api.models.data.FcmDeviceRegistration
import ke.co.kazi.api.models.data.ServiceRequestData
import ke.co.kazi.model.Service
import ke.co.kazi.repository.ServiceRepository
import ke.co.kazi.repository.UserRepository
import ke.co.kazi.vo.Command
import ke.co.kazi.vo.Resource
import okhttp3.MultipartBody
import javax.inject.Inject

class MainViewModel @Inject constructor(var userRepository: UserRepository, var serviceRepository: ServiceRepository) : ViewModel() {
    fun fetchUser() = userRepository.fetchAnyOne()

    fun loadServicesBySubCategoryAndLocation(serviceRequestData: ServiceRequestData): LiveData<Resource<List<Service>>> {
        return serviceRepository.loadServicesForSubCategoryAndLocation(Request.get(serviceRequestData, Command.MAKE_SERVICES_REQUEST.value))
    }

    fun sendFcmRegistration(token: String?, deviceId: String) = userRepository.sendFcmRegistration(
            Request.add(FcmDeviceRegistration(deviceToken = token,
                    deviceUuid = deviceId), Command.REGISTER_DEVICE_TOKEN.value))

    fun updateProfileSettings(profileSettingsData: ProfileSettingsData) = userRepository.updateProfileSettings(Request.add(
            profileSettingsData, Command.UPDATE_PROFILE_SETTINGS.value ))

    fun uploadProfilePhoto(photo: MultipartBody.Part) = userRepository.uploadProfilePhoto(photo)
}