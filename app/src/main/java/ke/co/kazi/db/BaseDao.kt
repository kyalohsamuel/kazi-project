package ke.co.kazi.db

import androidx.room.*

@Dao
interface BaseDao<in T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(t: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(list: List<T>): Array<Long>

    @Update
    fun update(t: T)

    @Delete
    fun delete(t: T)
}