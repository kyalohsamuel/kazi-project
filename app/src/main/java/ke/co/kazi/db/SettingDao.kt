package ke.co.kazi.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import ke.co.kazi.model.Setting

@Dao
interface SettingDao : BaseDao<Setting> {

    @Query("SELECT * FROM settings WHERE id= :id")
    fun fetchOneById(id: Long): LiveData<Setting>

    @Query("SELECT * FROM settings")
    fun fetchAll(): LiveData<Setting>

    @Query("DELETE FROM settings")
    fun deleteAll()

    @Transaction
    fun save(setting: Setting): Long {
        deleteAll()
        return insert(setting)
    }
}