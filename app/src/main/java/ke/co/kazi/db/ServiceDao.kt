package ke.co.kazi.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import ke.co.kazi.model.Service

@Dao
interface ServiceDao {

    @Insert
    fun save(user: Service): Long

    @Insert
    fun saveAll(list: List<Service>): Array<Long>

    @Query("DELETE FROM services")
    fun deleteAll()

    @Query("SELECT * from services")
    fun allServices(): LiveData<List<Service>>

    @Transaction
    fun saveOnClear(list: List<Service>): Array<Long> {
        deleteAll()
        return saveAll(list)
    }
}