package ke.co.kazi.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import ke.co.kazi.model.ServiceCategory

@Dao
interface ServiceCategoryDao : BaseDao<ServiceCategory> {

    @Query("SELECT * from service_categories")
    fun allServices(): LiveData<List<ServiceCategory>>

    @Query("SELECT COUNT(*) FROM service_categories WHERE service_category_id = :id")
    fun countByServiceByServiceId(id: Int): Long

    @Transaction
    fun saveUnique(serviceCategory: ServiceCategory): Long? {
        val count = serviceCategory.serviceCategoryId?.let { countByServiceByServiceId(it) } ?: 0
        if (count == 0L) {
            return insert(serviceCategory)
        }
        return null
    }
}