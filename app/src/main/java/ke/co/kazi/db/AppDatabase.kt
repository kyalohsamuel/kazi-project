package ke.co.kazi.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ke.co.kazi.db.converters.ServiceConverter
import ke.co.kazi.model.*

@Database(
        entities = [
            User::class,
            Service::class,
            ServiceCategory::class,
            Country::class,
            Setting::class
        ],
        version = 25,
        exportSchema = true)
@TypeConverters(value = [ServiceConverter::class])
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun serviceDao(): ServiceDao
    abstract fun serviceCategoryDao(): ServiceCategoryDao
    abstract fun countryDao(): CountryDao
    abstract fun settingDao(): SettingDao
}