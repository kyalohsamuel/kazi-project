package ke.co.kazi.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import ke.co.kazi.model.Country

@Dao
interface CountryDao : BaseDao<Country> {

    @Query("SELECT * FROM countries")
    fun fetchAll(): LiveData<List<Country>>

    @Query("DELETE FROM countries")
    fun deleteAll()

    @Transaction
    fun saveAll(countries: List<Country>) {
        deleteAll()
        insertAll(countries)
    }
}