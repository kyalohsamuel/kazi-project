package ke.co.kazi.db

import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import ke.co.kazi.model.User

@Dao
interface UserDao {
    @Insert(onConflict = REPLACE)
    fun save(user: User): Long

    @Query("SELECT * FROM users LIMIT 1")
    fun fetchAnyOne(): LiveData<User>

    @Query("SELECT * FROM users WHERE id = :userId")
    fun getById(userId: Long): LiveData<User>

    @Query("SELECT * FROM users WHERE serverId = :serverId")
    fun getByServerId(serverId: String): LiveData<User>

    @Query("SELECT * FROM users WHERE contact = :phoneNumber")
    fun getByPhoneNumber(phoneNumber: String): LiveData<User>

    @Query("DELETE FROM users")
    fun deleteAll()

    @Query("SELECT * FROM users LIMIT 1")
    fun fetchUser(): User

    @Update
    fun update(user: User)

    @Transaction
    fun saveOnlyOne(user: User): Long {
        deleteAll()
        return save(user)
    }
}