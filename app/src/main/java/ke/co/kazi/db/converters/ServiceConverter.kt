package ke.co.kazi.db.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ke.co.kazi.model.OperationTime
import ke.co.kazi.model.ServiceImage
import ke.co.kazi.model.ServiceStatus
import java.util.*


class ServiceConverter {

    val gson = Gson()

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun stringToStringArray(value: String?): Array<String> {

        return if (value == null) {
            emptyArray()
        } else {
            val listType = object : TypeToken<Array<String>>() {
            }.type

            return gson.fromJson(value, listType)
        }
    }

    @TypeConverter
    fun arrayToString(stringArray: Array<String>): String {
        return gson.toJson(stringArray)
    }

    @TypeConverter
    fun valueToDoubleArray(value: String?): Array<Double> {

        return if (value == null) {
            emptyArray()
        } else {
            val listType = object : TypeToken<Array<Double>>() {
            }.type

            return gson.fromJson(value, listType)
        }
    }

    @TypeConverter
    fun arrayToString(array: Array<Double>): String {
        return gson.toJson(array)
    }

    @TypeConverter
    fun valueToOperationTimeArray(value: String?): Array<OperationTime> {

        return if (value == null) {
            emptyArray()
        } else {
            val listType = object : TypeToken<Array<OperationTime>>() {
            }.type

            return gson.fromJson(value, listType)
        }
    }

    @TypeConverter
    fun arrayToString(array: Array<OperationTime>): String {
        return gson.toJson(array)
    }

    @TypeConverter
    fun valueToImagesArray(value: String?): Array<ServiceImage> {

        return if (value == null) {
            emptyArray()
        } else {
            val listType = object : TypeToken<Array<ServiceImage>>() {
            }.type

            return gson.fromJson(value, listType)
        }
    }

    @TypeConverter
    fun arrayToString(array: Array<ServiceImage>): String {
        return gson.toJson(array)
    }

    @TypeConverter
    fun toString(serviceStatus: ServiceStatus?) = serviceStatus?.toString() ?: ""

    @TypeConverter
    fun toServiceStatus(value: String?) = if (value.isNullOrEmpty()) null else ServiceStatus.valueOf(value!!)
}