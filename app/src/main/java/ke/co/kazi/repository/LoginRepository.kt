package ke.co.kazi.repository

import androidx.lifecycle.LiveData
import ke.co.kazi.AppExecutors
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.Response
import ke.co.kazi.api.models.data.*
import ke.co.kazi.api.models.response.MobileCheckResponse
import ke.co.kazi.db.UserDao
import ke.co.kazi.model.User
import ke.co.kazi.util.AbsentLiveData
import ke.co.kazi.vo.NetworkBoundResource
import ke.co.kazi.vo.NetworkOnlyResource
import ke.co.kazi.vo.Resource
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginRepository @Inject constructor(val webservice: Webservice, val userDao: UserDao, val appExecutors: AppExecutors) : AnkoLogger {

    fun checkMobileNumber(mobileCheckRequest: Request<MobileCheckRequest>): LiveData<Resource<MobileCheckResponse>> {

        return object : NetworkBoundResource<MobileCheckResponse, Response<MobileCheckResponse>>(appExecutors) {

            private var mobileCheckResponse: MobileCheckResponse? = null

            override fun saveCallResult(item: Response<MobileCheckResponse>) {
                mobileCheckResponse = item.data
            }

            override fun shouldFetch(data: MobileCheckResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<MobileCheckResponse> {
                return if (mobileCheckResponse == null) AbsentLiveData.create()
                else object : LiveData<MobileCheckResponse>() {
                    override fun onActive() {
                        super.onActive()
                        value = mobileCheckResponse
                        mobileCheckResponse = null
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<Response<MobileCheckResponse>>> = webservice.checkPhoneStatus(mobileCheckRequest)

        }.asLiveData()


    }

    fun login(loginData: Request<LoginData>): LiveData<Resource<LoginResponse>> {
        return object : NetworkBoundResource<LoginResponse, Response<LoginResponse>>(appExecutors) {

            var user: LoginResponse? = null

            override fun saveCallResult(item: Response<LoginResponse>) {
                item.data?.apply {
                    error("User: $this")
//                    userDao.saveOnlyOne(user = this)
                    user = this
                }
            }

            override fun shouldFetch(data: LoginResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<LoginResponse> {
                return when (user) {
                    null -> AbsentLiveData.create()
                    else -> object : LiveData<LoginResponse>() {
                        override fun onActive() {
                            super.onActive()
                            value = user
                            user = null
                        }
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<Response<LoginResponse>>> = webservice.loginCall(loginData)

        }.asLiveData()
    }


    fun register(registerData: Request<RegisterRequest>): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, Response<User>>(appExecutors) {

            var user: User? = null

            override fun saveCallResult(item: Response<User>) {
                item.data?.apply {
                    error("User: $this")
//                    userDao.saveOnlyOne(user = this)
                    user = this
                }
            }

            override fun shouldFetch(data: User?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<User> {
                return when (user) {
                    null -> AbsentLiveData.create()
                    else -> object : LiveData<User>() {
                        override fun onActive() {
                            super.onActive()
                            value = user
                            user = null
                        }
                    }
                }
//                return userDao.getByPhoneNumber(mobileCheckRequest.data?.mobileNumber!!)
            }

            override fun createCall(): LiveData<ApiResponse<Response<User>>> = webservice.register(registerData)

        }.asLiveData()
    }

    fun requestOtp(request: Request<MobileCheckRequest>): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, Response<User>>(appExecutors) {

            var user: User? = null

            override fun saveCallResult(item: Response<User>) {
                item.data?.apply {
                    error("User: $this")
//                    userDao.saveOnlyOne(user = this)
                    user = this
                }
            }

            override fun shouldFetch(data: User?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<User> {
                return when (user) {
                    null -> AbsentLiveData.create()
                    else -> object : LiveData<User>() {
                        override fun onActive() {
                            super.onActive()
                            value = user
                            user = null
                        }
                    }
                }
//                return userDao.getByPhoneNumber(mobileCheckRequest.data?.mobileNumber!!)
            }

            override fun createCall(): LiveData<ApiResponse<Response<User>>> = webservice.requestOtp(request)

        }.asLiveData()
    }

    fun validateOtp(request: Request<ValidateOtpData>): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, Response<User>>(appExecutors) {

            var user: User? = null

            override fun saveCallResult(item: Response<User>) {
                item.data?.apply {
                    error("User: $this")
//                    userDao.saveOnlyOne(user = this)
                    user = this
                }
            }

            override fun shouldFetch(data: User?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<User> {
                return when (user) {
                    null -> AbsentLiveData.create()
                    else -> object : LiveData<User>() {
                        override fun onActive() {
                            super.onActive()
                            value = user
                            user = null
                        }
                    }
                }
//                return userDao.getByPhoneNumber(mobileCheckRequest.data?.mobileNumber!!)
            }

            override fun createCall(): LiveData<ApiResponse<Response<User>>> = webservice.validateOtp(request)

        }.asLiveData()
    }

    fun resetPassword(request: Request<ForgotPasswordData>): LiveData<Resource<Unit>>{
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? = item?.data

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.resetPassword(request)

        }.asLiveData()
    }
}