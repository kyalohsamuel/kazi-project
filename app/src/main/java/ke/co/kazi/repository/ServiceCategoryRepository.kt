package ke.co.kazi.repository

import androidx.lifecycle.LiveData
import ke.co.kazi.AppExecutors
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.Response
import ke.co.kazi.db.ServiceCategoryDao
import ke.co.kazi.model.ServiceCategory
import ke.co.kazi.model.ServiceSubCategory
import ke.co.kazi.vo.NetworkBoundResource
import ke.co.kazi.vo.Resource
import org.jetbrains.anko.AnkoLogger
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ServiceCategoryRepository @Inject constructor(val serviceCategoryDao: ServiceCategoryDao, val webservice: Webservice, val appExecutors: AppExecutors) : AnkoLogger {

    fun loadServices(request: Request<Nothing>): LiveData<Resource<List<ServiceCategory>>> {
        return object : NetworkBoundResource<List<ServiceCategory>, Response<List<ServiceCategory>>>(appExecutors) {
            override fun saveCallResult(item: Response<List<ServiceCategory>>) {
                item.data?.let {
                    it.forEach {
                        serviceCategoryDao.saveUnique(it)
                    }
                }
            }

            override fun shouldFetch(data: List<ServiceCategory>?): Boolean = true

            override fun loadFromDb(): LiveData<List<ServiceCategory>> = serviceCategoryDao.allServices()

            override fun createCall(): LiveData<ApiResponse<Response<List<ServiceCategory>>>> = webservice.getMyServices(request)


        }.asLiveData()
    }

    fun loadServiceSubCategories(request: Request<Nothing>): LiveData<Resource<List<ServiceSubCategory>>> {
        return object : NetworkBoundResource<List<ServiceSubCategory>, Response<List<ServiceSubCategory>>>(appExecutors) {

            var subCategories: List<ServiceSubCategory>? = null

            override fun saveCallResult(item: Response<List<ServiceSubCategory>>) {
                subCategories = item.data
            }

            override fun shouldFetch(data: List<ServiceSubCategory>?): Boolean = true

            override fun loadFromDb(): LiveData<List<ServiceSubCategory>> {
                return object : LiveData<List<ServiceSubCategory>>() {
                    override fun onActive() {
                        super.onActive()
                        value = subCategories
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<Response<List<ServiceSubCategory>>>> = webservice.getServiceSubCategories(request)


        }.asLiveData()
    }
}