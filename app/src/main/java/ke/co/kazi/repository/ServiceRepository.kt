package ke.co.kazi.repository

import androidx.lifecycle.LiveData
import ke.co.kazi.AppExecutors
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.Response
import ke.co.kazi.api.models.data.*
import ke.co.kazi.db.ServiceDao
import ke.co.kazi.db.UserDao
import ke.co.kazi.model.Service
import ke.co.kazi.model.ServiceRequest
import ke.co.kazi.vo.LocalResource
import ke.co.kazi.vo.NetworkBoundResource
import ke.co.kazi.vo.NetworkOnlyResource
import ke.co.kazi.vo.Resource
import okhttp3.MultipartBody
import org.jetbrains.anko.AnkoLogger
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ServiceRepository @Inject constructor(val serviceDao: ServiceDao, val webservice: Webservice, val appExecutors: AppExecutors, val userDao: UserDao) : AnkoLogger {
//    fun loadServicesBySubCategoryAndLocation(): LiveData<Resource<List<Service>>> {
//        return object : LocalResource<List<Service>>() {
//            override fun loadFromDb(): LiveData<List<Service>> {
//                return serviceDao.allServices()
//            }
//
//        }.asLiveData()
//    }

    fun loadServices(request: Request<Nothing>): LiveData<Resource<List<Service>>> {
        return object : NetworkBoundResource<List<Service>, Response<List<Service>>>(appExecutors) {
            override fun saveCallResult(item: Response<List<Service>>) {
                item.data?.let {
                    if (it.isNotEmpty()) {
                        serviceDao.saveOnClear(it)
                    }
                }
            }

            override fun shouldFetch(data: List<Service>?): Boolean = true

            override fun loadFromDb(): LiveData<List<Service>> = serviceDao.allServices()

            override fun createCall(): LiveData<ApiResponse<Response<List<Service>>>> = webservice.getServices(request)


        }.asLiveData()
    }

    fun loadServicesForSubCategory(request: Request<ServiceByCategoryData>): LiveData<Resource<List<Service>>> {
        return object : NetworkBoundResource<List<Service>, Response<List<Service>>>(appExecutors) {

            var list: List<Service>? = null

            override fun saveCallResult(item: Response<List<Service>>) {
                item.data?.let {
                    list = it
                }
            }

            override fun shouldFetch(data: List<Service>?): Boolean = true

            override fun loadFromDb(): LiveData<List<Service>> {
                return object : LiveData<List<Service>>() {
                    override fun onActive() {
                        super.onActive()
                        value = list
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<Response<List<Service>>>> = webservice.getServicesByCategory(request)


        }.asLiveData()
    }

    fun loadServicesForSubCategoryAndLocation(requestData: Request<ServiceRequestData>): LiveData<Resource<List<Service>>> {
        return object : NetworkBoundResource<List<Service>, Response<List<Service>>>(appExecutors) {

            var list: List<Service>? = null

            override fun saveCallResult(item: Response<List<Service>>) {
                item.data?.let {
                    list = it
                }
            }

            override fun shouldFetch(data: List<Service>?): Boolean = true

            override fun loadFromDb(): LiveData<List<Service>> {
                return object : LiveData<List<Service>>() {
                    override fun onActive() {
                        super.onActive()
                        value = list
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<Response<List<Service>>>> = webservice.getServicesByCategoryAndLocation(requestData)


        }.asLiveData()
    }

    fun createServices(request: Request<Service>): LiveData<Resource<Service>> {
        return object : NetworkBoundResource<Service, Response<Service>>(appExecutors) {

            var service: Service? = null

            override fun saveCallResult(item: Response<Service>) {
                service = item.data
            }

            override fun shouldFetch(data: Service?): Boolean = true

            override fun loadFromDb(): LiveData<Service> {
                return object : LiveData<Service>() {
                    override fun onActive() {
                        super.onActive()
                        value = service
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<Response<Service>>> = webservice.addServices(request)


        }.asLiveData()
    }

    fun saveService(service: Service): LiveData<Resource<Long>> {
        return object : LocalResource<Long>() {
            override fun loadFromDb(): LiveData<Long> {
                Timber.e("Loading data $service")
                return object : LiveData<Long>() {
                    override fun onActive() {
                        super.onActive()
                        val v = serviceDao.save(service)

                        appExecutors.mainThread().execute {
                            Timber.e("Saved index: $v")
                        }

                    }
                }
            }

        }.asLiveData()
    }

    fun save(list: List<Service>) {
        object : LocalResource<Unit>() {
            override fun loadFromDb(): LiveData<Unit> {
                return object : LiveData<Unit>() {
                    override fun onActive() {
                        super.onActive()
                        serviceDao.saveAll(list)
                    }
                }
            }

        }
    }

    fun confirmService(request: Request<ServiceRequestIdModel>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? {
                return item?.data
            }

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.confirmServiceRequest(request)
        }.asLiveData()
    }

    fun requestService(request: Request<ServiceIdModel>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? {
                return item?.data
            }

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.requestService(request)
        }.asLiveData()
    }

    fun getServiceRequests(request: Request<Nothing>): LiveData<Resource<List<ServiceRequest>>> {
        return object : NetworkOnlyResource<List<ServiceRequest>, Response<List<ServiceRequest>>>(appExecutors) {
            override fun processResult(item: Response<List<ServiceRequest>>?): List<ServiceRequest>? {
                return item?.data
            }

            override fun createCall(): LiveData<ApiResponse<Response<List<ServiceRequest>>>> = webservice.getServiceRequests(request)
        }.asLiveData()
    }

    fun createServiceProvider(request: Request<ProviderType>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? {
                userDao.update(userDao.fetchUser().apply {
                    userType = "SP"
                })
                return item?.data
            }

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.addServiceProvider(request)
        }.asLiveData()
    }

    fun updateIndividualServiceProvider(request: Request<UpdateServiceProvider>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? {
                return item?.data
            }

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.updateServiceProvider(request)
        }.asLiveData()
    }

    fun updateCompanyServiceProvider(request: Request<UpdateCompanyServiceProvider>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? {
                return item?.data
            }

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.updateCoServiceProvider(request)
        }.asLiveData()
    }

    fun addServiceProviderCertificate(request: Request<Certificate>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? {
                return item?.data
            }

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.addCertification(request)
        }.asLiveData()
    }

    fun uploadServiceImage(serviceId: String, photo: MultipartBody.Part): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? = item?.data

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>>
                    = webservice.uploadServiceImage(
                    MultipartBody.Part.createFormData("service_id", serviceId), photo)
        }.asLiveData()
    }

    fun updateService(request: Request<Service>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? = item?.data

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>>
                    = webservice.updateService(request)
        }.asLiveData()
    }
}