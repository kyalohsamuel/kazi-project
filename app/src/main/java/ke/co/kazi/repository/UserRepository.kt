package ke.co.kazi.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import ke.co.kazi.AppExecutors
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.Response
import ke.co.kazi.api.models.data.FcmDeviceRegistration
import ke.co.kazi.api.models.data.ProfileSettingsData
import ke.co.kazi.api.models.data.ServiceProviderDetail
import ke.co.kazi.db.UserDao
import ke.co.kazi.model.User
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.vo.LocalResource
import ke.co.kazi.vo.NetworkBoundResource
import ke.co.kazi.vo.NetworkOnlyResource
import ke.co.kazi.vo.Resource
import okhttp3.MultipartBody
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(private val webservice: Webservice, private val userDao: UserDao, val appExecutors: AppExecutors,
                                         val sharedPref: CustomSharedPreferences) {

    fun loadUser(userId: String): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, ke.co.kazi.api.models.Response<User>>(appExecutors) {
            override fun saveCallResult(item: ke.co.kazi.api.models.Response<User>) {
                userDao.save(item.data!!)
            }

            override fun shouldFetch(data: User?): Boolean = true

            override fun loadFromDb(): LiveData<User> = userDao.getByServerId(userId)

            override fun createCall(): LiveData<ApiResponse<Response<User>>> = webservice.getUserById(userId)


        }.asLiveData()
    }

    fun loadUser(id: Long): LiveData<Resource<User>> {
        return object : LocalResource<User>() {
            override fun loadFromDb(): LiveData<User> {
                return userDao.getById(id)
            }

        }.asLiveData()
    }

    fun fetchAnyOne(): LiveData<Resource<User>> {
        return object : LocalResource<User>() {
            override fun loadFromDb(): LiveData<User> {
                return userDao.fetchAnyOne()
            }

        }.asLiveData()
    }

    fun saveUser(user: User, token: String): LiveData<Resource<Long>> {
        sharedPref.setAccessToken(token)
        sharedPref.userId = user.serverId.toString()

        return object : LocalResource<Long>() {
            override fun loadFromDb(): LiveData<Long> {
                Timber.e("Saving user $user")
                return object : LiveData<Long>() {
                    override fun onActive() {
                        super.onActive()
                        appExecutors.runDiskIOWithMainThreadResult({
                            return@runDiskIOWithMainThreadResult userDao.saveOnlyOne(user)
                        }, { diskResult ->
                            value = diskResult
                        })
                    }
                }
            }

        }.asLiveData()
    }

    fun sendServiceProviderDetails(request: Request<ServiceProviderDetail>): LiveData<Resource<Unit>> {
        return object : NetworkBoundResource<Unit, Response<Unit>>(appExecutors) {

            var nothing: Unit? = null

            override fun saveCallResult(item: Response<Unit>) {
                this.nothing = item.data
            }

            override fun shouldFetch(data: Unit?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<Unit> {
                return object : LiveData<Unit>() {
                    override fun onActive() {
                        super.onActive()
                        value = nothing
                    }
                }
            }

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> {
                return webservice.sendServiceProviderDetails(request)
            }

        }.asLiveData()
    }

    fun sendFcmRegistration(request: Request<FcmDeviceRegistration>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? = item?.data

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.sentFcmRegistrationToServer(request)

        }.asLiveData()
    }

    fun updateProfileSettings(request: Request<ProfileSettingsData>): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.updateProfileSettings(request)

            override fun processResult(item: Response<Unit>?): Unit? = item?.data

        }.asLiveData()
    }

    fun uploadProfilePhoto(photo: MultipartBody.Part): LiveData<Resource<Unit>> {
        return object : NetworkOnlyResource<Unit, Response<Unit>>(appExecutors) {
            override fun processResult(item: Response<Unit>?): Unit? = item?.data

            override fun createCall(): LiveData<ApiResponse<Response<Unit>>> = webservice.uploadProfilePic(photo)
        }.asLiveData()
    }
}