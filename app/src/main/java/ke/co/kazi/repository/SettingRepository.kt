package ke.co.kazi.repository

import androidx.lifecycle.LiveData
import ke.co.kazi.AppExecutors
import ke.co.kazi.api.ApiResponse
import ke.co.kazi.api.Webservice
import ke.co.kazi.api.models.Request
import ke.co.kazi.api.models.Response
import ke.co.kazi.db.CountryDao
import ke.co.kazi.db.SettingDao
import ke.co.kazi.model.Country
import ke.co.kazi.model.Setting
import ke.co.kazi.vo.LocalResource
import ke.co.kazi.vo.NetworkBoundResource
import ke.co.kazi.vo.Resource
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SettingRepository @Inject constructor(var appExecutors: AppExecutors, val webservice: Webservice, val settingDao: SettingDao, val countryDao: CountryDao) {

    fun loadSetting(request: Request<Nothing>): LiveData<Resource<Setting>> {
        return object : NetworkBoundResource<Setting, Response<Setting>>(appExecutors = appExecutors) {
            override fun saveCallResult(item: Response<Setting>) {
                item.data?.let {
                    settingDao.save(setting = it)
                    countryDao.saveAll(it.countries)
                }
            }

            override fun shouldFetch(data: Setting?): Boolean = true

            override fun loadFromDb(): LiveData<Setting> = settingDao.fetchAll()

            override fun createCall(): LiveData<ApiResponse<Response<Setting>>> = webservice.getInitSettings(request)

        }.asLiveData()
    }

    fun loadCountries(): LiveData<Resource<List<Country>>> {
        return object : LocalResource<List<Country>>() {
            override fun loadFromDb(): LiveData<List<Country>> = countryDao.fetchAll()

        }.asLiveData()
    }
}