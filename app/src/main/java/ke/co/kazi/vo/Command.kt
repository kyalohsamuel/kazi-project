package ke.co.kazi.vo

enum class Command(var value: String) {
    MOBILE_CHECK("check_contact"),
    LOGIN("login"),
    REGISTER("register_user"),
    REQUEST_OTP("registration_otp"),
    VALIDATE_OTP("validate_reg_otp"),
    LIST_SERVICE_CATEGORIES("list_service_categories"),
    LIST_SERVICE_SUB_CATEGORIES("list_service_sub_categories"),
    GET_SETTINGS("get_settings"),
    ADD_SERVICE_PROVIDER("add_service_provider"),
    ADD_PROFESSIONAL_DETAILS("add_professional_details"),
    LIST_PROVIDER_SERVICES("list_serviceprovider_services"),
    ADD_SERVICE("add_service"),
    LIST_SERVICES_BY_SUB_CATEGORY("get_services_by_categorycode"),
    MAKE_SERVICES_REQUEST("make_services_request"),
    CONFIRM_SERVICE("confirm_request"),
    REQUEST_SERVICE("request_service"),
    LIST_SP_CLIENT_REQUEST("list_sp_client_request"),
    LIST_CLIENT_REQUEST("list_client_request"),
    REGISTER_DEVICE_TOKEN("register_device_token"),
    RESET_PASSWORD("reset_password"),
    UPDATE_PROFILE_SETTINGS("update_user_profile"),
    UPDATE_SERVICE_PROVIDER("update_service_provider"),
    ADD_CERTIFICATE("add_certificate"),
    UPDATE_SERVICE("update_service")
}