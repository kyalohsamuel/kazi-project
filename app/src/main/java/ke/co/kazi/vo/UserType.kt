package ke.co.kazi.vo

enum class UserType {
    SP, CLIENT
}