package ke.co.kazi.vo


enum class LoginStatus(var value: String) {
    NEW_USER("10002"),
    USER_FOUND("10001")
}