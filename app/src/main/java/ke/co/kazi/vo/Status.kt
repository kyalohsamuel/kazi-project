package ke.co.kazi.vo

enum class Status {
    LOADING,
    SUCCESS,
    FAILED
}