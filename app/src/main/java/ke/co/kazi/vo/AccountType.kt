package ke.co.kazi.vo

enum class AccountType {
    STANDARD,
    PREMIUM
}