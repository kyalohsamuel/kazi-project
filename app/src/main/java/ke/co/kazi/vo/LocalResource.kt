package ke.co.kazi.vo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.annotation.MainThread
import ke.co.kazi.AppExecutors


abstract class LocalResource<ResultType> @MainThread constructor(private val appExecutors: AppExecutors = AppExecutors()) {

    private val result = MediatorLiveData<Resource<ResultType>>()

    init {
        result.value = Resource.loading(null)
        val dbSource = loadFromDb()
        result.addSource(dbSource) { data ->
            setValue(Resource.success(data))
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    // Called to get the cached data from the database
    @MainThread
    protected abstract fun loadFromDb(): LiveData<ResultType>

    // returns a LiveData that represents the resource, implemented
    // in the base class.
    fun asLiveData() = result as LiveData<Resource<ResultType>>

}