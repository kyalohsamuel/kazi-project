package ke.co.kazi.vo

import com.google.android.gms.location.places.AutocompletePrediction
import java.io.Serializable

data class Place(var fullText: String, var primaryText: String, var secondaryText: String, var placeId: String) : Serializable {
    companion object {
        fun toPlace(result: AutocompletePrediction): Place {
            return Place(
                    result.getFullText(null).toString(),
                    result.getPrimaryText(null).toString(),
                    result.getSecondaryText(null).toString(),
                    result.placeId.toString())
        }
    }
}