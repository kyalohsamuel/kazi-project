package ke.co.kazi.vo

data class LatLong(var latitude: Double, var longitude: Double)