package ke.co.kazi.vo

enum class Action {
    POST,
    GET,
    PUT,
    ADD,
    LIST
}