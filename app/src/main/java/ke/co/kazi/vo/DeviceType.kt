package ke.co.kazi.vo


enum class DeviceType {
    ANDROID,
    IOS
}