package ke.co.kazi.binding

import androidx.databinding.BindingAdapter
import android.text.Html
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import ke.co.kazi.util.BASE_URL
import java.text.SimpleDateFormat
import java.util.*


object BindingAdapters {
    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("android:text")
    fun setText(view: TextView, date: Date?) {
        view.text = if (date == null) {
            ""
        } else {
            val format = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            format.format(date)
        }
    }


    @JvmStatic
    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView, imageUrl: String?) {
        if (imageUrl == null) return
        Glide.with(view.context)
                .load("$BASE_URL/$imageUrl")
                .thumbnail(0.1f)
                .into(view)
    }

    @JvmStatic
    @BindingAdapter("colouredText")
    fun colouredText(textView: TextView, text: String) {
        textView.text = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N)
            Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        else
            Html.fromHtml(text)
    }
}