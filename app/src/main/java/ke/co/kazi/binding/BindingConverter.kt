package ke.co.kazi.binding

import androidx.databinding.InverseMethod

class BindingConverter {

    @InverseMethod("stringToLong")
    fun longToString(value: Long?): String? = value?.toString() ?: ""

    fun stringToLong(value: String?): Long? {
        return if (value.isNullOrEmpty()) {
            0L
        } else {
            value?.toLong()
        }
    }

    @InverseMethod("stringToFloat")
    fun floatToString(value: Float?): String? = value?.toString() ?: ""

    fun stringToFloat(value: String?): Float? {
        return if (value.isNullOrEmpty()) {
            0F
        } else {
            value?.toFloat()
        }
    }

    @InverseMethod("stringToInt")
    fun intToString(value: Int?): String? = value?.toString() ?: ""

    fun stringToInt(value: String?): Int? {
        return if (value.isNullOrEmpty()) {
            0
        } else {
            value?.toInt()
        }
    }

    @InverseMethod("stringToDouble")
    fun doubleToString(value: Double?): String? = value?.toString() ?: ""

    fun stringToDouble(value: String?): Double? {
        return if (value.isNullOrEmpty()) {
            0.0
        } else {
            value?.toDouble()
        }
    }

}