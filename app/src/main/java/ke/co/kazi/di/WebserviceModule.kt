package ke.co.kazi.di

import dagger.Module
import dagger.Provides
import ke.co.kazi.api.Webservice
import ke.co.kazi.util.BASE_URL
import ke.co.kazi.util.CustomSharedPreferences
import ke.co.kazi.util.LiveDataCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import javax.inject.Singleton


@Module
class WebserviceModule {

    @Singleton
    @Provides
    fun providesWebservice(retrofit: Retrofit): Webservice {
        return retrofit.create(Webservice::class.java)
    }

    @Singleton
    @Provides
    fun providesRetrofit(okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
    }

    @Singleton
    @Provides
    fun providesOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor, sharedPreferences: CustomSharedPreferences): OkHttpClient {
        return OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .addNetworkInterceptor { chain ->
                    val token = sharedPreferences.getAccessToken()
                    val request = chain.request().newBuilder().addHeader("token", token ?: "").build()

                    chain.proceed(request)
                }
                .addInterceptor(httpLoggingInterceptor)
                .build()
    }

    @Singleton
    @Provides
    fun providesGsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Singleton
    @Provides
    fun providesHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor
                .Logger { message -> Timber.d(message) })
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }
}