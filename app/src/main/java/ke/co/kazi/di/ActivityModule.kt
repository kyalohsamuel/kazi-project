package ke.co.kazi.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ke.co.kazi.ui.v1.*
import ke.co.kazi.ui.v2.dashboard.DashboardActivity
import ke.co.kazi.ui.v2.merchant.ServiceActivity
import ke.co.kazi.ui.v2.onboarding.SplashActivity

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributeCheckMobileActivity(): CheckMobileActivity

    @ContributesAndroidInjector
    internal abstract fun contributeGetOtpActivity(): GetOtpActivity

    @ContributesAndroidInjector
    internal abstract fun contributeLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun contributeRegisterActvity(): RegisterActivity

    @ContributesAndroidInjector
    internal abstract fun contributeInitActivity(): InitActivity

    @ContributesAndroidInjector
    internal abstract fun contributeMainDrawerActivity(): MainDrawerActivity

    @ContributesAndroidInjector
    internal abstract fun contributeServiceProviderInfoActivity(): ServiceProviderInfoActivity

    @ContributesAndroidInjector
    internal abstract fun contributePlacesSearchActivity(): PlacesSearchActivity

    @ContributesAndroidInjector
    internal abstract fun contributeAddServiceActivity(): AddServiceActivity

    @ContributesAndroidInjector
    internal abstract fun contributeServiceCategoryActivity(): ServiceCategoryActivity

    @ContributesAndroidInjector
    internal abstract fun contributeServiceSubCategoryActivity(): ServiceSubCategoryActivity

    @ContributesAndroidInjector
    internal abstract fun contributeServicesListActivity(): ServicesListActivity

    @ContributesAndroidInjector
    internal abstract fun contributeServiceDetailsActivity(): ServiceDetailsActivity

    @ContributesAndroidInjector
    internal abstract fun contributeConfirmServiceRequestActivity(): ConfirmServiceRequestActivity

    @ContributesAndroidInjector
    internal abstract fun contributeBecomeServiceProviderActivity(): BecomeServiceProviderActivity

    @ContributesAndroidInjector
    internal abstract fun contributeChatActivity(): ChatActivity

    @ContributesAndroidInjector
    internal abstract fun contributeForgotPasswordActivity(): ForgotPasswordActivity

    @ContributesAndroidInjector
    internal abstract fun contributeProfileSettingsActivity(): ProfileSettingsActivity

    @ContributesAndroidInjector
    internal abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    internal abstract fun contributeDashboardActivity(): DashboardActivity

    @ContributesAndroidInjector
    internal abstract fun contributeServiceActivity(): ServiceActivity
}