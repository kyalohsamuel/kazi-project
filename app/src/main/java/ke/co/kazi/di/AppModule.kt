package ke.co.kazi.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import ke.co.kazi.AppExecutors
import ke.co.kazi.api.Webservice
import ke.co.kazi.db.*
import ke.co.kazi.repository.*
import ke.co.kazi.util.CustomSharedPreferences
import javax.inject.Singleton


@Module
class AppModule {

    @Provides
    @Singleton
    fun providePreferences(application: Application): CustomSharedPreferences {
        return CustomSharedPreferences(application)
    }

    @Provides
    fun providesUserRepository(webservice: Webservice, userDao: UserDao, appExecutors: AppExecutors, sharedPrefs: CustomSharedPreferences): UserRepository
            = UserRepository(webservice, userDao, appExecutors, sharedPrefs)

    @Provides
    fun providesLoginRepository(webservice: Webservice, userDao: UserDao, appExecutors: AppExecutors): LoginRepository = LoginRepository(webservice, userDao, appExecutors)

    @Provides
    fun providesServiceRepository(webservice: Webservice, serviceDao: ServiceDao, appExecutors: AppExecutors, userDao: UserDao): ServiceRepository = ServiceRepository(serviceDao, webservice, appExecutors, userDao)

    @Provides
    fun providesMyServiceRepository(webservice: Webservice, serviceCategoryDao: ServiceCategoryDao, appExecutors: AppExecutors): ServiceCategoryRepository = ServiceCategoryRepository(serviceCategoryDao, webservice, appExecutors)

    @Provides
    fun providesSettingRepository(webservice: Webservice, settingDao: SettingDao, countryDao: CountryDao, appExecutors: AppExecutors): SettingRepository = SettingRepository(appExecutors, webservice, settingDao, countryDao)

    @Singleton
    @Provides
    fun providesAppDatabase(application: Application): AppDatabase = Room.databaseBuilder(application.applicationContext, AppDatabase::class.java, "kazi.db")
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    fun providesUserDao(appDatabase: AppDatabase): UserDao = appDatabase.userDao()

    @Provides
    fun providesServiceDao(appDatabase: AppDatabase): ServiceDao = appDatabase.serviceDao()

    @Provides
    fun providesServiceCategoryDao(appDatabase: AppDatabase): ServiceCategoryDao = appDatabase.serviceCategoryDao()

    @Provides
    fun providesCountryDao(appDatabase: AppDatabase): CountryDao = appDatabase.countryDao()

    @Provides
    fun providesSettingDao(appDatabase: AppDatabase): SettingDao = appDatabase.settingDao()

}