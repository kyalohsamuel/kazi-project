package ke.co.kazi.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ke.co.kazi.notification.MyFirebaseMessagingService

@Module
abstract class ServiceModule {

    @ContributesAndroidInjector
    internal abstract fun contributeMyFirebaseMessagingService(): MyFirebaseMessagingService
}