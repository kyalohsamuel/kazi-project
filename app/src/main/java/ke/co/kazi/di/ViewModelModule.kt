package ke.co.kazi.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ke.co.kazi.ViewModelFactory
import ke.co.kazi.ViewModelKey
import ke.co.kazi.viewmodel.*
import javax.inject.Singleton

@Module
abstract class ViewModelModule {

    @Binds
    @Singleton
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @Singleton
    @IntoMap
    @ViewModelKey(UserProfileViewModel::class)
    abstract fun bindUserProfileViewModel(viewModel: UserProfileViewModel): ViewModel

    @Binds
    @Singleton
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @Singleton
    @IntoMap
    @ViewModelKey(ServiceViewModel::class)
    abstract fun bindServiceViewModel(viewModel: ServiceViewModel): ViewModel

    @Binds
    @Singleton
    @IntoMap
    @ViewModelKey(ServiceCategoryViewModel::class)
    abstract fun bindMyServiceViewModel(categoryViewModel: ServiceCategoryViewModel): ViewModel

    @Binds
    @Singleton
    @IntoMap
    @ViewModelKey(SettingViewModel::class)
    abstract fun bindSettingViewModel(settingViewModel: SettingViewModel): ViewModel

    @Binds
    @Singleton
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(settingViewModel: MainViewModel): ViewModel

}