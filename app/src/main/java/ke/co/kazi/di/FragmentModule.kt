package ke.co.kazi.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ke.co.kazi.ui.v1.fragments.*
import ke.co.kazi.ui.v2.dashboard.fragment.CategoriesFragment
import ke.co.kazi.ui.v2.merchant.fragment.ServiceImageUploadFragment
import ke.co.kazi.ui.v2.merchant.fragment.ServicesFragment
import ke.co.kazi.ui.v2.onboarding.fragment.*

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun contributeHistoryFragment(): HistoryFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServiceFragment(): ServiceFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServiceCategoryFragment(): ServiceCategoryFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServiceCategoryDetailsFragment(): ServiceCategoryDetailsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeNewServiceFragment(): NewServiceFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServiceMapFragment(): ServiceMapFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServiceDetailFragment(): ServiceDetailFragment

    @ContributesAndroidInjector
    internal abstract fun contributeFilePickerFragment(): FilePickerFragment

    @ContributesAndroidInjector
    internal abstract fun contributeUserProfileFragment(): UserProfileFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCheckNumberFragment(): CheckNumberFragment

    @ContributesAndroidInjector
    internal abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    internal abstract fun contributeRegistrationFragment(): RegistrationFragment

    @ContributesAndroidInjector
    internal abstract fun contributeVerificationCodeFragment(): VerificationCodeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCreateIndividualServiceProvider(): IndividualServiceProviderFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCreateCompanyServiceProvider(): CompanyServiceProviderFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCreateCertificationsServiceProvider(): CreateCertificationsServiceProviderFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCreateProfileDetailsFragment(): CreateProfileDetailsFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServicesFragment(): ServicesFragment

    @ContributesAndroidInjector
    internal abstract fun contributeOtherInformationServiceFragment(): OtherInformationServiceFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServiceImageUploadFragment(): ServiceImageUploadFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServiceInformationFragment(): ServiceInformationFragment

    @ContributesAndroidInjector
    internal abstract fun contributeServiceContactFragment(): ServiceContactFragment

    @ContributesAndroidInjector
    internal abstract fun contributesCategoriesFragment(): CategoriesFragment
}